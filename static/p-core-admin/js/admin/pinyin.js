define(function (require, exports, module) {
//拼音
    $("#J_pinyin").click(function () {
        str = $('input[name="name"]').val();
        $.ajax({
            url: '?m=module&c=city&a=pinyin',
            type: "post",
            data: 't=1&str=' + str,
            success: function (s) {
                $('input[name="pinyin"]').val(s.message);
            }
        });
    });

//首字母	
    $("#J_p").click(function () {
        str = $('input[name="name"]').val();
        $.ajax({
            url: '?m=module&c=city&a=pinyin',
            type: "post",
            data: 't=0&str=' + str,
            success: function (s) {
                $('input[name="p"]').val(s.message);
            }
        });
    });
}); 