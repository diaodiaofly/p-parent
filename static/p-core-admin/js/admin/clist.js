define(function (require, exports, module) {
    require('tree');
    $("#content_left").height($('#content_height', parent.document).height());
    $("#category_tree").show();
    $("#content_category_loading").hide();

    $("#category_tree").treeview({
        control: "#treecontrol",
        persist: "cookie",
        cookieId: "treeview-black"
    });
});