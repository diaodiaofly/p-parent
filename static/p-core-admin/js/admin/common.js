define(function (require, exports, module) {
    require('AjaxForm');
    var dialog = require('Dialog');

    exports.init = function () {
        //ajaxForm表单
        var ajaxForm = $("form.J_ajaxForm");
        if (ajaxForm.length) {
            if ($.browser.msie) {
                ajaxForm.on("submit", function (e) {
                    e.preventDefault();
                });
            }
            $("button.btn_submit").click(function (e) {
                e.preventDefault();
                var btn = $(this), text = btn.text();
                //ie处理placeholder提交问题
                if ($.browser.msie) {
                    ajaxForm.find("[placeholder]").each(function () {
                        var input = $(this);
                        if (input.val() == input.attr("placeholder")) {
                            input.val("");
                        }
                    });
                }
                //这里编辑器需要sync下
                if ($(".J_editor").length) {
                    editor.sync();
                }
                ajaxForm.ajaxSubmit({
                    url: btn.data("action") ? btn.data("action") : ajaxForm.attr("action"),
                    dataType: "json",
                    beforeSubmit: function (arr) {
                        btn.text(text + "中...").prop("disabled", true).addClass("disabled");
                    },
                    success: function (data) {
                        btn.removeProp("disabled").removeClass("disabled").text(text.replace("中...", "")).parent().find("span").remove();
                        if (data.code == "1") {
                            $('<span class="tips_success">' + data.msg + "</span>").appendTo(btn.parent()).delay(800).fadeOut(function () {
                                if (data.extData && data.extData.refer) {
                                    window.location.href = decodeURIComponent(data.extData.refer);
                                } else {
                                    window.location.reload();
                                }
                            });
                        } else if (data.code == "0") {
                            $('<span class="tips_error">' + data.msg + "</span>").appendTo(btn.parent());
                        }
                    }
                });
            });
        }

        //ajax请求
        if ($("a.J_ajaxRequest").length) {
            $(".J_ajaxRequest").click(function (e) {
                e.preventDefault();
                var href = $(this).prop("href"), msg = $(this).data("msg"), followid = $(this).attr("id");
                var msg = msg ? msg : "确定要执行吗？";
                dialog({
                    id: 'J_ajaxRequest',
                    follow: document.getElementById(followid),
                    content: msg,
                    ok: function () {
                        this.remove();
                        var d = dialog({
                            id: 'J_ajaxRequest',
                            follow: document.getElementById(followid)
                        }).show();
                        $.post(href, [], function (data) {
                            d.content(data.msg);
                            if (data.referer) {
                                setTimeout("window.location.href = decodeURIComponent(data.referer)", 1500);
                            } else {
                                setTimeout("window.location.reload()", 1500);

                            }
                        });
                        return false;
                    },
                    cancel: function () {
                    }
                }).show();
            });
        }

        //ajaxUrl弹窗
        if ($("a.J_ajaxUrl").length) {
            $(".J_ajaxUrl").click(function (e) {
                e.preventDefault();
                var href = $(this).prop("href"), title = $(this).data("title"), followid = $(this).attr("id");
                var title = title ? title : "信息";
                dialog({
                    fixed: true,
                    scrolling: 'yes',
                    id: 'J_ajaxUrl',
                    title: title,
                    url: href,
                    padding: 0,
                    width: '960px',
                    height: '450px'
                }).showModal();
            });
        }

        //所有的全选操作
        if ($(".J_check_all").length) {
            $(".J_check_all").click(function () {
                if (this.checked) {
                    $(".J_check").each(function () {
                        this.checked = true;
                    });
                } else {
                    $(".J_check").each(function () {
                        this.checked = false;
                    });
                }
            });
        }

        //日期选择器
        var dateInput = $("input.J_date");
        if (dateInput.length) {
            require.async("DatePicker", function () {
                dateInput.datePicker();
            });
        }

        //日期+时间选择器
        var dateTimeInput = $("input.J_datetime");
        if (dateTimeInput.length) {
            require.async("DatePicker", function () {
                dateTimeInput.datePicker({
                    time: true
                });
            });
        }

        //图片上传组件
        var J_upload = $(".J_upload");
        if (J_upload.length) {
            require.async("SwfUpload", function () {
                J_upload.each(function () {
                    var num_limit = $(this).attr('data');
                    $(this).swfupload({
                        id: $(this).attr("id"),
                        num_limit: num_limit
                    });
                });
            });
        }
    }

    //会员
    var J_user = $("#J_user");
    if (J_user.length) {
        require.async("autocomplete", function () {
            J_user.autocomplete({
                serviceUrl: Ajax_URL + "ajaxuser/",
                onSelect: function (value, data) {
                    J_user.val(data.username);
                    var J_userid = $("#J_userid");
                    if (J_userid.length) {
                        J_userid.val(data.userid);
                    }
                }
            });
        });
    }

    //如果含有编辑器则加载编辑器
    var J_editor = $(".J_editor");

    if (J_editor.length) {
        var editor;
        require.async('Editor', function () {
            J_editor.each(function () {
                editor = KindEditor.create($(this), {
                    width: "650px",
                    height: "300px",
                    items: [
                        'source', '|', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
                        'italic', 'underline', 'strikethrough', 'removeformat', '|',
                        'hr', 'justifyleft', 'justifycenter', 'justifyright',
                        'justifyfull', 'insertorderedlist', 'insertunorderedlist', '|', 'lineheight', 'link', 'unlink', 'table', 'image'
                    ]
                });
            });
        });
    }

    //含有拼音 绑定拼音
    var J_pinyin = $(".J_pinyin");
    if (J_pinyin.length) {
        J_pinyin.each(function () {
            $(this).click(function () {
                var _this = $(this), target = $(this).attr("data"), str = $('input[name="' + target + '"]').val();
                $.ajax({
                    url: "?m=module&c=city&a=pinyin",
                    type: "post",
                    data: "t=1&str=" + str,
                    success: function (s) {
                        _this.prev().val(s.message);
                    }
                });
            });
        });
    }

    // 旋转图片
    require("Rotate");
    //放大缩小图片
    function imgToSize(size) {
        var img = $("#J_check_pic");
        var oWidth = img.width();
        var oHeight = img.height();
        img.width(oWidth + size);
        img.height(oHeight + size / oWidth * oHeight);
    }

    var pic_size = 0;

    function imgReverse(arg) {
        var img = $("#J_check_pic");
        if (arg == 'left') {
            pic_size = pic_size - 30;
            img.rotate(pic_size);

        } else {
            pic_size = pic_size + 30;
            img.rotate(pic_size);
        }
    }

    //弹窗图片
    $("body").delegate(".check-pic-title", "click", function () {
        var width = ($(window).width() - 200) + 'px',
            height = ($(window).height() - 200) + 'px';
        var _pic = $(this).attr('data');
        var title = $(this).text();
        var d = art.dialog({
            id: 'check-pic-title',
            lock: true,
            fixed: true,
            title: title,
            content: '<div class="check-pic-img" id="check-pic-img" style="width:' + width + ';height:' + height + '"><div id="check-pic-imghere"><img src="' + _pic + '"  id="J_check_pic"></div></div>',
            button: [
                {
                    value: '放大',
                    callback: function () {
                        imgToSize(50);
                        return false;
                    }
                },
                {
                    value: '缩小',
                    callback: function () {
                        imgToSize(-50);
                        return false;
                    }
                },
                {
                    value: '向左旋转',
                    callback: function () {
                        imgReverse('left');
                        return false;
                    }
                },
                {
                    value: '向右旋转',
                    callback: function () {
                        imgReverse('right');
                        return false;
                    }
                }
            ]
        });
        require.async("dragDrop", function () {
            $('#check-pic-imghere').dragDrop();
        });
    });

    //自动计算信用等级的分数
    var J_credit_point = $(".J_credit_point");
    if (J_credit_point.length) {
        $(".J_credit_point").on("blur", function () {
            var p = 0;
            $('.J_credit_point').each(function () {
                p = parseInt($(this).val()) + p;
            })
            $('#credit_total').text(p);
        })
    }

    //拒绝特征码
    var J_verifystatus = $("#J_verifystatus");
    if (J_verifystatus.length) {
        J_verifystatus.find('input').click(function () {
            if ($(this).attr('value') == '-1') {
                $('#J_refusedata').show();
            } else {
                $('#J_refusedata').hide();
            }
        });
    }

    if ($(".J_slide_toggle").length) {
        $(".J_slide_toggle").click(function (e) {
            e.preventDefault();
            $('.J_slide_toggle_content').slideToggle('fast', function () {
                if ($('.J_slide_toggle_content').is(":visible")) {
                    $(".J_slide_toggle").text('隐藏用户细节');
                } else {
                    $(".J_slide_toggle").text('显示用户细节');
                }
            });
        });
    }

    if ($(".J_tr_expand").length) {
        $(".J_tr_expand").click(function () {
            var id_label = $(this).attr('data-hiddenControl');
            if ($('#' + id_label).is(':visible')) {
                $('#' + id_label).hide();
            } else {
                $('#' + id_label).show();
            }
        });
    }

});