define(function (require, exports, module) {
    var dialog = require('Dialog');
    if (window.parent !== window.self) {
        document.write = '';
        window.parent.location.href = window.self.location.href;
        setTimeout(function () {
            document.body.innerHTML = '';
        }, 0);
    }
    exports.init = function () {
        $(".btn").click(function () {
            var username = $('#name').val();
            var password = $('#password').val();
            //取消验证码20140607
            //var code = $('#code').val();
            if (!username || !password) {
                dialog({
                    id: 'login',
                    lock: true,
                    title: '提示',
                    content: '<span style="font-family:Arial, Microsoft Yahei;font-size:12px;color:#333;">帐号、密码不能为空！</span>'
                });

                return false;
            } else {
                return true;
            }
        })
    }
}); 