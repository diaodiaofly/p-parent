define(function (require, exports, module) {
    //栏目
    var root_tr_html = '<tr>\
						<td><span class="zero_icon mr10"></span></td>\
						<td>\
							<input type="text" style="width:20px;" value="" name="new_pid[0][]" class="input mr5">\
							<input name="new_name[0][]" type="text" class="input mr5" value="">\
							<input type="hidden" value="NEW_ID_" name="tempid[0][]">\
							<a style="display:none" data-nameid="NEW_ID_" class="link_add J_addChild" href="#" data-html="tbody" data-type="forum_child" data-id="temp_root_" data-forumlevel="2">添加子栏目</a>\
						</td>\
						<td></td>\
						<td><a class="mr5 J_newRow_del" href="#">[删除]</a></td>\
					</tr>';
    //返回二~四级版块添加的html
    function forumChild(forum_level, a_id, name_id) {
        var forum_text, plus_none_icon_arr = [], temp_name = 'new_', temp_id = 'id_';
        forum_text = '添加子栏目';
        //不同级别html差异
        for (var i = 2; i < forum_level; i++) {
            plus_none_icon_arr.push('<span class="plus_icon plus_none_icon"></span>');
        }
        //name_id值为添加按钮的data-nameid属性，表示父版未保存
        if (name_id) {
            temp_name = 'temp_';
            temp_id = name_id;	//未保存父版的隐藏input的value值
        }
        return '<tr data-del_level="' + forum_level + '"><td></td>\
				<td>' + plus_none_icon_arr.join('') + '\
					<span class="plus_icon plus_end_icon"></span>\
					<input type="text" style="width:20px;" value="" name="' + temp_name + 'pid[' + temp_id + '][]" class="input mr5">\
					<input name="' + temp_name + 'name[' + temp_id + '][]" type="text" class="input mr5" value="">\
					<input type="hidden" value="NEW_ID_" name="tempid[' + temp_id + '][]">\
					<a style="display:none" data-nameid="' + a_id + '" class="link_add J_addChild" href="#" data-id="id_" data-html="tr" data-type="forum_child" data-forumlevel="' + (forum_level + 1) + '">' + forum_text + '</a>\
				</td>\
				<td></td>\
				<td><a href="" class="mr5 J_new_forum_del">[删除]</a></td>\
			</tr>';
    }

    /*
     * 	各页面要声明插入的html段落变量，如：一级html变量为"root_tr_html"，子html为"child_tr_html"
     * 	html段所含的"root_", "child_", "id_"等字符(最后一位均为下划线)由以下js代码进行替换，做表单提交参数用；
     * 	各页面的input提交参数会有不同规律和需求。
     *
     *  删除“版块设置”页面添加行的js在其页面定义
     *
     *  $Id: forumTree_table.js 16999 2012-08-30 07:14:27Z hao.lin $
     */
    var table_list = $('#J_table_list');
    if (table_list.length) {
        var child_id = 1;
        //添加根导航&添加新分类&添加新积分，均为一级内容
        //var temp_id = 1;
        $('#J_add_root').on('click', function (e) {
            e.preventDefault();
            child_id++; //添加一个临时id关联
            var $this = $(this), $tbody;
            //转换&输出最终的html段
            if ($this.data('type') === 'credit_root') {
                //积分设置，依赖已有的最新积分的key值，由credit_run.htm页面声明
                last_credit_key = last_credit_key + 1;
                $tbody = $('<tbody>' + root_tr_html.replace(/root_/g, 'root_' + child_id).replace(/credit_key_/g, last_credit_key) + '</tbody>');
            } else {
                //其他
                $tbody = $('<tbody>' + root_tr_html.replace(/root_/g, 'root_' + child_id).replace(/NEW_ID_/g, child_id) + '</tbody>');
            }
            //完成添加
            table_list.append($tbody);
            $tbody.find('input.input').first().focus();
        });

        //添加二级导航&子版块等，均为二级及以下内容
        $('#J_table_list').on('click', '.J_addChild', function (e) {
            e.preventDefault();
            child_id++;
            var $this = $(this),
                id = $this.data('id'), //关联父子结构的参数
                $tr;
            //其他页面特殊变量
            var forum_level = $this.data('forumlevel') ? parseInt($this.data('forumlevel')) : '';
            /*版块设置_级别*/

            //转换&输出最终的html段
            if (forum_level) {
                //根据添加按钮是否含data-nameid属性，判断父版是否已保存
                if ($this.data('nameid')) {
                    var name_id = $this.data('nameid');
                }
                //二、三级_版块设置，forumChild()方法在setforum_run.htm模板底部，会返回“二、三级版块”的html段
                $tr = $(forumChild(forum_level, child_id, name_id).replace(/id_/g, id).replace(/NEW_ID_/, child_id));
            } else {
                //其他子html
                $tr = $(child_tr_html.replace(/child_/g, 'child_' + child_id).replace(/id_/g, id));
            }

            //判断插入位置，完成添加
            if ($this.data('html') === 'tbody') {
                //展开下拉
                $this.parents('tr').find('.J_start_icon.start_icon').click();
                //添加新版块&添加二级导航，需要判断tbody标签
                var tbody = $('#J_table_list_' + id);
                //无子内容则创建tbody标签
                if (!tbody.length) {
                    tbody = $('<tbody id="J_table_list_' + id + '"/>');
                    tbody.insertAfter($this.parents('tbody'));
                }
                $tr.prependTo(tbody);
            } else if ($this.data('html') === 'tr') {
                //添加二三级版块，html待定
                $tr.insertAfter($this.parents('tr'));
            }
            $tr.find('input.input').first().focus();

        });

        //新添加的行可直接删除
        table_list.on('click', 'a.J_newRow_del', function (e) {
            e.preventDefault();
            var tr = $(this).parents('tr'),
                tbody = $(this).parents('tbody');
            if (tr.next().length && !tr.prev().length) {
                //删除一级内容
                tbody.remove();
            } else {
                if (tbody.children().length === 1) {
                    tbody.remove();
                } else {
                    $(this).parents('tr').remove();
                }
            }
        });

        //树形菜单展开收缩
        var start_icon = $('.J_start_icon');
        start_icon.toggle(function (e) {
            var data_id = $(this).attr('data-id');
            $('#J_table_list_' + data_id).hide(100);
            $(this).removeClass('away_icon').addClass('start_icon');
        }, function () {
            var data_id = $(this).attr('data-id');
            $('#J_table_list_' + data_id).show(100);
            $(this).removeClass('start_icon').addClass('away_icon');

        });

        //展开全部
        $('#J_start_all').on('click', function (e) {
            e.preventDefault();
            var start_icons = $('.J_start_icon.start_icon');
            if (start_icons.length) {
                start_icons.removeClass('start_icon').addClass('away_icon');
                $('tbody[id^="J_table_list"]').show();
            }
        });


        //收起全部
        $('#J_away_all').on('click', function (e) {
            e.preventDefault();
            var away_icons = $('.J_start_icon.away_icon')
            if (away_icons.length) {
                away_icons.removeClass('away_icon').addClass('start_icon');
                $('tbody[id^="J_table_list"]').hide();
            }
        });

        //鼠标移上去显示添加导航按钮
        $('#J_table_list').on('mouseover', 'tr', function (e) {
            $(this).find('a.J_addChild').show();
        }).on('mouseout', 'tr', function (e) {
            $(this).find('a.J_addChild').hide();
        });

    }


    //版块_新添加的行可直接删除
    $('#J_table_list').on('click', 'a.J_new_forum_del', function (e) {
        e.preventDefault();
        var $this = $(this), tr = $this.parents('tr');

        //跟当前行比较"del-level"的值，含子版不删除
        if (tr.data('del_level') < tr.next().data('del_level')) {
            alert('该版块含有子版块，请先删除所有子版块，再进行此操作！');
            $this.focus();

        } else {
            tr.remove();
        }

    });


    //双击编辑版块名称
    var org_val;
    $('#J_table_list').on('dblclick', '.J_forum_names', function () {
        var $this = $(this),
            $input = $('<input type="text" value="' + $this.text() + '" data-id="' + $this.data('id') + '" class="input mr5 J_forum_names_input" name="name">');
        org_val = $this.text(); //原始版块名
        $input.insertAfter($this).focus();
        $this.remove();
    });

    //版块名称input失焦ajax提交
    $('#J_table_list').on('blur', '.J_forum_names_input', function () {
        var $this = $(this),
            restore = function () { //版块取消编辑状态
                $this.hide().after('<span class="mr10 J_forum_names" data-id="' + $this.data('id') + '">' + $this.val() + '</span>');
                $this.remove();
            };

        //判断版块名是否修改过
        if ($this.val() !== org_val) {
            $.post("/?m=funds&c=FinanceType&a=editname", {id: $this.data('id'), name: $this.val()}, function (data) {
                if (data.state === 'success') {
                    restore();
                }
            });
        } else {
            restore();
        }

    });
}); 