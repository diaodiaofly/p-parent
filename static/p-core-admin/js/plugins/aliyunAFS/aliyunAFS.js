define(function(require,exports,module){
	
	require('//g.alicdn.com/sd/ncpc/nc.css?t=20170522');
	require('//g.alicdn.com/sd/ncpc/nc.js?t=20170522');
	var aliyun_nc = new noCaptcha();
	var aliyunAFS = {
		init : function(myform,scene){
					$(function(){
						var aliyunAFSID = $(myform).find('#aliyunAFS');
						if (aliyunAFSID.length) {
							$(myform).append('<input type="hidden" name="aliyun_sessionid" /><input type="hidden" name="aliyun_sig" /><input type="hidden" name="aliyun_token" /><input type="hidden" name="aliyun_scene" />');
							
							var nc_appkey	= '2W3M';  // 应用标识,不可更改
							var nc_scene	= scene;  //场景,不可更改
							var nc_token	= [nc_appkey, (new Date()).getTime(), Math.random()].join(':');
							var nc_option	= {
								renderTo: '#aliyunAFS',//渲染到该DOM ID指定的Div位置
								appkey: nc_appkey,
								scene: nc_scene,
								token: nc_token,
								callback: function (data) {
									$(myform).find("input[name='aliyun_sessionid']").val(data.csessionid);
									$(myform).find("input[name='aliyun_sig']").val(data.sig);
									$(myform).find("input[name='aliyun_token']").val(nc_token);
									$(myform).find("input[name='aliyun_scene']").val(nc_scene);
									$(myform).find("input[name='aliyun_AFS_Check']").val('1');
								}
							};
							aliyun_nc.init(nc_option);
						}
					})
		},
		reload : function(myform){
			myform.find("input[name='aliyun_sessionid']").val('');
			myform.find("input[name='aliyun_sig']").val('');
			myform.find("input[name='aliyun_token']").val('');
			myform.find("input[name='aliyun_scene']").val('');
			myform.find("input[name='aliyun_AFS_Check']").val('0');
			aliyun_nc.reload();
		}
		
	}
	
	window.aliyunAFS = aliyunAFS;


});