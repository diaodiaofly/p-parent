define(function(require, exports, module) { 
require('./formvalidator.css');
require('./formvalidatorregex');
var onShowHtml = "$data$";
var onFocusHtml = "$data$";
var onErrorHtml = "$data$";
var onCorrectHtml = "$data$";
var onShowClass = "myinput";
var onFocusClass = "myinput focus";
var onErrorClass = "myinput inerror";
var onCorrectClass = "myinput";

    $.formValidator = {
        initConfig: function(d) {
            var a = {};
            $.extend(!0, a, initConfig_setting, d || {});
            "SingleTip" == a.mode && (a.errorFocus = !1);
            "" != a.formID && $("#" + a.formID).submit(function() {
                return null != a.ajaxForm ? ($.formValidator.ajaxForm(a.validatorGroup, a.ajaxForm), 
                !1) : 0 < a.ajaxCountValid ? (a.onAlert(a.ajaxPrompt), !1) : $.formValidator.pageIsValid(a.validatorGroup);
            });
            $("body").data(a.validatorGroup, a);
        },
        sustainType: function(e, a) {
            var c = e.tagName, f = e.type;
            switch (a) {
              case "formValidator":
                return !0;

              case "inputValidator":
                return "INPUT" == c || "TEXTAREA" == c || "SELECT" == c;

              case "compareValidator":
                return "INPUT" == c || "TEXTAREA" == c ? "checkbox" != f && "radio" != f : !1;

              case "ajaxValidator":
                return "text" == f || "textarea" == f || "file" == f || "password" == f || "select-one" == f;

              case "regexValidator":
                return "INPUT" == c || "TEXTAREA" == c ? "checkbox" != f && "radio" != f : !1;

              case "functionValidator":
                return !0;

              case "passwordValidator":
                return "password" == f;
            }
        },
    

        appendValid: function(d, a) {
            var c = $("#" + d).get(0), f = a.validateType;
            if (!$.formValidator.sustainType(c, f)) return -1;
            if ("formValidator" == f || void 0 == c.settings) c.settings = [];
            f = c.settings.push(a);
            c.settings[f - 1].index = f - 1;
            return f - 1;
        },
        setTipState: function(d, a, c) {
            if ("SingleTip" == $("body").data(d.validatorGroup).mode) $("#fv_content").html(c), 
            d.Tooltip = c, "onError" != a && f.hide(); else {
                var f = $("#" + d.settings[0].tipID), h = "onShow" == a ? onShowHtml : "onFocus" == a ? onFocusHtml : "onCorrect" == a ? onCorrectHtml : onErrorHtml;
                if (0 == h.length) f.hide(); else {
                    if (0 < d.validatorPasswordIndex && "onCorrect" == a) {
                        var g = d.settings[d.validatorPasswordIndex], j = $.formValidator.passwordValid(d);
                        c = "";
                        -1 == j && "" != g.onErrorContinueChar ? c = g.onErrorContinueChar : -2 == j && "" != g.onErrorSameChar ? c = g.onErrorSameChar : -3 == j && "" != g.onErrorCompareSame && (c = g.onErrorCompareSame);
                        if ("" != c) {
                            $.formValidator.setTipState(d, "onError", c);
                            return;
                        }
                        c = passwordStrengthText[0 >= j ? 0 : j - 1];
                    }
                    h = h.replace(/\$class\$/g, a).replace(/\$data\$/g, c);
                    "" != a ? f.html(h).removeClass().addClass(a) : f.html(h).show();
                }
                c = d.type;
                if ("password" == c || "text" == c || "file" == c) jqobj = $(d), "" != onShowClass && "onShow" == a && jqobj.removeClass().addClass(onShowClass), 
                "" != onFocusClass && "onFocus" == a && jqobj.removeClass().addClass(onFocusClass), 
                "" != onCorrectClass && "onCorrect" == a && jqobj.removeClass().addClass(onCorrectClass), 
                "" != onErrorClass && "onError" == a && jqobj.removeClass().addClass(onErrorClass);
            }
        },
        resetTipState: function(d) {
            void 0 == d && (d = "1");
            d = $("body").data(d);
            $.each(d.validObjects, function() {
                var a = this.settings[0], c = a.defaultPassed;
                $.formValidator.setTipState(this, c ? "onCorrect" : "onShow", c ? $.formValidator.getStatusText(this, a.onCorrect) : a.onShow);
            });
        },
        setFailState: function(d, a) {
            $.formValidator.setTipState($("#" + d).get(0), "onError", a);
        },
        showMessage: function(d) {
            var a = $("#" + d.id).get(0), c = d.isValid, f = d.setting, h = "", g = "", j = $("body").data(a.validatorGroup);
            if (c) $.formValidator.setTipState(a, "onCorrect", ""); else if (g = "onError", 
            "ajaxValidator" == f.validateType ? "" == f.lastValid ? (g = "onLoad", h = f.onWait) : h = $.formValidator.getStatusText(a, f.onError) : h = "" == d.errormsg ? $.formValidator.getStatusText(a, f.onError) : d.errormsg, 
            "AlertTip" == j.mode) {
                if (a.validValueOld != $(a).val()) j.onAlert(h);
            } else $.formValidator.setTipState(a, g, h);
            return h;
        },
        showAjaxMessage: function(d) {
            var a = $("#" + d.id).get(0), c = a.settings[d.ajax], f = a.validValueOld, h = $(a).val();
            d.setting = c;
            f != h || f == h && void 0 == a.onceValided ? $.formValidator.ajaxValid(d) : (void 0 != c.isValid && !c.isValid && (a.lastshowclass = "onError", 
            a.lastshowmsg = $.formValidator.getStatusText(a, c.onError)), $.formValidator.setTipState(a, a.lastshowclass, a.lastshowmsg));
        },
        getLength: function(d) {
            var a = $("#" + d), c = a.get(0), f = c.type;
            d = 0;
            switch (f) {
              case "text":
              case "hidden":
              case "password":
              case "textarea":
              case "file":
                a = a.val();
                f = c.settings[0];
                c.isInputControl && c.value == f.onShowText && (a = "");
                if ($("body").data(c.validatorGroup).wideWord) for (c = 0; c < a.length; c++) d += 19968 <= a.charCodeAt(c) && 40869 >= a.charCodeAt(c) ? 2 : 1; else d = a.length;
                break;

              case "checkbox":
              case "radio":
                d = $("input[type='" + f + "'][name='" + a.attr("name") + "']:checked").length;
                break;

              case "select-one":
                d = c.options ? c.options.selectedIndex : -1;
                break;

              case "select-multiple":
                d = $("select[name=" + c.name + "] option:selected").length;
            }
            return d;
        },
        isEmpty: function(d) {
            return $("#" + d).get(0).settings[0].empty && 0 == $.formValidator.getLength(d);
        },
        isOneValid: function(d) {
            return $.formValidator.oneIsValid(d).isValid;
        },
        oneIsValid: function(d) {
            var a = {};
            a.id = d;
            a.dom = $("#" + d).get(0);
            a.initConfig = $("body").data(a.dom.validatorGroup);
            a.ajax = -1;
            a.errormsg = "";
            a.settings = a.dom.settings;
            var c = a.settings.length, f;
            1 == c && (a.settings[0].bind = !1);
            if (!a.settings[0].bind) return null;
            $.formValidator.resetInputValue(!0, a.initConfig, d);
            for (var h = 0; h < c; h++) if (0 == h) {
                if ($.formValidator.isEmpty(d)) {
                    a.isValid = !0;
                    a.setting = a.settings[0];
                    break;
                }
            } else {
                a.setting = a.settings[h];
                f = a.settings[h].validateType;
                switch (f) {
                  case "inputValidator":
                    $.formValidator.inputValid(a);
                    break;

                  case "compareValidator":
                    $.formValidator.compareValid(a);
                    break;

                  case "regexValidator":
                    $.formValidator.regexValid(a);
                    break;

                  case "functionValidator":
                    $.formValidator.functionValid(a);
                    break;

                  case "ajaxValidator":
                    a.ajax = h;
                }
                if (a.settings[h].isValid) {
                    if (a.isValid = !0, a.setting = a.settings[0], "ajaxValidator" == a.settings[h].validateType) break;
                } else {
                    a.isValid = !1;
                    a.setting = a.settings[h];
                    break;
                }
            }
            $.formValidator.resetInputValue(!1, a.initConfig, d);
            return a;
        },
        pageIsValid: function(d) {
            void 0 == d && (d = "1");
            var a = !0, c, f = "", h, g = "^", j, l, n = "^", k = [], m = $("body").data(d);
            m.status = "sumbiting";
            m.ajaxCountSubmit = 0;
            $.each(m.validObjects, function() {
                if (0 == $(this).length) return !0;
                this.settings[0].bind && void 0 != this.validatorAjaxIndex && void 0 == this.onceValided && (c = $.formValidator.oneIsValid(this.id), 
                c.ajax == this.validatorAjaxIndex && (m.status = "sumbitingWithAjax", $.formValidator.ajaxValid(c)));
            });
            if (0 < m.ajaxCountSubmit) return !1;
            $.each(m.validObjects, function() {
                if (0 == $(this).length) return !0;
                if (this.settings[0].bind && (l = this.name, -1 == n.indexOf("^" + l + "^") && (onceValided = void 0 == this.onceValided ? !1 : this.onceValided, 
                l && (n = n + l + "^"), c = $.formValidator.oneIsValid(this.id)))) {
                    c.isValid || (a = !1, h = "" == c.errormsg ? c.setting.onError : c.errormsg, k[k.length] = h, 
                    null == j && (j = c.id), "" == f && (f = h));
                    if ("AlertTip" != m.mode) {
                        var d = this.settings[0].tipID;
                        -1 == g.indexOf("^" + d + "^") && (c.isValid || (g = g + d + "^"), $.formValidator.showMessage(c));
                    }
                    if (m.oneByOneVerify && !c.isValid) return !1;
                }
            });
            if (a) {
                if (!m.onSuccess()) return !0;
            } else m.onError(f, $("#" + j).get(0), k), j && m.errorFocus && $("#" + j).focus();
            m.status = "init";
            a && m.debug && alert("现在正处于调试模式(debug:true)，不能提交");
            return !m.debug && a;
        },
        ajaxForm: function(d, a, c) {
            void 0 == d && (d = "1");
            var f = {};
            $.extend(!0, f, ajaxForm_setting, a || {});
            a = $("body").data(d);
            if (void 0 == c && (c = a.formID, "" == c)) return alert("表单ID未传入"), !1;
            if (!$.formValidator.pageIsValid(d)) return !1;
            d = f.url;
            a = f.data;
            c = $.formValidator.serialize("#" + c);
            "POST" == f.type ? a += "" != a ? "&" + c : c : d += -1 < d.indexOf("?") ? "&" + c : "?" + c;
            $.ajax({
                type: f.type,
                url: d,
                cache: f.cache,
                data: a,
                async: f.async,
                timeout: f.timeout,
                dataType: f.dataType,
                beforeSend: function(a, c) {
                    f.buttons && 0 < f.buttons.length && f.buttons.attr({
                        disabled: !0
                    });
                    return f.beforeSend(a, c);
                },
                success: function(a, c, e) {
                    f.success(a, c, e);
                },
                complete: function(a, c) {
                    f.buttons && 0 < f.buttons.length && f.buttons.attr({
                        disabled: !1
                    });
                    f.complete(a, c);
                },
                error: function(a, c, e) {
                    f.error(a, c, e);
                }
            });
        },
        serialize: function(d, a) {
            void 0 != a && $.formValidator.resetInputValue(!0, a);
            var c = $(d).serialize();
            void 0 != a && $.formValidator.resetInputValue(!1, a);
            var c = c.split("&"), f = "";
            $.each(c, function(a, c) {
                var e = c.indexOf("=");
                if (0 < e) {
                    var d = c.substring(0, e), e = escape(decodeURIComponent(c.substr(e + 1))), d = d + "=" + e;
                    f = "" == f ? d : f + "&" + d;
                }
            });
            return f;
        },
        ajaxValid: function(d) {
            var a = d.id, c = $("#" + a).get(0), f = d.initConfig, h = c.settings, g = h[d.ajax], j = g.url, l = g.data, n = c.validatorGroup, f = $("body").data(n), k = $.formValidator.serialize(f.ajaxObjects), k = "clientid=" + a + "&" + (g.randNumberName ? g.randNumberName + "=" + (new Date().getTime() + Math.round(1e4 * Math.random())) : "") + (0 < k.length ? "&" + k : "");
            "POST" == g.type ? l += "" != l ? "&" + k : k : j += -1 < j.indexOf("?") ? "&" + k : "?" + k;
            $.ajax({
                type: g.type,
                url: j,
                cache: g.cache,
                data: l,
                async: g.async,
                timeout: g.timeout,
                dataType: g.dataType,
                success: function(a, j, p) {
                    var q = !1;
                    $.formValidator.dealAjaxRequestCount(n, -1);
                    d.dom.onceValided = !0;
                    j = g.success(a, j, p);
                    "string" == typeof j ? a = "onError" : j ? (q = !0, a = "onCorrect", j = h[0].onCorrect) : (a = "onError", 
                    j = $.formValidator.getStatusText(c, g.onError));
                    g.isValid = q;
                    $.formValidator.setTipState(c, a, j);
                    "sumbitingWithAjax" == d.initConfig.status && 0 == d.initConfig.ajaxCountSubmit && "" != f.formID && $("#" + f.formID).trigger("submit");
                },
                complete: function(a, c) {
                    g.buttons && 0 < g.buttons.length && g.buttons.attr({
                        disabled: !1
                    });
                    g.complete(a, c);
                },
                beforeSend: function(a, f) {
                    this.lastXMLHttpRequest && this.lastXMLHttpRequest.abort();
                    this.lastXMLHttpRequest = a;
                    g.buttons && 0 < g.buttons.length && g.buttons.attr({
                        disabled: !0
                    });
                    var j = g.beforeSend(a, f), q = !1;
                    g.isValid = !1;
                    "boolean" == typeof j && j ? (q = !0, $.formValidator.setTipState(c, "onLoad", h[d.ajax].onWait)) : (q = !1, 
                    $.formValidator.setTipState(c, "onError", j));
                    g.lastValid = "-1";
                    q && $.formValidator.dealAjaxRequestCount(n, 1);
                    return q;
                },
                error: function(a, f, h) {
                    $.formValidator.dealAjaxRequestCount(n, -1);
                    $.formValidator.setTipState(c, "onError", $.formValidator.getStatusText(c, g.onError));
                    g.isValid = !1;
                    d.dom.onceValided = !0;
                    g.error(a, f, h);
                },
                processData: g.processData
            });
        },
        dealAjaxRequestCount: function(d, a) {
            var c = $("body").data(d);
            c.ajaxCountValid += a;
            "sumbitingWithAjax" == c.status && (c.ajaxCountSubmit += a);
        },
        regexValid: function(d) {
            var a = d.id, c = d.setting;
            $("#" + a).get(0);
            var f = $("#" + a).get(0), h;
            f.settings[0].empty && "" == f.value ? c.isValid = !0 : (d = c.regExp, c.isValid = !1, 
            "string" == typeof d && (d = [ d ]), $.each(d, function() {
                var a = this;
                "enum" == c.dataType && (a = eval("regexEnum." + a));
                if (void 0 == a || "" == a) return !1;
                h = RegExp(a, c.param).test($(f).val());
                if ("||" == c.compareType && h) return c.isValid = !0, !1;
                if ("&&" == c.compareType && !h) return !1;
            }), c.isValid || (c.isValid = h));
        },
        functionValid: function(d) {
            var a = d.setting, c = $("#" + d.id), c = a.fun(c.val(), c.get(0));
            void 0 != c ? "string" === typeof c ? (a.isValid = !1, d.errormsg = c) : a.isValid = c : a.isValid = !0;
        },
        inputValid: function(d) {
            var a = d.id, c = d.setting, f = $("#" + a), h = f.get(0), f = f.val(), h = h.type, a = $.formValidator.getLength(a), g = c.empty, j = !1;
            switch (h) {
              case "text":
              case "hidden":
              case "password":
              case "textarea":
              case "file":
                "size" == c.type && (g = c.empty, g.leftEmpty || (j = f.replace(/^[ \s]+/, "").length != f.length), 
                !j && !g.rightEmpty && (j = f.replace(/[ \s]+$/, "").length != f.length), j && g.emptyError && (d.errormsg = g.emptyError));

              case "checkbox":
              case "select-one":
              case "select-multiple":
              case "radio":
                g = !1;
                if ("select-one" == h || "select-multiple" == h) c.type = "size";
                h = c.type;
                "size" == h ? (j || (g = !0), g && (f = a)) : "date" == h || "datetime" == h ? ("date" == h && (g = isDate(f)), 
                "datetime" == h && (g = isDate(f)), g && (f = new Date(f), c.min = new Date(c.min), 
                c.max = new Date(c.max))) : (stype = typeof c.min, "number" == stype && (f = new Number(f).valueOf(), 
                isNaN(f) || (g = !0)), "string" == stype && (g = !0));
                c.isValid = !1;
                g && (f < c.min || f > c.max ? (f < c.min && c.onErrorMin && (d.errormsg = c.onErrorMin), 
                f > c.min && c.onErrorMax && (d.errormsg = c.onErrorMax)) : c.isValid = !0);
            }
        },
        compareValid: function(d) {
            var a = d.setting, c = $("#" + d.id), f = $("#" + a.desID);
            d = a.dataType;
            curvalue = c.val();
            ls_data = f.val();
            if ("number" == d) if (!isNaN(curvalue) && !isNaN(ls_data)) curvalue = parseFloat(curvalue), 
            ls_data = parseFloat(ls_data); else return;
            if ("date" == d || "datetime" == d) if (c = !1, "date" == d && (c = isDate(curvalue) && isDate(ls_data)), 
            "datetime" == d && (c = isDateTime(curvalue) && isDateTime(ls_data)), c) curvalue = new Date(curvalue), 
            ls_data = new Date(ls_data); else return;
            switch (a.operateor) {
              case "=":
                a.isValid = curvalue == ls_data;
                break;

              case "!=":
                a.isValid = curvalue != ls_data;
                break;

              case ">":
                a.isValid = curvalue > ls_data;
                break;

              case ">=":
                a.isValid = curvalue >= ls_data;
                break;

              case "<":
                a.isValid = curvalue < ls_data;
                break;

              case "<=":
                a.isValid = curvalue <= ls_data;
                break;

              default:
                a.isValid = !1;
            }
        },
        passwordValid: function(d) {
            function a(c, e, d) {
                void 0 == e && (e = [ 0, 0, 0, 0 ]);
                void 0 == d && (d = -1);
                d++;
                e[d] = c % 2;
                c = Math.floor(c / 2);
                return 1 == c || 0 == c ? (e[d + 1] = c, e) : e = a(c, e, d);
            }
            var c = d.settings[d.validatorPasswordIndex];
            d = d.value;
            if ("" == d) return 0;
            var f;
            if (f = "" != c.onErrorContinueChar) a: {
                f = d.toLowerCase();
                for (var h = 0, g = 0; g < f.length; g++) if (f.charCodeAt(g) != h + 1 && 0 != h) {
                    f = !1;
                    break a;
                } else h = f.charCodeAt(g);
                f = !0;
            }
            if (f) return -1;
            if (f = "" != c.onErrorSameChar) a: {
                f = d.toLowerCase();
                for (g = h = 0; g < f.length; g++) if (f.charCodeAt(g) != h && 0 != h) {
                    f = !1;
                    break a;
                } else h = f.charCodeAt(g);
                f = !0;
            }
            if (f) return -2;
            if ("" != c.compareID && $("#" + c.compareID).val() == d) return -3;
            for (var j = [ 0, 0, 0, 0 ], l = d.length, c = 0; c < l; c++) f = d.charCodeAt(c), 
            48 <= f && 57 >= f ? j[0] += 1 : 97 <= f && 122 >= f ? j[1] += 1 : 65 <= f && 90 >= f ? j[2] += 1 : 0 <= "!,@,#,$,%,^,&,*,?,_,~".indexOf(d.substr(c, 1)) && (j[3] += 1);
            var n = 0, k = !0;
            $.each(passwordStrengthRule, function(c, d) {
                var f = d.level;
                $.each(d.rule, function(c, d) {
                    var g = 0;
                    k = !0;
                    $.each(a(d.flag), function(a, c) {
                        if (1 == c) {
                            var e = d.value[g++], e = 0 == e ? l : e > l ? l : e;
                            if (j[a] < e) return k = !1;
                        }
                    });
                    if (k) return n = f, !1;
                });
                k && (n = f);
            });
            return n;
        },
        localTooltip: function(d) {
            d = d || window.event;
            var a = d.pageX || (d.clientX ? d.clientX + document.body.scrollLeft : 0);
            d = d.pageY || (d.clientY ? d.clientY + document.body.scrollTop : 0);
            $("#fvtt").css({
                top: d + 2 + "px",
                left: a - 40 + "px"
            });
        },
        reloadAutoTip: function(d) {
            void 0 == d && (d = "1");
            var a = $("body").data(d);
            $.each(a.validObjects, function() {
                if ("AutoTip" == a.mode) {
                    var c = this.settings[0], d = "#" + c.relativeID, h = $(d).offset(), g = h.top, d = $(d).width() + h.left;
                    $("#" + c.tipID).parent().show().css({
                        left: d + "px",
                        top: g + "px"
                    });
                }
            });
        },
        getStatusText: function(d, a) {
            return $.isFunction(a) ? a($(d).val()) : a;
        },
        resetInputValue: function(d, a, c) {
            (c ? $("#" + c) : $(a.showTextObjects)).each(function(a, c) {
                if (c.isInputControl) {
                    var e = c.settings[0].onShowText;
                    d && e == c.value && (c.value = "");
                    !d && "" != e && "" == c.value && (c.value = e);
                }
            });
        }
    };
    $.fn.formValidator = function(d) {
        d = d || {};
        var a = {};
        void 0 == d.validatorGroup && (d.validatorGroup = "1");
        $.extend(!0, a, formValidator_setting);
        var c = $("body").data(d.validatorGroup);
        c.validCount += 1;
        "SingleTip" == c.mode && (a.tipCss = {
            left: 10,
            top: 1,
            width: 22,
            height: 22,
            display: "none"
        });
        "AlertTip" == c.mode && (a.autoModify = !0);
        $.extend(!0, a, d || {});
        return this.each(function() {
            this.validatorIndex = c.validCount - 1;
            this.validatorGroup = d.validatorGroup;
            var f = $(this), h = {};
            $.extend(!0, h, a);
            var g = f.attr("id");
            g || (g = Math.ceil(5e7 * Math.random()), f.attr("id", g));
            var j = h.tipID ? h.tipID : g + "Tip";
            a.tipID = a.tipID;
            a.pwdTipID = h.pwdTipID ? h.pwdTipID : a.tipID;
            a.fixTipID = h.fixTipID ? h.fixTipID : g + "FixTip";
            $.formValidator.appendValid(g, a);
            var l = $.inArray(f, c.validObjects);
            -1 == l ? (h.ajax && c.ajaxObjects.push(this), c.validObjects.push(this)) : c.validObjects[l] = this;
            "AlertTip" != c.mode && $.formValidator.setTipState(this, "onShow", a.onShow);
            var h = this.tagName.toLowerCase(), l = this.type, n = a.defaultValue, k = "password" == l || "text" == l || "textarea" == l;
            this.isInputControl = k;
            n && f.val(n);
            var m = $("#" + a.fixTipID), r = a.onShowFixText;
            1 == m.length && "" != onMouseOutFixTextHtml && "" != onMouseOnFixTextHtml && "" != r && (f.hover(function() {
                m.html(onMouseOnFixTextHtml.replace(/\$data\$/g, r));
            }, function() {
                m.html(onMouseOutFixTextHtml.replace(/\$data\$/g, r));
            }), m.css("padding", "0px 0px 0px 0px").css("margin", "0px 0px 0px 0px").html(onMouseOutFixTextHtml.replace(/\$data\$/g, a.onShowFixText)));
            var p = a.onShowText;
            "input" == h || "textarea" == h ? (k && "" != p && "" == f.val() && (showObjs = c.showTextObjects, 
            c.showTextObjects = showObjs + ("" != showObjs ? ",#" : "#") + g, f.val(p), f.css("color", a.onShowTextColor.mouseOutColor)), 
            f.focus(function() {
                if (k) {
                    var d = f.val();
                    this.validValueOld = d;
                    p == d && (this.value = "", f.css("color", a.onShowTextColor.mouseOnColor));
                }
                "AlertTip" != c.mode && (d = $("#" + j), this.lastshowclass = d.attr("class"), this.lastshowmsg = d.text(), 
                $.formValidator.setTipState(this, "onFocus", a.onFocus));
                0 < this.validatorPasswordIndex && ($("#" + a.pwdTipID).show(), f.trigger("keyup"));
            }), f.bind("keyup", function() {
                if (0 < this.validatorPasswordIndex) try {
                    var c = $.formValidator.oneIsValid(g), d = $.formValidator.passwordValid(this);
                    0 > d && (d = 0);
                    c.isValid || (d = 0);
                    $("#" + a.pwdTipID).show();
                    $("#" + a.pwdTipID).html(passwordStrengthStatusHtml[d]);
                } catch (f) {
                    alert("密码强度校验失败,错误原因:变量passwordStrengthStatusHtml语法错误或者为设置)");
                }
            }), f.bind(a.triggerEvent, function() {
                var d = this.settings;
                d[0].leftTrim && (this.value = this.value.replace(/^\s*/g, ""));
                d[0].rightTrim && (this.value = this.value.replace(/\s*$/g, ""));
                k && ("" == this.value && "" != p && (this.value = p), this.value == p && f.css("color", a.onShowTextColor.mouseOutColor));
                d = $.formValidator.oneIsValid(g);
                if (null != d) if (0 <= d.ajax) $.formValidator.showAjaxMessage(d); else {
                    var h = $.formValidator.showMessage(d);
                    if (!d.isValid) if (a.autoModify && k) $(this).val(this.validValueOld), "AlertTip" != c.mode && $.formValidator.setTipState(this, "onShow", $.formValidator.getStatusText(this, a.onCorrect)); else if (c.forceValid || a.forceValid) intiConfig.onAlert(h), 
                    this.focus();
                }
            })) : "select" == h && f.bind({
                focus: function() {
                    "AlertTip" != c.mode && $.formValidator.setTipState(this, "onFocus", a.onFocus);
                },
                blur: function() {
                    (void 0 == this.validValueOld || this.validValueOld == f.val()) && $(this).trigger("change");
                },
                change: function() {
                    var a = $.formValidator.oneIsValid(g);
                    null != a && (0 <= a.ajax ? $.formValidator.showAjaxMessage(a) : $.formValidator.showMessage(a));
                }
            });
        });
    };
    $.fn.inputValidator = function(d) {
        var a = {};
        $.extend(!0, a, inputValidator_setting, d || {});
        return this.each(function() {
            $.formValidator.appendValid(this.id, a);
        });
    };
    $.fn.compareValidator = function(d) {
        var a = {};
        $.extend(!0, a, compareValidator_setting, d || {});
        return this.each(function() {
            $.formValidator.appendValid(this.id, a);
        });
    };
    $.fn.regexValidator = function(d) {
        var a = {};
        $.extend(!0, a, regexValidator_setting, d || {});
        return this.each(function() {
            $.formValidator.appendValid(this.id, a);
        });
    };
    $.fn.functionValidator = function(d) {
        var a = {};
        $.extend(!0, a, functionValidator_setting, d || {});
        return this.each(function() {
            $.formValidator.appendValid(this.id, a);
        });
    };
    $.fn.ajaxValidator = function(d) {
        var a = {};
        $.extend(!0, a, ajaxValidator_setting, d || {});
        return this.each(function() {
            var c = $("body").data(this.validatorGroup);
            -1 == $.inArray(this, c.ajaxObjects) && c.ajaxObjects.push(this);
            this.validatorAjaxIndex = $.formValidator.appendValid(this.id, a);
        });
    };
    $.fn.passwordValidator = function(d) {
        var a = {};
        $.extend(!0, a, passwordValidator_setting, d || {});
        return this.each(function() {
            this.validatorPasswordIndex = $.formValidator.appendValid(this.id, a);
        });
    };
    $.fn.defaultPassed = function(d) {
        return this.each(function() {
            var a = this.settings;
            this.onceValided = a[0].defaultPassed = !0;
            for (var c = 1; c < a.length; c++) a[c].isValid = !0, "AlertTip" != $("body").data(a[0].validatorGroup).mode && (this.lastshowclass = d ? "onShow" : "onCorrect", 
            this.lastshowmsg = d ? a[0].onShow : a[0].onCorrect, $.formValidator.setTipState(this, this.lastshowclass, this.lastshowmsg));
        });
    };
    $.fn.unFormValidator = function(d) {
        return this.each(function() {
            this.settings && (this.settings[0].bind = !d, d ? $("#" + this.settings[0].tipID).hide() : $("#" + this.settings[0].tipID).show());
        });
    };
    $.fn.showTooltips = function() {
        0 == $("body [id=fvtt]").length && (fvtt = $("<div id='fvtt' style='position:absolute;z-index:56002'></div>"), 
        $("body").append(fvtt), fvtt.before("<iframe index=0 src='about:blank' class='fv_iframe' scrolling='no' frameborder='0'></iframe>"));
        return this.each(function() {
            jqobj = $(this);
            s = $("<span class='top' id=fv_content style='display:block'></span>");
            b = $("<b class='bottom' style='display:block' />");
            this.tooltip = $("<span class='fv_tooltip' style='display:block'></span>").append(s).append(b).css({
                filter: "alpha(opacity:95)",
                KHTMLOpacity: "0.95",
                MozOpacity: "0.95",
                opacity: "0.95"
            });
            jqobj.bind({
                mouseover: function(d) {
                    $("#fvtt").empty().append(this.tooltip).show();
                    $("#fv_content").html(this.Tooltip);
                    $.formValidator.localTooltip(d);
                },
                mouseout: function() {
                    $("#fvtt").hide();
                },
                mousemove: function(d) {
                    $.formValidator.localTooltip(d);
                }
            });
        });
    };
var initConfig_setting = {
    theme: "Default",
    validatorGroup: "1",
    formID: "",
    submitOnce: !0,
    ajaxForm: null,
    mode: "FixTip",
    errorFocus: !1,
    wideWord: !0,
    forceValid: !1,
    debug: !1,
    inIframe: !1,
    onSuccess: function() {
        return !0;
    },
    onError: $.noop,
    onAlert: function(e) {
        alert(e);
    },
    status: "",
    ajaxPrompt: "当前有数据正在进行服务器端校验，请稍候",
    validCount: 0,
    ajaxCountSubmit: 0,
    ajaxCountValid: 0,
    validObjects: [],
    ajaxObjects: [],
    showTextObjects: "",
    validateType: "initConfig",
    offsetChrome: {
        left: 42,
        top: 0
    },
    oneByOneVerify: !1
}, formValidator_setting = {
    validatorGroup: "1",
    onShowText: "",
    onShowTextColor: {},
    onShowFixText: "",
    onShow: "",
    onFocus: "",
    onCorrect: "",
    onEmpty: "",
    empty: !1,
    autoModify: !1,
    defaultValue: null,
    bind: !0,
    ajax: !1,
    validateType: "formValidator",
    tipCss: {
        left: 10,
        top: -4,
        height: 20,
        width: 280
    },
    triggerEvent: "blur",
    forceValid: !1,
    tipID: null,
    pwdTipID: null,
    fixTipID: null,
    relativeID: null,
    index: 0,
    leftTrim: !1,
    rightTrim: !1
}, inputValidator_setting = {
    isValid: !1,
    type: "size",
    min: 0,
    max: 99999,
    onError: "输入错误",
    validateType: "inputValidator",
    empty: {
        leftEmpty: !0,
        rightEmpty: !0,
        leftEmptyError: null,
        rightEmptyError: null
    }
}, compareValidator_setting = {
    isValid: !1,
    desID: "",
    operateor: "=",
    onError: "输入错误",
    validateType: "compareValidator"
}, regexValidator_setting = {
    isValid: !1,
    regExp: "",
    param: "i",
    dataType: "string",
    compareType: "||",
    onError: "输入的格式不正确",
    validateType: "regexValidator"
}, ajaxForm_setting = {
    type: "GET",
    url: window.location.href,
    dataType: "html",
    timeout: 1e5,
    data: null,
    type: "GET",
    async: !0,
    cache: !1,
    buttons: null,
    beforeSend: function() {
        return !0;
    },
    success: function() {
        return !0;
    },
    complete: $.noop,
    processData: !0,
    error: $.noop
}, ajaxValidator_setting = {
    isValid: !1,
    lastValid: "",
    oneceValid: !1,
    randNumberName: "rand",
    onError: "服务器校验没有通过",
    onWait: "正在等待服务器返回数据",
    validateType: "ajaxValidator"
};

$.extend(!0, ajaxValidator_setting, ajaxForm_setting);

var functionValidator_setting = {
    isValid: !0,
    fun: function() {
        this.isValid = !0;
    },
    validateType: "functionValidator",
    onError: "输入错误"
}, passwordValidator_setting = {
    isValid: !0,
    compareID: "",
    validateType: "passwordValidator",
    onErrorContinueChar: "密码字符为连续字符不被允许",
    onErrorSameChar: "密码字符都相同不被允许",
    onErrorCompareSame: "密码于用户名相同不被允许"
}, fv_scriptSrc = document.getElementsByTagName("script")[document.getElementsByTagName("script").length - 1].src;

}); 