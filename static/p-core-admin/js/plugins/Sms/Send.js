define(function(require,exports,module){
	var CountDownTime 	= 5;
	var J_SmsBtn		= $(myform).find("#J_SmsBtn");
	var SetTime			= CountDownTime
	exports.init = function(myform,url,register){
		
		J_SmsBtn.click(function() {
			var mpNumberRegex	= /^1[3|4|5|8|7][0-9]\d{8}$/;
			var mobileValue		= $(myform).find('#J_Mobile').val();
			var aliyunAFSCheck	= $(myform).find('input[name="aliyun_AFS_Check"]');
			if (!mobileValue){
				alert('请输入手机号码'); return;
			}
			if (!mpNumberRegex.test(mobileValue)){
				alert('手机号码格式不正确'); return;
			}
			
			if (aliyunAFSCheck.val()!='1'){
				alert('请先进行滑动验证'); return;
			}
			
			$.post(url,{
						'mobile'			: mobileValue,
						'aliyun_sessionid'	: $(myform).find("input[name='aliyun_sessionid']").val(),
						'aliyun_token'		: $(myform).find("input[name='aliyun_token']").val(),
						'aliyun_scene'		: $(myform).find("input[name='aliyun_scene']").val(),
						'aliyun_sig'		: $(myform).find("input[name='aliyun_sig']").val(),
						'register'			: register
				},function(data){
					aliyunAFS.reload($(myform));
					if (data.state == "1") {
						J_SmsBtn.attr("disabled",true).text('重新获取(' + SetTime + ')');
						window.interval = setInterval(function() {
							if (SetTime > 0) {
								J_SmsBtn.text("重新获取(" + (--SetTime) + ")");
								
							}else{
								clearInterval(interval);
								SetTime = CountDownTime;
								J_SmsBtn.attr("disabled", false).text("重新获取");
							}
						}, 1000);
						return;
					}else{
						alert(data.msg);
						return ;
					}
					
				})
		});
			
	};
			


});