KindEditor.plugin('link', function(K) {
	var self = this, name = 'link';
	self.plugin.link = {
		edit : function() {
			var html = '<div class="ke-dialog-body">' +
					//url
					'<div class="ke-dialog-row">' +
					'<label for="keUrl" style="width:100px;">URL</label>' +
					'<input class="ke-input-text" type="text" id="keUrl" name="url" value="" style="width:260px;" /></div>' +
					//type
					'<div class="ke-dialog-row"">' +
					'<label for="keType" style="width:100px;">打开类型</label>' +
					'<select id="keType" name="type"></select>' +
					'</div>' +
					'</div>',
			   dialog = art.dialog({
					id : name,
					padding:'20px',
					title : '插入连接',
					content: html,
					ok :  function(e) {
					        
						    var url = K.trim(urlBox.val());
							if (url == 'http://' || K.invalidUrl(url)) {
								alert('请输入有效的URL地址。');
								urlBox[0].focus();
								return;
							}
							self.exec('createlink', url, typeBox.val()).focus();
							this.close();
						}
					
				}),
				div = $(".ke-dialog-body"),
				urlBox = K('input[name="url"]', div),
				typeBox = K('select[name="type"]', div);
			urlBox.val('http://');
			typeBox[0].options[0] = new Option('新窗口', '_blank');
			typeBox[0].options[1] = new Option('当前窗口', '');
			self.cmd.selection();
			var a = self.plugin.getSelectedLink();
			if (a) {
				self.cmd.range.selectNode(a[0]);
				self.cmd.select();
				urlBox.val(a.attr('data-ke-src'));
				typeBox.val(a.attr('target'));
			}
			urlBox[0].focus();
			urlBox[0].select();
		},
		'delete' : function() {
			self.exec('unlink', null);
		}
	};
	self.clickToolbar(name, self.plugin.link.edit);
});
