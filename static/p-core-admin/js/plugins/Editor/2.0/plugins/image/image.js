KindEditor.plugin('image', function(K) {
	var self = this, name = 'image';
	self.plugin.imageDialog = function(options) {
		var clickFn = options.clickFn;
		var html = [
		    '<div class="upload-attach J_upload" id="J_upload_editor">',
			'<div class="upload-tip clearfix">',
			'<span id="J_buttonPlaceHolder_editor"></span>',
			'<span class="num J_upload_info">你还可以上传<em class="J_count">10</em>张图片</span>',
			'</div>',
			'<div class="upload-pictures J_upload_queue" >',
			'<div class="arrow"><em></em><span></span></div>',
			'<ul class="cc"><li class="J_pic_empty">1</li><li class="J_pic_empty">2</li><li class="J_pic_empty">3</li><li class="J_pic_empty">4</li><li class="J_pic_empty">5</li><li class="J_pic_empty">6</li><li class="J_pic_empty">7</li><li class="J_pic_empty">8</li><li class="J_pic_empty">9</li><li class="J_pic_empty">10</li></ul>',
			'</div>',
			'</div>'
		].join('');
	    self.bodyDiv = K('#J_upload_editor', self.div);
		var dialog = art.dialog({
			name : name,
			padding: '15px 12px 20px 15px',
			title : '批量上传图片',
			content : html,
			button:[
                     { 
					   value: '插入图片',
                       callback: function (e) {
					     var list = [];
						  K('.uploaded', self.bodyDiv).each(function() {
						    var img = $(K(this)).find('img');
						    list.push(img);
						  });
						  
                         return  clickFn.call(self,list);
						   
						 
                       },
                       focus: true
                     },
                     {
                      value: '取消',
					  callback: function () {
                        this.close();
						
                       }
                     }
                   ]
			
		});
        YJD.use("SwfUpload", function() {
            $('#J_upload_editor').swfupload({
                   id:'J_upload_editor'
            });  
           
        });
		return dialog;
	};
	


	
	
	self.clickToolbar(name, function() {
		self.plugin.imageDialog({
			clickFn : function (urlList) {
				if (urlList.length === 0) {
					return;
				}
				
				K.each(urlList, function(i, data) {
					self.exec('insertimage', data.attr('src'), data.attr('title'), '', '', 0, '');
				});
				
			}
		});
	});
});


