YJD.config({

    alias: {
        'Editor': 'plugins/Editor/2.0/Editor',
        'AutoComplete': 'plugins/AutoComplete/1.0/AutoComplete',
        'PlaceHolder': 'plugins/PlaceHolder/1.0/PlaceHolder',
        'AjaxForm': 'plugins/AjaxForm/1.0/AjaxForm',
        'Dialog': 'plugins/Dialog/6.0/dialog-plus.js',
        'DatePicker': 'plugins/DatePicker/1.0/DatePicker',
        'Bgiframe': 'plugins/Bgiframe/1.0/Bgiframe',
        'SwfUpload': 'plugins/SwfUpload/1.0/Init',
        'SwfUploadck': 'plugins/SwfUpload/1.0/Cookie',
        'FormValidator': 'plugins/FormValidator/Src/FormValidator',
        'Cookie': 'plugins/Cookie/1.0/Cookie',
        'Linkage': 'plugins/Linkage/1.0/Linkage',
        'Rotate': 'plugins/Rotate/1.0/Rotate',
        'DragDrop': 'plugins/DragDrop/1.0/DragDrop',
        'Position': 'plugins/Position/1.0/Position',
        'Chart': 'plugins/Chart/1.0/Chart',
        'Md5': 'plugins/Md5/1.0/Md5',
        /*App Admin*/
        'AppAdminCommon': 'admin/common',
        'AppAdminLogin': 'admin/adminLogin',
        'AppAdminMain': 'admin/main',
        'AppAdminCategory': 'admin/category',
        'AppAdminContent': 'admin/content'
    }
});