package com.ms.base.web.page;

import com.ms.base.comm.util.FrameUtil;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import com.ms.base.page.core.PageUtil;
import com.ms.base.web.mvc.WebUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <b>description</b>：默认分页按钮构建器 <br>
 * <b>time</b>：2018-08-17 13:05 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class DefaultPageHtmlModelBuilder implements IPageHtmlModelBuilder {
    /**
     * 分页结果对象
     */
    private PageModel pageModel;
    /**
     * 分页参数
     */
    private PageParamModel pageParamModel;
    /**
     * 分页链接的根路径
     */
    private String baseUrl;

    @Override
    public PageHtmlModel build() {
        PageHtmlModel<Object> pageHtmlModel = PageHtmlModel.builder().
                pageModel(pageModel).
                baseUrl(baseUrl).
                pageParamModel(pageParamModel).
                pageToolsModels(this.buildPageToolsModelList()).
                build();
        return pageHtmlModel;
    }

    /**
     * 获取分页页码集合
     *
     * @return
     */
    public List<PageToolsModel> buildPageToolsModelList() {
        if (Objects.isNull(this.pageModel.getDataList()) || this.pageModel.getDataList().isEmpty()) {
            return null;
        }
        List<PageToolsModel> result = FrameUtil.newArrayList();
        long pageCount = this.pageModel.getPageCount(), currentPage = this.pageModel.getCurrentPage();
        StringBuilder builder = new StringBuilder("");
        int pcsize = (int) pageCount;
        int cp = (int) currentPage;
        // 总共显示数量
        int pagersize = 11;
        // 两端数量
        int betweensize = 2;
        // 中间数量
        int middlesize = 5;
        if (pcsize <= pagersize) {
            for (int i = 1; i <= pcsize; i++) {
                result.add(this.buildPageToolsModel(i, i + ""));
            }
        } else {
            int betmaxsize = betweensize + middlesize;
            int endMaxSize = pcsize - betweensize - middlesize + 1;
            if (cp <= betmaxsize) {
                for (int i = 1; i <= betmaxsize + 1; i++) {
                    result.add(this.buildPageToolsModel(i, i + ""));
                }
                result.add(this.buildPageToolsModel(null, "..."));
                for (int i = pcsize - 1; i <= pcsize; i++) {
                    result.add(this.buildPageToolsModel(i, i + ""));
                }
            } else if (cp >= endMaxSize) {
                for (int i = 1; i <= betweensize; i++) {
                    result.add(this.buildPageToolsModel(i, i + ""));
                }
                result.add(this.buildPageToolsModel(null, "..."));
                for (int i = pcsize - betmaxsize; i <= pcsize; i++) {
                    result.add(this.buildPageToolsModel(i, i + ""));
                }
            } else {
                for (int i = 1; i <= betweensize; i++) {
                    result.add(this.buildPageToolsModel(i, i + ""));
                }
                result.add(this.buildPageToolsModel(null, "..."));
                for (int i = cp - betweensize; i <= cp + betweensize; i++) {
                    result.add(this.buildPageToolsModel(i, i + ""));
                }
                result.add(this.buildPageToolsModel(null, "..."));
                for (int i = pcsize - 1; i <= pcsize; i++) {
                    result.add(this.buildPageToolsModel(i, i + ""));
                }
            }
        }
        return result;
    }

    private PageToolsModel buildPageToolsModel(Integer pageIndex, String label) {
        String url = null;
        Map<String, Object> paramMap = this.pageParamModel.getParamMap();
        if (pageIndex != null) {
            paramMap.put(PageUtil.PAGE_KEY, pageIndex);
            url = WebUtil.getUrl(this.baseUrl, paramMap);
        }
        return PageToolsModel.builder().pageIndex(pageIndex).label(label).url(url).build();
    }

}
