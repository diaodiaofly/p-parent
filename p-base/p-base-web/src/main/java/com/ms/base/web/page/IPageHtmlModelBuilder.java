package com.ms.base.web.page;

/**
 * <b>description</b>：分页菜单构建器 <br>
 * <b>time</b>：2018-08-17 13:05 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public interface IPageHtmlModelBuilder {
    PageHtmlModel build();
}
