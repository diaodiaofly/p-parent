package com.ms.base.web.page;

import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * <b>description</b>：html分页数据模型 <br>
 * <b>time</b>：2018-08-17 12:59 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PageHtmlModel<T> implements Serializable{
    private static final long serialVersionUID = 1L;
    /**
     * 分页结果对象
     */
    private PageModel<T> pageModel;
    /**
     * 分页链接的根路径
     */
    private String baseUrl;
    /**
     * 分页参数
     */
    private PageParamModel pageParamModel;

    /**
     * 分页工具
     */
    private List<PageToolsModel> pageToolsModels;
}
