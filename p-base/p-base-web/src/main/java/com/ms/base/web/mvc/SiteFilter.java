package com.ms.base.web.mvc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <b>description</b>：web配置拦截器 <br>
 * <b>time</b>：2018-07-27 10:27 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Slf4j
public class SiteFilter extends OncePerRequestFilter implements Ordered {

    public static final String SITE_CONFIG = "SITE_CONFIG";
    private SiteFilterProperties siteConfigFilter;

    public SiteFilter(SiteFilterProperties siteConfigFilter) {
        this.siteConfigFilter = siteConfigFilter;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (this.siteConfigFilter != null && this.siteConfigFilter.isEnabled()) {
            request.setAttribute(SITE_CONFIG, this.siteConfigFilter.getConfig());
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 20;
    }
}
