package com.ms.base.web.mvc;

import java.util.Map;

/**
 * <b>description</b>：标准模块化controller接口 <br>
 * <b>time</b>：2018-08-15 13:22 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public interface IModuleViewController {

    /**
     * 获取viewname
     *
     * @param method
     * @return
     */
    default String getViewName(String method) {
        return WebUtil.getControllerViewName(moduleName(), controllerName(), method);
    }

    /**
     * 获取方法对应的完整路径
     *
     * @param method 方法名称
     * @return
     */
    default String getUrl(String method) {
        return WebUtil.getControllerUrl(moduleName(), controllerName(), method);
    }

    /**
     * 获取方法对应的完整路径
     *
     * @param method   方法名称
     * @param paramMap 请求参数，跟在?之后
     * @return
     */
    default String getUrl(String method, Map<String, Object> paramMap) {
        return WebUtil.getControllerUrl(moduleName(), controllerName(), method, paramMap);
    }

    /**
     * 获取方法对应的相对路径
     *
     * @param method 方法名称
     * @return
     */
    default String getPath(String method) {
        return WebUtil.getControllerPath(moduleName(), controllerName(), method);
    }


    /**
     * 模块名称
     *
     * @return
     */
    String moduleName();

    /**
     * controller名称
     */
    String controllerName();

}
