package com.ms.base.web.page;

import lombok.*;

import java.io.Serializable;

/**
 * <b>description</b>：分页按钮对象 <br>
 * <b>time</b>：2018-08-17 13:01 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PageToolsModel implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 请求的url
     */
    private String url;
    /**
     * 按钮文本
     */
    private String label;
    /**
     * 当前页码
     */
    private Integer pageIndex;

}
