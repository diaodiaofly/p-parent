package com.ms.base.web.interceptor;

import com.ms.base.comm.util.FrameUtil;
import com.ms.base.web.mvc.WebUtil;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author ready likun_557@163.com
 * @ClassName: BaseInterceptor
 * @Description: 拦截器基类
 * @date 2014-8-19 下午2:16:14
 */
public abstract class BaseInterceptor extends HandlerInterceptorAdapter {

    public final static String BASENOURLFILE = "basenourl.txt";

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        if (this.urlIsNeedValidate(WebUtil.getRequestUri(request))) {
            return true;
        } else {
            return this.doPreHandle(request, response, handler);
        }
    }

    public abstract boolean doPreHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception;

    /**
     * 判断url是否需要验证
     *
     * @param requestUri
     * @return
     */
    protected boolean urlIsNeedValidate(String requestUri) {
        List<String> list = FrameUtil.newArrayList(BASENOURLFILE);
        List<String> noUrlFiles = this.getNoUrlFiles();
        if (noUrlFiles != null && !noUrlFiles.isEmpty()) {
            list.addAll(noUrlFiles);
        }
        return FrameUtil.isValidate(list, requestUri);
    }

    protected List<String> getNoUrlFiles() {
        return null;
    }
}
