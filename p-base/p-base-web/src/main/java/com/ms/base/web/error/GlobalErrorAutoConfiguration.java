package com.ms.base.web.error;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * <b>description</b>：全局错误自动装配 <br>
 * <b>time</b>：2018-07-31 14:49 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Configuration
@ConditionalOnProperty(prefix = "ms.globalerror", name = "enabled", matchIfMissing = true)
@AutoConfigureBefore(ErrorMvcAutoConfiguration.class)
@EnableConfigurationProperties({ServerProperties.class, ResourceProperties.class, GlobalErrorConfigProperties.class})
@Slf4j
public class GlobalErrorAutoConfiguration {

    public GlobalErrorAutoConfiguration(){
        log.info("init");
    }

    @Bean
    public GlobalErrorController defaultErrorController(ErrorAttributes errorAttributes, ServerProperties serverProperties, ObjectProvider<List<ErrorViewResolver>> errorViewResolversProvider, GlobalErrorConfigProperties globalErrorConfigProperties) {
        return new GlobalErrorController(errorAttributes, serverProperties.getError(), errorViewResolversProvider.getIfAvailable(), globalErrorConfigProperties);
    }

    @Bean
    public GlobalErrorAttributes errorAttributes() {
        return new GlobalErrorAttributes();
    }

    @Bean
    public GlobalErrorViewResolver globalErrorViewResolver() {
        return new GlobalErrorViewResolver();
    }
}
