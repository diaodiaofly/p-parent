package com.ms.base.web.mvc;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.cloud.ResultUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.http.HttpServletRequest;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-08-14 14:16 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public class BaseViewController {

    @Value("${ms.web.wait.second:2}")
    private int waitSecond;
    public static String REQUEST_WAIT_SECONDS_KEY = "REQUEST_WAIT_SECONDS_KEY";
    @Value("${ms.web.result.viewname.success:other/success}")
    public String successViewName;
    @Value("${ms.web.result.viewname.error:other/error}")
    public String errorViewName;

    public static final String REFER_KEY = "refer";

    /**
     * 错误页面
     *
     * @param request 请求对象
     * @param msg     错误提示信息
     * @return
     */
    public String errorView(HttpServletRequest request, String msg) {
        return this.errorView(request, msg, null);
    }

    /**
     * 错误页面
     *
     * @param request 请求对象
     * @param path    跳转的相对路径
     * @return
     */
    public String errorView1(HttpServletRequest request, String path) {
        return this.errorView(request, null, path);
    }


    /**
     * 错误页面
     *
     * @param request 请求对象
     * @param msg     错误提示信息
     * @param path    跳转的相对路径
     * @return
     */
    public String errorView(HttpServletRequest request, String msg, String path) {
        ResultDto<Object> resultDto = ResultUtil.build(ResultUtil.ERROR_CODE, null, StringUtils.isBlank(msg) ? ResultUtil.ERROR_MSG : msg, null, null).putExtData(REQUEST_WAIT_SECONDS_KEY, waitSecond).putExtData("refer", WebUtil.getPathUrl(path));
        WebUtil.putResultDto(request, resultDto);
        return this.errorViewName;
    }

    /**
     * 成功页面
     *
     * @param request 请求对象
     * @param msg     成功提示信息
     * @return
     */
    public String successView(HttpServletRequest request, String msg) {
        return this.successView(request, msg, null);
    }

    /**
     * 成功页面
     *
     * @param request 请求对象
     * @param path    跳转的相对路径
     * @return
     */
    public String successView1(HttpServletRequest request, String path) {
        return this.successView(request, null, path);
    }

    /**
     * 成功页面
     *
     * @param request 请求对象
     * @param msg     成功提示信息
     * @param path    跳转的相对路径
     * @return
     */
    public String successView(HttpServletRequest request, String msg, String path) {
        ResultDto<Object> resultDto = ResultUtil.build(ResultUtil.SUCCESS_CODE, null, StringUtils.isBlank(msg) ? ResultUtil.SUCCESS_MSG : msg, null, null).putExtData(REQUEST_WAIT_SECONDS_KEY, waitSecond).putExtData(REFER_KEY, WebUtil.getPathUrl(path));
        WebUtil.putResultDto(request, resultDto);
        return this.successViewName;
    }

    /**
     * 成功
     *
     * @param msg 提示消息
     * @param <T>
     * @return
     */
    public <T> ResultDto<T> successResultDto(String msg) {
        return successPathResultDto(msg, null);
    }

    /**
     * 成功
     *
     * @param msg  提示消息
     * @param path 成功之后跳转的相对路径
     * @param <T>
     * @return
     */
    public <T> ResultDto<T> successPathResultDto(String msg, String path) {
        ResultDto resultDto = ResultUtil.build(ResultUtil.SUCCESS_CODE, null, StringUtils.isBlank(msg) ? ResultUtil.SUCCESS_MSG : msg, null, null).putExtData(REQUEST_WAIT_SECONDS_KEY, waitSecond).putExtData(REFER_KEY, WebUtil.getPathUrl(path));
        return resultDto;
    }

    /**
     * 成功
     *
     * @param msg 提示消息
     * @param url 成功之后跳转的绝对路径
     * @param <T>
     * @return
     */
    public <T> ResultDto<T> successUrlResultDto(String msg, String url) {
        ResultDto resultDto = ResultUtil.build(ResultUtil.SUCCESS_CODE, null, StringUtils.isBlank(msg) ? ResultUtil.SUCCESS_MSG : msg, null, null).putExtData(REQUEST_WAIT_SECONDS_KEY, waitSecond).putExtData(REFER_KEY, url);
        return resultDto;
    }

    public String getRootUrl(){
        return WebUtil.getRootUrl();
    }
}
