package com.ms.base.web.page;

import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;

/**
 * <b>description</b>：html分页工具 <br>
 * <b>time</b>：2018-08-17 13:45 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public class PageHtmlUtil {

    /**
     * 构建PageHtmlModel
     *
     * @param pageModel
     * @param pageParamModel
     * @param baseUrl
     * @param <T>
     * @return
     */
    public static <T> PageHtmlModel<T> build(PageModel<T> pageModel, PageParamModel pageParamModel, String baseUrl) {
        return new DefaultPageHtmlModelBuilder(pageModel, pageParamModel, baseUrl).build();
    }
}
