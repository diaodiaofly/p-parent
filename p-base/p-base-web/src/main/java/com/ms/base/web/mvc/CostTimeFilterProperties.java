package com.ms.base.web.mvc;

import lombok.Data;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-31 14:37 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
public class CostTimeFilterProperties {
    public static final String ENABLED = WebConfigProperties.COSTFILTER + ".enabled";
    /**
     * 当前filter是否可用
     */
    private boolean enabled = true;
}
