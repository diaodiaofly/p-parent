package com.ms.base.web.mvc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <b>description</b>：耗时拦截器 <br>
 * <b>time</b>：2018-07-27 10:27 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Slf4j
public class CostTimeFilter extends OncePerRequestFilter implements Ordered {

    private CostTimeFilterProperties costTimeFilterProperties;

    public CostTimeFilter(CostTimeFilterProperties costTimeFilterProperties) {
        this.costTimeFilterProperties = costTimeFilterProperties;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (this.costTimeFilterProperties != null && this.costTimeFilterProperties.isEnabled()) {
            long startTime = System.nanoTime();
            String requestURL = request.getRequestURL().toString();
            try {
                log.info(String.format("本次请求start[%s]", requestURL));
                filterChain.doFilter(request, response);
            } catch (Exception e) {
                throw e;
            } finally {
                double costTime = ((double) (System.nanoTime() - startTime)) / 1000000.00;
                log.info(String.format("本次请求end[%s]耗时(ms):%s", requestURL, costTime));
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 10;
    }
}
