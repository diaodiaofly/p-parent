package com.ms.base.web.mvc;

import com.ms.base.comm.spring.SpringContextHolder;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <b>description</b>：webconfig配置信息 <br>
 * <b>time</b>：2018-07-27 10:52 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@ConfigurationProperties(WebConfigProperties.PREFIX)
@Data
public class WebConfigProperties {
    public static final String PREFIX = "com.ms.base.web.mvc.webconfig";
    public static final String COSTFILTER = PREFIX + ".costfilter";
    public static final String SITEFILTER = PREFIX + ".sitefilter";
    private CostTimeFilterProperties costfilter = new CostTimeFilterProperties();
    private SiteFilterProperties sitefilter = new SiteFilterProperties();

    /**
     * 获取WebConfigProperties对象
     *
     * @return
     */
    public static WebConfigProperties webConfigProperties() {
        return SpringContextHolder.getBean(WebConfigProperties.class);
    }
}
