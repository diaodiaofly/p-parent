package com.ms.base.web.mvc;

import lombok.Data;

import java.util.Map;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-31 14:37 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
public class SiteFilterProperties {
    public static final String ENABLED = WebConfigProperties.SITEFILTER + ".enabled";
    public static final String CONFIG_ROOTURL = "rootUrl";
    /**
     * 当前filter是否可用
     */
    private boolean enabled = true;
    private Map<String, String> config;

    public String rootUrl() {
        return this.get(CONFIG_ROOTURL);
    }

    private String get(String configKey) {
        return this.config != null ? this.config.get(configKey) : null;
    }
}
