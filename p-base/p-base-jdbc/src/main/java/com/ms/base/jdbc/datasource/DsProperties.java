package com.ms.base.jdbc.datasource;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-08-02 14:40 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@ConfigurationProperties(prefix = DsProperties.PREFIX)
public class DsProperties {
    public static final String PREFIX = "com.ms.base.jdbc.datasource";
    private Class dsType;
}
