package com.ms.base.jdbc.datasource;

/**
 * <b>description</b>：数据源类型 <br>
 * <b>time</b>：2018-08-02 14:40 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public enum DsType {
    MASTER, SLAVE;

    public static DsType dsType(boolean master) {
        return master ? MASTER : SLAVE;
    }
}
