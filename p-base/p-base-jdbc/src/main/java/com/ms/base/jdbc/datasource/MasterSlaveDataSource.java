package com.ms.base.jdbc.datasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.lang.Nullable;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-26 13:09 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public class MasterSlaveDataSource extends AbstractRoutingDataSource {
    @Nullable
    @Override
    protected Object determineCurrentLookupKey() {
        return DsContextHolder.getDsType();
    }
}
