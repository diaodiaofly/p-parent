package com.ms.base.page.core;

import lombok.*;

import java.io.Serializable;
import java.util.Map;

/**
 * <b>description</b>：分页参数对象 <br>
 * <b>time</b>：2018-08-17 10:54 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PageParamModel implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 查询参数
     */
    private Map<String, Object> paramMap;
    /**
     * 第几页
     */
    private int page = 1;
    /**
     * 每页条数
     */
    private int rows = PageUtil.PAGE_SIZE;
}
