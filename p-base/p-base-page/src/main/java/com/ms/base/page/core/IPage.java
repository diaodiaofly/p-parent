package com.ms.base.page.core;

import java.util.List;
import java.util.Map;

/**
 * <b>description</b>：分页相关数据获取接口 <br>
 * <b>time</b>：2018-08-03 13:46 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public interface IPage {

    /**
     * 获取记录数
     *
     * @param map      查询参数
     * @param sqlMapId sqlMapId
     * @return
     * @throws Exception
     */
    long getPageCount(Map<String, Object> map, String sqlMapId) throws Exception;

    /**
     * 获取记录数
     *
     * @param map      查询参数
     * @param sqlMapId sqlMapId
     * @return
     * @throws Exception
     */
    List getPageList(Map<String, Object> map, String sqlMapId) throws Exception;
}
