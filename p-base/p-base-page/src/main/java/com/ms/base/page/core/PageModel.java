package com.ms.base.page.core;

import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * <b>description</b>：分页对象 <br>
 * <b>time</b>：2018-08-03 13:46 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@NoArgsConstructor
@ToString
public class PageModel<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 每页显示数量
     */
    private long pageSize = 20;
    /**
     * 当前页行的开始行的索引，如1,2,3....
     */
    private long startIndex;
    /**
     * 当前页行的结束索引
     */
    private long endIndex;
    /**
     * 当前页
     */
    private long currentPage;

    /**
     * 上一页索引
     */
    private long prePage;

    /**
     * 下一页索引
     */
    private long nextPage;

    /**
     * 总记录数
     */
    private long count;

    /**
     * 是否有上一页
     */
    private boolean hasPrePage;

    /**
     * 是否有下一页
     */
    private boolean hasNextPage;

    /**
     * 总页数
     */
    private long pageCount;

    /**
     * 数据集合
     */
    private List<T> dataList;

    public void setCurrentPage(long currentPage) {
        this.currentPage = currentPage;
        this.startIndex = (this.currentPage - 1) * this.pageSize + 1;
        this.endIndex = this.startIndex + this.pageSize - 1;
    }

    public void setCount(long count) {
        this.count = count;
        this.pageCount = (this.count % this.pageSize == 0) ? this.count
                / this.pageSize : this.count / this.pageSize + 1;
        if (this.currentPage >= this.pageCount) {
            this.setCurrentPage(this.pageCount);
            this.nextPage = this.pageCount;
            this.hasNextPage = false;
        } else {
            this.nextPage = this.currentPage + 1;
            this.hasNextPage = true;
        }
        if (this.currentPage <= 1) {
            this.prePage = 1;
            this.hasPrePage = false;
        } else {
            this.prePage = this.currentPage - 1;
            this.hasPrePage = true;
        }
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public long getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(long startIndex) {
        this.startIndex = startIndex;
    }

    public long getCurrentPage() {
        return currentPage;
    }

    public long getPrePage() {
        return prePage;
    }

    public void setPrePage(long prePage) {
        this.prePage = prePage;
    }

    public long getNextPage() {
        return nextPage;
    }

    public void setNextPage(long nextPage) {
        this.nextPage = nextPage;
    }

    public long getCount() {
        return count;
    }

    public boolean isHasPrePage() {
        return hasPrePage;
    }

    public void setHasPrePage(boolean hasPrePage) {
        this.hasPrePage = hasPrePage;
    }

    public boolean isHasNextPage() {
        return hasNextPage;
    }

    public void setHasNextPage(boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }

    public long getPageCount() {
        return pageCount;
    }

    public void setPageCount(long pageCount) {
        this.pageCount = pageCount;
    }

    public List<T> getDataList() {
        return dataList;
    }

    public void setDataList(List<T> dataList) {
        this.dataList = dataList;
    }

    public long getEndIndex() {
        if (endIndex >= this.getCount()) {
            this.endIndex = this.getCount();
        }
        return endIndex;
    }

    public void setEndIndex(long endIndex) {
        this.endIndex = endIndex;
    }
}
