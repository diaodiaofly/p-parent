package com.ms.base.comm.model;

import com.ms.base.comm.util.FrameUtil;

import java.io.Serializable;
import java.util.Map;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-08-17 14:00 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public class BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 扩展数据
     */
    private Map<Object, Object> extData;

    public Map<Object, Object> getExtData() {
        return extData;
    }

    public void setExtData(Map<Object, Object> extData) {
        this.extData = extData;
    }

    public void putExtData(Object key, Object value) {
        if (extData == null) {
            extData = FrameUtil.newLinkedHashMap();
        }
        extData.put(key, value);
    }
}
