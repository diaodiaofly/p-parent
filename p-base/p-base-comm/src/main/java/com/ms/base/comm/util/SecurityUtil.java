package com.ms.base.comm.util;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * <b>description</b>：加密相关方法 <br>
 * <b>time</b>：2018-08-21 09:56 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public final class SecurityUtil {

    /**
     * 获取固定长度的随机数字类型字符串
     *
     * @param length 字符串长度
     * @return
     */
    public static String getRandomNumber(int length) {
        if (length <= 0) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            sb.append((int) Math.floor(Math.random() * 9 + 1));
        }
        return sb.reverse().toString();
    }

    /**
     * md5加密，加密方式 = md5(data+salt)
     *
     * @param data 明文
     * @param salt 盐
     * @return 密文
     */
    public static String md5(String data, String salt) {
        return md5(data + salt);
    }

    /**
     * md5加密，加密方式 = md5(md5(target)+salt)
     *
     * @param target 明文
     * @param salt   盐
     * @return 密文
     */
    public static String md5_2(String target, String salt) {
        return md5(md5(target) + salt);
    }

    /**
     * md5加密，加密方式 = md5(data)
     *
     * @param data 明文
     * @return 密文
     */
    public static String md5(String data) {
        return DigestUtils.md5Hex(data);
    }
}
