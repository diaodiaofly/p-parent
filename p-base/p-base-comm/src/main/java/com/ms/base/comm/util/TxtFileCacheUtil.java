package com.ms.base.comm.util;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <b>description</b>：txt文件缓存 <br>
 * <b>time</b>：2018-08-22 16:34 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Slf4j
public class TxtFileCacheUtil {
    static class TxtModel {
        private long lastModifyTime;
        private List<String> strList;

        private TxtModel(long lastModifyTime, List<String> strList) {
            super();
            this.lastModifyTime = lastModifyTime;
            this.strList = strList;
        }

        public long getLastModifyTime() {
            return lastModifyTime;
        }

        public void setLastModifyTime(long lastModifyTime) {
            this.lastModifyTime = lastModifyTime;
        }

        public List<String> getStrList() {
            return strList;
        }

        public void setStrList(List<String> strList) {
            this.strList = strList;
        }
    }

    private static Map<String, TxtModel> proCachedMap = new HashMap<String, TxtModel>();

    /**
     * 获取某个txt中所有的数据，数据放在map中
     *
     * @param txtFile
     * @return
     */
    public static List<String> get(String txtFile) {
        File file = getFile(txtFile);
        TxtModel tm = proCachedMap.get(txtFile);
        if (tm != null && tm.getLastModifyTime() != file.lastModified()) {
            proCachedMap.remove(txtFile);
        }
        tm = proCachedMap.get(txtFile);
        if (tm == null) {
            synchronized (proCachedMap) {
                tm = proCachedMap.get(txtFile);
                if (tm == null) {
                    put(txtFile);
                    tm = proCachedMap.get(txtFile);
                }
            }
        }
        return tm == null ? null : tm.getStrList();
    }

    /**
     * 获取资源文件指定的文件
     *
     * @param propertiesFile
     * @return
     */
    public static File getFile(String propertiesFile) {
        return new File(TxtFileCacheUtil.class.getClassLoader().getResource(propertiesFile).getPath());
    }

    public static List<String> put(String file) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(TxtFileCacheUtil.class.getClassLoader().getResourceAsStream(file)));
            List<String> strings = new ArrayList<String>();
            String string = null;
            while ((string = reader.readLine()) != null) {
                strings.add(string);
            }
            proCachedMap.put(file, new TxtModel(getFile(file).lastModified(), strings));
            return strings;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
