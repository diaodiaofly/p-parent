package com.ms.base.comm.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-27 10:26 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Configuration
public class SpringAutoConfiguration {
    @Bean
    public SpringContextHolder springContextHolder() {
        return new SpringContextHolder();
    }
}
