package com.ms.base.comm.cloud;

import com.ms.base.comm.util.FrameUtil;
import lombok.Builder;
import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-26 15:28 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@Builder
public class ResultDto<T> {
    /**
     * 响应代码
     */
    private String code;
    /**
     * 子代码
     */
    private String subCode;
    /**
     * 信息提示
     */
    private String msg;
    /**
     * 数据
     */
    private T data;
    /**
     * 扩展数据
     */
    private Map extData;

    /**
     * 添加扩展数据
     *
     * @param key
     * @param value
     * @return
     */
    public ResultDto<T> putExtData(Object key, Object value) {
        if (this.extData == null) {
            this.extData = new LinkedHashMap<>();
        }
        this.extData.put(key, value);
        return this;
    }

    /**
     * 添加扩展数据
     *
     * @param map
     * @return
     */
    public ResultDto<T> putExtDatas(Map<Object, Object> map) {
        if (this.extData == null) {
            this.extData = new LinkedHashMap<>();
        }
        this.extData.putAll(map);
        return this;
    }

    /**
     * 添加扩展数据
     *
     * @param key
     * @param value
     * @return
     */
    public ResultDto<T> removeExtData(Object key, Object value) {
        if (this.extData != null) {
            this.extData.remove(key);
        }
        return this;
    }

    public Map<String, Object> map() throws Exception {
        return FrameUtil.javaBeanToMap(this);
    }

    /**
     * 获取结果，获取之前对结果进行校验
     *
     * @return
     */
    public ResultDto<T> ok() {
        return ResultUtil.ok(this);
    }

    /**
     * 获取data，获取之前对结果进行校验
     *
     * @return
     */
    public T okData() {
        return ResultUtil.okData(this);
    }
}
