package com.ms.base.comm.cloud;

import lombok.Builder;
import lombok.Data;

/**
 * <b>description</b>：请求信息 <br>
 * <b>time</b>：2018-08-16 09:26 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@Builder
public class RequestDto<T> {
    private T data;
}
