package com.ms.base.comm.db;

/**
 * <b>description</b>： 数据库类型<br>
 * <b>time</b>：2018-07-26 17:49 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public enum DbType {
    MYSQL, ORACLE, SQLSERVER;

    /**
     * 数据库是否是oracle
     *
     * @param dbType
     * @return
     */
    public static boolean isOracle(DbType dbType) {
        return ORACLE == dbType;
    }

    /**
     * 数据库是否是sqlserver
     *
     * @param dbType
     * @return
     */
    public static boolean isSqlserver(DbType dbType) {
        return SQLSERVER == dbType;
    }

    /**
     * 数据库是否是mysql
     *
     * @param dbType
     * @return
     */
    public static boolean isMysql(DbType dbType) {
        return MYSQL == dbType;
    }
}
