/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50525
Source Host           : localhost:3306
Source Database       : ms_master

Target Server Type    : MYSQL
Target Server Version : 50525
File Encoding         : 65001

Date: 2018-08-22 19:40:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `demo`
-- ----------------------------
DROP TABLE IF EXISTS `demo`;
CREATE TABLE `demo` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of demo
-- ----------------------------
INSERT INTO `demo` VALUES ('1', 'a');
INSERT INTO `demo` VALUES ('2', 'a');
INSERT INTO `demo` VALUES ('3', 'a');
INSERT INTO `demo` VALUES ('4', 'a');
INSERT INTO `demo` VALUES ('5', 'a');
INSERT INTO `demo` VALUES ('6', '30966a29-b584-422b-b18c-290ec442ad8a');
INSERT INTO `demo` VALUES ('7', '282758ca-cf75-4601-af76-31b8d0f3468a');
INSERT INTO `demo` VALUES ('14', 'ede418c3-8e85-4b8f-9413-5a2637b4fbd5');
INSERT INTO `demo` VALUES ('15', 'c405d01a-f5cc-4519-9ac4-aeebb1b346a559f358e1-a6ea-4041-bd20-fdccb1837e41');
INSERT INTO `demo` VALUES ('16', '181204bb-27f5-4ef9-99bf-7cf53b6514c6');
INSERT INTO `demo` VALUES ('17', '84d7d7f8-0986-4ec3-8d5c-3d57187f8e137ae7b5a6-9ef0-49de-8147-874e1573d6d9');
INSERT INTO `demo` VALUES ('18', 'd2ffe16b-ecf8-4a18-9910-ba7be27e9200');
INSERT INTO `demo` VALUES ('19', '2e1e4872-aea8-4dd9-97a4-446dab6c17f78d2cdb74-eaba-4ef5-a2c0-56c4e8e57064');
INSERT INTO `demo` VALUES ('20', '44585185-8725-4c53-9fe4-cfa8fd36b3bc');
INSERT INTO `demo` VALUES ('21', '4a980150-2c0a-4525-a8f7-2e0f02136cde8f463e08-c67a-4255-a96e-ae41ff7a9443');
INSERT INTO `demo` VALUES ('22', 'f64ddd3b-d756-40eb-8a76-ea9fddbdd7ac');
INSERT INTO `demo` VALUES ('23', 'b571eee0-626a-4740-b7f3-ba6ad7e9cbc0603743ff-68f0-4a7d-9869-938a612848b6');
INSERT INTO `demo` VALUES ('24', '9a5d9326-55de-4165-86cd-b649ad955992');
INSERT INTO `demo` VALUES ('25', '16b3ba95-70d1-49c7-a41b-6d57a1735fb93739fd31-f03d-4198-b0d9-b29d901ec9b7');
INSERT INTO `demo` VALUES ('26', 'c16cd9e9-1bc5-47a5-a990-4c9258017ca8');
INSERT INTO `demo` VALUES ('27', '49288ecf-2d32-47ae-b158-74bea7d1e28e778e2ce9-11bc-4150-9232-18f987dcac17');
INSERT INTO `demo` VALUES ('28', '24c7c9b5-ee6d-4f4c-aaee-19b8bbd70195');
INSERT INTO `demo` VALUES ('29', '3df5deeb-fe60-429f-aa65-a1b0c9c17681');
INSERT INTO `demo` VALUES ('30', 'ce1a1c60-aff1-4f2a-8a30-9fccd3eb474c');
INSERT INTO `demo` VALUES ('31', '1a71bed8-2b65-4284-bce6-9edeb3d2ae8f');
INSERT INTO `demo` VALUES ('32', '3b402005-dba1-4a58-ab2b-1cefb1aaa0b0');
INSERT INTO `demo` VALUES ('33', '08f9e515-7816-48c2-95ea-17c370a51a9f');
INSERT INTO `demo` VALUES ('34', '133bc7a3-4ce8-40f6-98fd-cd90fc3ecffc');
INSERT INTO `demo` VALUES ('35', 'dd416339-c9d8-484d-9212-8ecdb63385f8');
INSERT INTO `demo` VALUES ('36', '88cb838b-15a2-431e-a974-e64e6af942ff');
INSERT INTO `demo` VALUES ('37', '5c530ca3-2827-4e5c-ab38-39046309e24b');
INSERT INTO `demo` VALUES ('38', 'b70966a6-c1b6-472f-9de9-b19146de3569');
INSERT INTO `demo` VALUES ('39', 'c353c841-79dc-4216-8dfd-5e39c7ba7b1f');
INSERT INTO `demo` VALUES ('40', 'a0c8dd38-d69c-4e3b-9da7-35ec0b5ac7ca');
INSERT INTO `demo` VALUES ('41', 'bcbd635d-75b2-4cc4-a64c-052b05132a96');
INSERT INTO `demo` VALUES ('42', 'b0f21afc-b790-4f4f-bf1f-c19abe4be0e7');
INSERT INTO `demo` VALUES ('43', '96d6caaf-68d3-4938-a443-e0720cb4cdf0');
INSERT INTO `demo` VALUES ('44', 'f2bb44ff-6d0d-49be-9363-9bf760fc0c13');
INSERT INTO `demo` VALUES ('45', 'b2ab259e-4e26-48af-a6d7-a6edbec65ba2');
INSERT INTO `demo` VALUES ('46', 'c368ee52-81b9-42bc-8716-c800c227b6cf');
INSERT INTO `demo` VALUES ('47', 'b15155a2-4711-48d5-a979-970cb01dc7e9');
INSERT INTO `demo` VALUES ('48', '8dab0934-d341-4438-9351-778cdd9b9d02');
INSERT INTO `demo` VALUES ('49', '92f529be-7686-4460-bf14-b6f8715ca4d5');
INSERT INTO `demo` VALUES ('50', 'dbfdfd87-4ba9-477a-aeca-14d4a674c890');
INSERT INTO `demo` VALUES ('51', 'ceb1ad89-93d9-4be6-ac92-0caecba93254');
INSERT INTO `demo` VALUES ('52', 'a78043a9-7bed-4533-bafe-c9e57c18f880');
INSERT INTO `demo` VALUES ('53', '835807e4-63f4-47e9-b5af-bbedc817f36a');
INSERT INTO `demo` VALUES ('54', 'becccc6a-abca-4d15-841b-454ea88c7a46');
INSERT INTO `demo` VALUES ('55', '4072f71c-c0c5-44fc-960e-5dd464be0565');
INSERT INTO `demo` VALUES ('56', 'bea591db-6427-4e46-8d54-044c98e66a28');
INSERT INTO `demo` VALUES ('57', '44355cd6-47fc-4310-b1e7-158658759562');
INSERT INTO `demo` VALUES ('58', '4078e936-c50f-4a64-aa89-561aa8547809');
INSERT INTO `demo` VALUES ('59', 'e6ca31c6-8e2d-4f85-b625-3a6c99e8c8e8');
INSERT INTO `demo` VALUES ('60', 'da0756ea-bed2-4012-8108-8acb6eeac77c');
INSERT INTO `demo` VALUES ('61', 'fd5bbc6a-6398-4b0c-a538-53803de6f548');
INSERT INTO `demo` VALUES ('62', 'a323acfc-1fc2-4483-a933-fcfd8ed9a7b6');
INSERT INTO `demo` VALUES ('63', 'ffb8a226-6fa1-4987-9222-618559b1d270');
INSERT INTO `demo` VALUES ('64', '6cd646ef-c168-4af5-b75b-e736c5b81649');
INSERT INTO `demo` VALUES ('65', 'cdb700d1-1149-4041-99e0-aabd90a88e94');
INSERT INTO `demo` VALUES ('66', '08db726b-b6ff-48de-8c61-44fb3cb9f762');
INSERT INTO `demo` VALUES ('67', '2d12efba-50b4-4073-9742-b3a694c7b21b');
INSERT INTO `demo` VALUES ('68', '1ec0dec7-4f14-405f-85ae-9f24e2de904d');
INSERT INTO `demo` VALUES ('69', '123');
INSERT INTO `demo` VALUES ('70', 'string');

-- ----------------------------
-- Table structure for `t_admin`
-- ----------------------------
DROP TABLE IF EXISTS `t_admin`;
CREATE TABLE `t_admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '用户名',
  `mobile` varchar(11) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '手机号',
  `password` varchar(128) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '密码',
  `secretkey` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '密码加密秘钥',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否是管理员,1:是，0：否',
  `last_login_ip` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `last_login_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='后台管理员表';

-- ----------------------------
-- Records of t_admin
-- ----------------------------
INSERT INTO `t_admin` VALUES ('3', 'admin', '15800926537', '9c6de63fefcf40f301cfc2ede9dd0e96', '659221', '1', '', '0');
INSERT INTO `t_admin` VALUES ('4', 'ready', '18888888888', 'fb61b5dd72d3f2532cf504e91e0f2f77', '956443', '0', '', '0');
INSERT INTO `t_admin` VALUES ('5', 'tom', '12345678901', '9c6de63fefcf40f301cfc2ede9dd0e96', '355132', '0', '', '0');
INSERT INTO `t_admin` VALUES ('6', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('7', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('8', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('9', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('10', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('11', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('12', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('13', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('14', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('15', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('16', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('17', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('18', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('19', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('20', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('21', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('22', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('23', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('24', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('25', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('26', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('27', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '', '0', '', '0');
INSERT INTO `t_admin` VALUES ('28', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '777856', '1', '', '0');
INSERT INTO `t_admin` VALUES ('29', 'ready', '', '9c6de63fefcf40f301cfc2ede9dd0e96', '266415', '1', '', '0');
INSERT INTO `t_admin` VALUES ('30', 'ready', '15800926537', '9c6de63fefcf40f301cfc2ede9dd0e96', '248853', '1', '', '0');
INSERT INTO `t_admin` VALUES ('38', 'ready12321321', '15800926537', '9c6de63fefcf40f301cfc2ede9dd0e96', '516922', '1', '', '0');
INSERT INTO `t_admin` VALUES ('39', 'ready111', '15800926537', '9c6de63fefcf40f301cfc2ede9dd0e96', '479435', '1', '', '0');

-- ----------------------------
-- Table structure for `t_admin_menu`
-- ----------------------------
DROP TABLE IF EXISTS `t_admin_menu`;
CREATE TABLE `t_admin_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '菜单名称',
  `pid` bigint(20) NOT NULL DEFAULT '0' COMMENT '父菜单id',
  `the_sort` int(3) NOT NULL DEFAULT '1' COMMENT '同级排序',
  `url` varchar(256) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'url',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='后台菜单表';

-- ----------------------------
-- Records of t_admin_menu
-- ----------------------------
INSERT INTO `t_admin_menu` VALUES ('1', '系统', '0', '1', '');
INSERT INTO `t_admin_menu` VALUES ('2', '管理员', '1', '1', '/admin/admin/listView');
INSERT INTO `t_admin_menu` VALUES ('3', '角色组', '1', '2', '/admin/adminRole/listView');
INSERT INTO `t_admin_menu` VALUES ('4', '系统菜单', '1', '3', '/admin/adminMenu/listView');
INSERT INTO `t_admin_menu` VALUES ('5', '爱电影', '1', '4', 'http://ady01.com');

-- ----------------------------
-- Table structure for `t_admin_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_admin_role`;
CREATE TABLE `t_admin_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '名称',
  `description` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '角色描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='后台角色表';

-- ----------------------------
-- Records of t_admin_role
-- ----------------------------
INSERT INTO `t_admin_role` VALUES ('1', '管理员', '具有最高权限');
INSERT INTO `t_admin_role` VALUES ('2', '管理员1', '');
INSERT INTO `t_admin_role` VALUES ('3', 'admin2', 'admin2');
INSERT INTO `t_admin_role` VALUES ('4', 'admin3', '');
INSERT INTO `t_admin_role` VALUES ('5', 'admin4', '');
INSERT INTO `t_admin_role` VALUES ('6', 'admin5', '');
INSERT INTO `t_admin_role` VALUES ('7', 'admin6', '');
INSERT INTO `t_admin_role` VALUES ('8', 'admin7', '');
INSERT INTO `t_admin_role` VALUES ('9', 'admin8', '');
INSERT INTO `t_admin_role` VALUES ('10', '运维', '运维专属角色');

-- ----------------------------
-- Table structure for `t_admin_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `t_admin_role_menu`;
CREATE TABLE `t_admin_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `role_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '角色id',
  `menu_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '菜单id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='角色菜单关联表';

-- ----------------------------
-- Records of t_admin_role_menu
-- ----------------------------
INSERT INTO `t_admin_role_menu` VALUES ('1', '1', '2');
INSERT INTO `t_admin_role_menu` VALUES ('2', '1', '3');
INSERT INTO `t_admin_role_menu` VALUES ('3', '1', '4');
INSERT INTO `t_admin_role_menu` VALUES ('4', '1', '5');
INSERT INTO `t_admin_role_menu` VALUES ('5', '2', '2');
INSERT INTO `t_admin_role_menu` VALUES ('6', '2', '4');
INSERT INTO `t_admin_role_menu` VALUES ('7', '10', '2');
INSERT INTO `t_admin_role_menu` VALUES ('8', '10', '3');
INSERT INTO `t_admin_role_menu` VALUES ('9', '10', '4');
INSERT INTO `t_admin_role_menu` VALUES ('10', '10', '5');

-- ----------------------------
-- Table structure for `t_admin_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_admin_user_role`;
CREATE TABLE `t_admin_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `role_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '角色id',
  `admin_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '后台用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户角色关联表';

-- ----------------------------
-- Records of t_admin_user_role
-- ----------------------------
INSERT INTO `t_admin_user_role` VALUES ('62', '1', '3');
INSERT INTO `t_admin_user_role` VALUES ('63', '2', '3');
INSERT INTO `t_admin_user_role` VALUES ('64', '3', '3');
INSERT INTO `t_admin_user_role` VALUES ('65', '4', '3');
INSERT INTO `t_admin_user_role` VALUES ('66', '5', '3');
INSERT INTO `t_admin_user_role` VALUES ('67', '6', '3');
INSERT INTO `t_admin_user_role` VALUES ('68', '7', '3');
INSERT INTO `t_admin_user_role` VALUES ('69', '8', '3');
INSERT INTO `t_admin_user_role` VALUES ('70', '9', '3');
INSERT INTO `t_admin_user_role` VALUES ('71', '10', '3');
INSERT INTO `t_admin_user_role` VALUES ('73', '2', '4');

DROP TABLE IF EXISTS `demo`;
CREATE TABLE `demo` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
INSERT INTO `demo` VALUES ('1', 'a');
INSERT INTO `demo` VALUES ('2', 'b');
INSERT INTO `demo` VALUES ('3', 'c');
INSERT INTO `demo` VALUES ('4', 'd');
INSERT INTO `demo` VALUES ('5', 'e');