export JAVA_HOME=/usr/java/jdk1.8.0_151
export M2_HOME=/data/maven/apache-maven-3.3.9
export CLASSPATH=$JAVA_HOME/lib/
export PATH=$JAVA_HOME/bin:$M2_HOME/bin:$PATH
export PATH JAVA_HOME CLASSPATH
cd /data/code/p-parent/
mvn install -Dmaven.test.skip=true

nohup java -jar ./p-cloud/p-cloud-discovery/target/p-cloud-discovery-0.0.1-SNAPSHOT.jar &
sleep 10
nohup java -jar ./p-core/p-core-service/target/p-core-service-0.0.1-SNAPSHOT.jar &
sleep 10
nohup java -jar ./p-core/p-core-admin/target/p-core-admin-0.0.1-SNAPSHOT.jar &


cd /data/code/p-parent/p-core/p-core-admin/src/main/resources/

/data/redis-4.0.2/src
./redis-server ../redis.conf