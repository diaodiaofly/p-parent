package com.ms.core.api.admin;

import com.ms.core.api.ApiConstant;
import com.ms.core.comm.admin.controller.IAdminRoleController;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * <b>description</b>：后台角色表 相关操作Feign客户端 <br>
 * <b>time</b>：2018-08-21 09:15:41 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@FeignClient(value = ApiConstant.SERVICE_ID)
public interface AdminRoleClient extends IAdminRoleController {
}