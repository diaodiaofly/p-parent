package com.ms.core.api.admin;

import com.ms.core.api.ApiConstant;
import com.ms.core.comm.admin.controller.IAdminUserRoleController;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * <b>description</b>：用户角色关联表 相关操作Feign客户端 <br>
 * <b>time</b>：2018-08-21 09:16:12 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@FeignClient(value = ApiConstant.SERVICE_ID)
public interface AdminUserRoleClient extends IAdminUserRoleController {
}