package com.ms.core.api.admin;

import com.ms.core.api.ApiConstant;
import com.ms.core.comm.admin.controller.IDemoController;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * <b>description</b>：DemoController客户端 <br>
 * <b>time</b>：2018-07-27 13:41 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@FeignClient(value = ApiConstant.SERVICE_ID)
public interface DemoClient extends IDemoController {

}