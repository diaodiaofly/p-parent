package com.ms.core.service.admin.dao;

import com.ms.base.jdbc.dao.IBaseDao;
import com.ms.core.comm.admin.model.AdminUserRoleModel;

/**
 * <b>description</b>：用户角色关联表数据访问接口 <br>
 * <b>time</b>：2018-08-21 09:16:12 <br>
 * <b>author</b>：ready likun_557@163.com
 */
public interface IAdminUserRoleDao extends IBaseDao<java.lang.Long, AdminUserRoleModel> {
}