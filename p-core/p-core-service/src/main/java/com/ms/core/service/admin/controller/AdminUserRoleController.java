package com.ms.core.service.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.cloud.ResultUtil;
import com.ms.base.comm.util.Constant;
import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import com.ms.base.page.core.PageUtil;
import com.ms.core.comm.admin.controller.IAdminUserRoleController;
import com.ms.core.comm.admin.model.AdminUserRoleModel;
import com.ms.core.service.admin.service.IAdminUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <b>description</b>：用户角色关联表 http接口 <br>
 * <b>time</b>：2018-08-21 09:16:12 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@RestController
public class AdminUserRoleController implements IAdminUserRoleController {

    @Autowired
    private IAdminUserRoleService adminUserRoleService;

    @Override
    public ResultDto<AdminUserRoleModel> insert(@RequestBody AdminUserRoleModel model) throws Exception {
        return ResultUtil.successData(this.adminUserRoleService.insert(model));
    }

    @Override
    public ResultDto<Integer> update(@RequestBody AdminUserRoleModel model) throws Exception {
        return ResultUtil.successData(this.adminUserRoleService.update(model));
    }

    @Override
    public ResultDto<Integer> deleteById(@RequestParam("id") long id) throws Exception {
        return ResultUtil.successData(this.adminUserRoleService.deleteById(id));
    }

    @Override
    public ResultDto<Long> getModelListCount(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminUserRoleService.getModelListCount(map, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminUserRoleModel>> getModelList(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminUserRoleService.getModelList(map, DsType.dsType(master)));
    }

    @Override
    public ResultDto<AdminUserRoleModel> getModelById(@RequestParam("id") long id, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminUserRoleService.getModelById(id, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminUserRoleModel>> getModelsByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminUserRoleService.getModelsByIds(idList, DsType.dsType(master)));
    }

    @Override
    public ResultDto<Map<Long, AdminUserRoleModel>> getModelMapByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminUserRoleService.getModelMapByIds(idList, DsType.dsType(master)));
    }

    @Override
    public ResultDto<AdminUserRoleModel> getModelOne(@RequestBody Map<String, Object> paramMap, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminUserRoleService.getModelOne(paramMap, DsType.dsType(master)));
    }

    @Override
    public ResultDto<PageModel<AdminUserRoleModel>> getPageModel(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.PAGE_KEY) int page, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminUserRoleService.getPageModel(map, page, rows, DsType.dsType(master)));
    }

    @Override
    public ResultDto<PageModel<AdminUserRoleModel>> getPageModel1(@RequestBody PageParamModel pageParamModel, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminUserRoleService.getPageModel1(pageParamModel, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminUserRoleModel>> getModelList1(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.SKIP_KEY) int skip, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminUserRoleService.getModelList1(map, skip, rows, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminUserRoleModel>> getListByAdminId(@RequestParam(AdminUserRoleModel.ADMIN_ID_COL) long admin_id, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminUserRoleService.getListByAdminId(admin_id, DsType.dsType(master)));
    }
}