package com.ms.core.service.admin.bus;

import com.ms.core.comm.admin.dto.AdminLoginRequest;
import com.ms.core.comm.admin.dto.AdminLoginResponse;

/**
 * <b>description</b>：管理员登录业务接口 <br>
 * <b>time</b>：2018-08-23 13:50 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public interface IAdminLoginBus {
    /**
     * 登录
     *
     * @param request
     * @return
     * @throws Exception
     */
    AdminLoginResponse login(AdminLoginRequest request) throws Exception;
}
