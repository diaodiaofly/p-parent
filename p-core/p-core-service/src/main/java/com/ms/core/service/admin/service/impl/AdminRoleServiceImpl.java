package com.ms.core.service.admin.service.impl;

import com.ms.base.comm.util.FrameUtil;
import com.ms.base.jdbc.dao.IBaseDao;
import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.jdbc.service.BaseServiceImpl;
import com.ms.core.comm.admin.dto.SaveAdminRoleRequest;
import com.ms.core.comm.admin.model.AdminRoleMenuModel;
import com.ms.core.comm.admin.model.AdminRoleModel;
import com.ms.core.service.admin.dao.IAdminRoleDao;
import com.ms.core.service.admin.service.IAdminRoleMenuService;
import com.ms.core.service.admin.service.IAdminRoleService;
import com.ms.core.service.admin.service.IAdminUserRoleService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * <b>description</b>：后台角色表业务实现类 <br>
 * <b>time</b>：2018-08-21 09:15:41 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Service
public class AdminRoleServiceImpl extends BaseServiceImpl<java.lang.Long, AdminRoleModel> implements IAdminRoleService {
    @Autowired
    private IAdminRoleDao adminRoleDao;
    @Autowired
    private IAdminRoleMenuService adminRoleMenuService;
    @Autowired
    private IAdminUserRoleService adminUserRoleService;

    @Override
    public IBaseDao<java.lang.Long, AdminRoleModel> getBaseDao() {
        return this.adminRoleDao;
    }

    @Transactional
    public AdminRoleModel save(SaveAdminRoleRequest dto) throws Exception {
        Assert.notNull(dto, "dto is not null!");
        Assert.notNull(dto.getModel(), "dto is not null!");
        Long id = null;
        if (dto.getModel().getId() == null) {
            //新增
            Assert.isTrue(StringUtils.isNotBlank(dto.getModel().getName()), "角色名不能为空!");
            AdminRoleModel model = this.getModelByName(dto.getModel().getName(), null);
            Assert.isTrue(model == null, "角色已存在!");
            id = this.insert(dto.getModel()).getId();
        } else {
            //修改
            Assert.isTrue(StringUtils.isNotBlank(dto.getModel().getName()), "角色名不能为空!");
            AdminRoleModel model = this.getModelByName(dto.getModel().getName(), null);
            Assert.isTrue(model == null || model.getId().equals(dto.getModel().getId()), "角色已存在!");
            this.update(dto.getModel());
            id = dto.getModel().getId();

            //删除关联菜单信息
            this.adminRoleMenuService.deleteByRoleId(id);
        }
        List<AdminRoleMenuModel> adminRoleMenuModelList = FrameUtil.newArrayList();
        if (dto.getMenuIdList() != null) {
            for (Long menuId : dto.getMenuIdList()) {
                if (menuId != null) {
                    adminRoleMenuModelList.add(AdminRoleMenuModel.builder().role_id(id).menu_id(menuId).build());
                }
            }
        }
        //插入角色关联的菜单信息
        this.adminRoleMenuService.insertBatch(adminRoleMenuModelList);

        return this.getModelById(id, DsType.MASTER);
    }

    @Override
    public int deleteById(Long id) throws Exception {
        this.adminRoleMenuService.deleteByRoleId(id);
        this.adminUserRoleService.deleteByRoleId(id);
        return super.deleteById(id);
    }

    @Override
    public AdminRoleModel getModelByName(String name, DsType dsType) throws Exception {
        return this.getModelOne(FrameUtil.newHashMap("name", name), dsType);
    }
}