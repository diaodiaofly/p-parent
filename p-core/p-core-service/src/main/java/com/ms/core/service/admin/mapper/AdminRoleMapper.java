package com.ms.core.service.admin.mapper;

import com.ms.base.jdbc.dao.IBaseMapper;
import com.ms.core.comm.admin.model.AdminRoleModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * <b>description</b>：后台角色表mapper <br>
 * <b>time</b>：2018-08-21 09:15:41 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Mapper
public interface AdminRoleMapper extends IBaseMapper<AdminRoleModel> {
}