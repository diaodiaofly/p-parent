package com.ms.core.service.admin.dao;

import com.ms.base.jdbc.dao.IBaseDao;
import com.ms.core.comm.admin.model.AdminMenuModel;

/**
 * <b>description</b>：后台菜单表数据访问接口 <br>
 * <b>time</b>：2018-08-21 09:15:21 <br>
 * <b>author</b>：ready likun_557@163.com
 */
public interface IAdminMenuDao extends IBaseDao<java.lang.Long, AdminMenuModel> {
}