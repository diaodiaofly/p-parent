package com.ms.core.service.admin.dao.impl;

import com.ms.base.jdbc.dao.BaseDaoImpl;
import com.ms.base.jdbc.dao.IBaseMapper;
import com.ms.core.comm.admin.model.AdminUserRoleModel;
import com.ms.core.service.admin.dao.IAdminUserRoleDao;
import com.ms.core.service.admin.mapper.AdminUserRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <b>description</b>：用户角色关联表数据访问实现类 <br>
 * <b>time</b>：2018-08-21 09:16:12 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Component
public class AdminUserRoleDaoImpl extends BaseDaoImpl<java.lang.Long, AdminUserRoleModel> implements IAdminUserRoleDao {
    @Autowired
    private AdminUserRoleMapper adminUserRoleMapper;

    @Override
    protected String getPrimaryKeyName() {
        return "id";
    }

    @Override
    public IBaseMapper<AdminUserRoleModel> getBaseMapper() {
        return this.adminUserRoleMapper;
    }
}