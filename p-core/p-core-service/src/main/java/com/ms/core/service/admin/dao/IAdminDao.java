package com.ms.core.service.admin.dao;

import com.ms.base.jdbc.dao.IBaseDao;
import com.ms.core.comm.admin.model.AdminModel;

/**
 * <b>description</b>：后台管理员数据访问接口 <br>
 * <b>time</b>：2018-08-15 18:21:11 <br>
 * <b>author</b>：ready likun_557@163.com
 */
public interface IAdminDao extends IBaseDao<Long, AdminModel> {
}