package com.ms.core.service.admin.service;

import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.jdbc.service.IBaseService;
import com.ms.core.comm.admin.dto.SaveAdminRoleRequest;
import com.ms.core.comm.admin.model.AdminRoleModel;

/**
 * <b>description</b>：后台角色表业务接口 <br>
 * <b>time</b>：2018-08-21 09:15:41 <br>
 * <b>author</b>：ready likun_557@163.com
 */
public interface IAdminRoleService extends IBaseService<java.lang.Long, AdminRoleModel> {
    /**
     * 保存(新增&修改)，返回保存之后的数据
     *
     * @param dto
     * @return
     * @throws Exception
     */
    AdminRoleModel save(SaveAdminRoleRequest dto) throws Exception;

    /**
     * 根据角色名获取角色信息
     *
     * @param name   用户名
     * @param dsType
     * @return
     * @throws Exception
     */
    AdminRoleModel getModelByName(String name, DsType dsType) throws Exception;

}