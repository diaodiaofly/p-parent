package com.ms.core.service.admin.dao;

import com.ms.base.jdbc.dao.IBaseDao;
import com.ms.core.comm.admin.model.AdminRoleModel;

/**
 * <b>description</b>：后台角色表数据访问接口 <br>
 * <b>time</b>：2018-08-21 09:15:41 <br>
 * <b>author</b>：ready likun_557@163.com
 */
public interface IAdminRoleDao extends IBaseDao<java.lang.Long, AdminRoleModel> {
}