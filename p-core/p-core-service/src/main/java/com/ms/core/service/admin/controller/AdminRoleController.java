package com.ms.core.service.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.cloud.ResultUtil;
import com.ms.base.comm.util.Constant;
import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import com.ms.base.page.core.PageUtil;
import com.ms.core.comm.admin.controller.IAdminRoleController;
import com.ms.core.comm.admin.dto.SaveAdminRoleRequest;
import com.ms.core.comm.admin.model.AdminRoleModel;
import com.ms.core.service.admin.service.IAdminRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <b>description</b>：后台角色表 http接口 <br>
 * <b>time</b>：2018-08-21 09:15:41 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@RestController
public class AdminRoleController implements IAdminRoleController {

    @Autowired
    private IAdminRoleService adminRoleService;

    @Override
    public ResultDto<AdminRoleModel> save(@RequestBody SaveAdminRoleRequest dto) throws Exception {
        return ResultUtil.successData(this.adminRoleService.save(dto));
    }

    @Override
    public ResultDto<Integer> deleteById(@RequestParam("id") long id) throws Exception {
        return ResultUtil.successData(this.adminRoleService.deleteById(id));
    }

    @Override
    public ResultDto<Long> getModelListCount(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleService.getModelListCount(map, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminRoleModel>> getModelList(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleService.getModelList(map, DsType.dsType(master)));
    }

    @Override
    public ResultDto<AdminRoleModel> getModelById(@RequestParam("id") long id, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleService.getModelById(id, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminRoleModel>> getModelsByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleService.getModelsByIds(idList, DsType.dsType(master)));
    }

    @Override
    public ResultDto<Map<Long, AdminRoleModel>> getModelMapByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleService.getModelMapByIds(idList, DsType.dsType(master)));
    }

    @Override
    public ResultDto<AdminRoleModel> getModelOne(@RequestBody Map<String, Object> paramMap, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleService.getModelOne(paramMap, DsType.dsType(master)));
    }

    @Override
    public ResultDto<PageModel<AdminRoleModel>> getPageModel(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.PAGE_KEY) int page, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleService.getPageModel(map, page, rows, DsType.dsType(master)));
    }

    @Override
    public ResultDto<PageModel<AdminRoleModel>> getPageModel1(@RequestBody PageParamModel pageParamModel, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleService.getPageModel1(pageParamModel, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminRoleModel>> getModelList1(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.SKIP_KEY) int skip, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleService.getModelList1(map, skip, rows, DsType.dsType(master)));
    }
}