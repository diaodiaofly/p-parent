package com.ms.core.service.admin.bus.impl;

import com.ms.base.comm.util.FrameUtil;
import com.ms.base.jdbc.datasource.DsType;
import com.ms.core.comm.admin.dto.AdminLoginRequest;
import com.ms.core.comm.admin.dto.AdminLoginResponse;
import com.ms.core.comm.admin.model.AdminMenuModel;
import com.ms.core.comm.admin.model.AdminModel;
import com.ms.core.comm.admin.model.AdminRoleMenuModel;
import com.ms.core.comm.admin.model.AdminUserRoleModel;
import com.ms.core.comm.admin.util.AdminUtil;
import com.ms.core.service.admin.bus.IAdminLoginBus;
import com.ms.core.service.admin.service.IAdminMenuService;
import com.ms.core.service.admin.service.IAdminRoleMenuService;
import com.ms.core.service.admin.service.IAdminService;
import com.ms.core.service.admin.service.IAdminUserRoleService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <b>description</b>：管理员登录业务实现 <br>
 * <b>time</b>：2018-08-23 13:51 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Component
public class AdminLoginBusImpl implements IAdminLoginBus {
    @Autowired
    private IAdminService adminService;
    @Autowired
    private IAdminUserRoleService adminUserRoleService;
    @Autowired
    private IAdminRoleMenuService adminRoleMenuService;
    @Autowired
    private IAdminMenuService adminMenuService;

    @Override
    public AdminLoginResponse login(AdminLoginRequest request) throws Exception {

        String name = request.getName();
        String password = request.getPassword();
        Assert.isTrue(StringUtils.isNotBlank(name), "登录名不能为空!");
        Assert.isTrue(StringUtils.isNotBlank(password), "密码不能为空!");

        AdminModel adminModel = this.adminService.getModelByName(name, DsType.SLAVE);
        Assert.isTrue(AdminUtil.validatePassword(adminModel, password), "用户名或密码有误!");
        List<AdminUserRoleModel> adminUserRoleModelList = this.adminUserRoleService.getListByAdminId(adminModel.getId(), DsType.SLAVE);
        List<Long> roleIdList = adminUserRoleModelList.stream().map(AdminUserRoleModel::getRole_id).collect(Collectors.toList());
        List<AdminRoleMenuModel> adminRoleMenuModelList = this.adminRoleMenuService.getListByRoleIdList(roleIdList, DsType.SLAVE);
        List<AdminMenuModel> adminMenuModelList = this.adminMenuService.getModelList(Collections.emptyMap(), DsType.SLAVE);

        //更新登录信息
        adminModel.setLast_login_ip(request.getIp());
        adminModel.setLast_login_time(FrameUtil.getTime());
        this.adminService.update(adminModel);

        return AdminLoginResponse.builder().
                adminModel(adminModel).
                adminUserRoleModelList(adminUserRoleModelList).
                adminRoleMenuModelList(adminRoleMenuModelList).
                adminMenuModelList(adminMenuModelList).
                build();

    }
}
