package com.ms.core.service.admin.dao.impl;

import com.ms.base.jdbc.dao.BaseDaoImpl;
import com.ms.base.jdbc.dao.IBaseMapper;
import com.ms.core.comm.admin.model.AdminModel;
import com.ms.core.service.admin.dao.IAdminDao;
import com.ms.core.service.admin.mapper.AdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <b>description</b>：后台管理员数据访问实现类 <br>
 * <b>time</b>：2018-08-15 18:21:11 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Component
public class AdminDaoImpl extends BaseDaoImpl<Long, AdminModel> implements IAdminDao {
    @Autowired
    private AdminMapper adminMapper;

    @Override
    protected String getPrimaryKeyName() {
        return "id";
    }

    @Override
    public IBaseMapper<AdminModel> getBaseMapper() {
        return this.adminMapper;
    }
}