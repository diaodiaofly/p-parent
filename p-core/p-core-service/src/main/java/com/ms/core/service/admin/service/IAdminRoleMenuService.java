package com.ms.core.service.admin.service;

import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.jdbc.service.IBaseService;
import com.ms.core.comm.admin.model.AdminRoleMenuModel;

import java.util.List;

/**
 * <b>description</b>：角色菜单关联表业务接口 <br>
 * <b>time</b>：2018-08-21 09:15:55 <br>
 * <b>author</b>：ready likun_557@163.com
 */
public interface IAdminRoleMenuService extends IBaseService<java.lang.Long, AdminRoleMenuModel> {
    /**
     * 删除role_id关联的数据
     *
     * @param role_id
     * @return 删除的行数
     * @throws Exception
     */
    int deleteByRoleId(long role_id) throws Exception;

    /**
     * 删除menu_id关联的数据
     *
     * @param menu_id
     * @return 删除的行数
     * @throws Exception
     */
    int deleteByMenuId(long menu_id) throws Exception;

    /**
     * 查询roleIdList关联的AdminRoleMenuModel集合信息
     *
     * @param roleIdList
     * @param dsType     主从查询标识
     * @return
     * @throws Exception
     */
    List<AdminRoleMenuModel> getListByRoleIdList(List<Long> roleIdList, DsType dsType) throws Exception;
}