package com.ms.core.service.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.cloud.ResultUtil;
import com.ms.core.comm.admin.controller.IAdminLoginController;
import com.ms.core.comm.admin.dto.AdminLoginRequest;
import com.ms.core.comm.admin.dto.AdminLoginResponse;
import com.ms.core.service.admin.bus.IAdminLoginBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <b>description</b>：系统用户登录 <br>
 * <b>time</b>：2018-08-16 11:39 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@RestController
public class AdminLoginController implements IAdminLoginController {

    @Autowired
    private IAdminLoginBus adminLoginBus;

    @Override
    public ResultDto<AdminLoginResponse> login(@RequestBody AdminLoginRequest request) throws Exception {
        return ResultUtil.successData(this.adminLoginBus.login(request));
    }

}
