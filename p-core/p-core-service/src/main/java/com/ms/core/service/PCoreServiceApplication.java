package com.ms.core.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@Slf4j
public class PCoreServiceApplication {

    public static void main(String[] args) throws InterruptedException {
         SpringApplication.run(PCoreServiceApplication.class, args);
    }
}
