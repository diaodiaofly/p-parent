package com.ms.core.service.admin.mapper;

import com.ms.base.jdbc.dao.IBaseMapper;
import com.ms.core.comm.admin.model.AdminModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * <b>description</b>：后台管理员mapper <br>
 * <b>time</b>：2018-08-15 18:21:11 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Mapper
public interface AdminMapper extends IBaseMapper<AdminModel> {
}