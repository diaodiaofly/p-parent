package com.ms.core.service.admin.mapper;

import com.ms.base.jdbc.dao.IBaseMapper;
import com.ms.core.comm.admin.model.AdminUserRoleModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * <b>description</b>：用户角色关联表mapper <br>
 * <b>time</b>：2018-08-21 09:16:12 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Mapper
public interface AdminUserRoleMapper extends IBaseMapper<AdminUserRoleModel> {
}