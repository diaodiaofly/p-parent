package com.ms.core.service.admin.controller;

import com.ms.base.comm.cloud.RequestDto;
import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.cloud.ResultUtil;
import com.ms.base.comm.util.Constant;
import com.ms.base.comm.util.FrameUtil;
import com.ms.base.page.core.PageUtil;
import com.ms.core.comm.admin.controller.IIndexController;
import com.ms.core.comm.admin.model.DemoModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-26 12:34 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@RestController
@Slf4j
public class IndexController implements IIndexController {

    public ResultDto<String> index() {
        log.info("哈哈");
        return ResultUtil.successData("hello");
    }

    public ResultDto<String> sleep(@PathVariable("timeout") long timeout, @RequestParam(value = "error", required = false) Integer i) throws Exception {
        TimeUnit.SECONDS.sleep(timeout);
        if (i != null && i == 1) {
            throw new Exception("error 了!");
        }
        return ResultUtil.successData("success");
    }

    public ResultDto<Integer> test1(@PathVariable("i") int i) throws Exception {
        int result = 10 / i;
        return ResultUtil.successData(result);
    }

    @Override
    public ResultDto<String> request1(@RequestBody RequestDto<Map<String, Object>> requestDto) {
        log.info(FrameUtil.json(requestDto, true));
        return ResultUtil.successData("ok");
    }

    @Override
    public ResultDto<String> request2(@RequestBody RequestDto<Object> requestDto) {
        log.info(FrameUtil.json(requestDto, true));
        return ResultUtil.successData("ok");
    }

    @Override
    public ResultDto<String> request3(@RequestBody RequestDto<List<DemoModel>> requestDto) {
        log.info(FrameUtil.json(requestDto, true));
        return ResultUtil.successData("ok");
    }

    @Override
    public ResultDto<String> request4(@RequestBody RequestDto<List<DemoModel>> requestDto, @RequestParam(PageUtil.PAGE_KEY) int page, @RequestParam(Constant.MSTER_KEY) boolean master) {
        log.info(FrameUtil.json(requestDto, true));
        return ResultUtil.successData("ok");
    }

    @Override
    public ResultDto<String> request5(@RequestBody RequestDto<List<DemoModel>> requestDto, @PathVariable(PageUtil.PAGE_KEY) int page, @PathVariable(Constant.MSTER_KEY) boolean master) {
        log.info(FrameUtil.json(requestDto, true));
        return ResultUtil.successData("ok");
    }
}
