package com.ms.core.service.admin.dao.impl;

import com.ms.base.jdbc.dao.BaseDaoImpl;
import com.ms.base.jdbc.dao.IBaseMapper;
import com.ms.core.comm.admin.model.AdminRoleModel;
import com.ms.core.service.admin.dao.IAdminRoleDao;
import com.ms.core.service.admin.mapper.AdminRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <b>description</b>：后台角色表数据访问实现类 <br>
 * <b>time</b>：2018-08-21 09:15:41 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Component
public class AdminRoleDaoImpl extends BaseDaoImpl<java.lang.Long, AdminRoleModel> implements IAdminRoleDao {
    @Autowired
    private AdminRoleMapper adminRoleMapper;

    @Override
    protected String getPrimaryKeyName() {
        return "id";
    }

    @Override
    public IBaseMapper<AdminRoleModel> getBaseMapper() {
        return this.adminRoleMapper;
    }
}