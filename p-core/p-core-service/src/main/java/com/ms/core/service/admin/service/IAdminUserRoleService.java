package com.ms.core.service.admin.service;

import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.jdbc.service.IBaseService;
import com.ms.core.comm.admin.model.AdminUserRoleModel;

import java.util.List;

/**
 * <b>description</b>：用户角色关联表业务接口 <br>
 * <b>time</b>：2018-08-21 09:16:12 <br>
 * <b>author</b>：ready likun_557@163.com
 */
public interface IAdminUserRoleService extends IBaseService<java.lang.Long, AdminUserRoleModel> {

    /**
     * 删除admin_id关联的数据
     *
     * @param admin_id
     * @return 删除的行数
     * @throws Exception
     */
    int deleteByAdminId(long admin_id) throws Exception;

    /**
     * 删除role_id关联的数据
     *
     * @param role_id
     * @return 删除的行数
     * @throws Exception
     */
    int deleteByRoleId(long role_id) throws Exception;

    /**
     * 根据admin_id获取对应的数据
     *
     * @param admin_id
     * @param dsType 主从标志
     * @return
     * @throws Exception
     */
    List<AdminUserRoleModel> getListByAdminId(long admin_id, DsType dsType) throws Exception;
}