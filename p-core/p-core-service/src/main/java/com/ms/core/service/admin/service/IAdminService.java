package com.ms.core.service.admin.service;

import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.jdbc.service.IBaseService;
import com.ms.core.comm.admin.dto.SaveAdminRequest;
import com.ms.core.comm.admin.model.AdminModel;

/**
 * <b>description</b>：后台管理员业务接口 <br>
 * <b>time</b>：2018-08-15 18:21:11 <br>
 * <b>author</b>：ready likun_557@163.com
 */
public interface IAdminService extends IBaseService<Long, AdminModel> {
    /**
     * 保存(新增&修改)，返回保存之后的数据
     *
     * @param dto
     * @return
     * @throws Exception
     */
    AdminModel save(SaveAdminRequest dto) throws Exception;

    /**
     * 根据用户名获取用户信息
     *
     * @param name   用户名
     * @param dsType
     * @return
     * @throws Exception
     */
    AdminModel getModelByName(String name, DsType dsType) throws Exception;
}