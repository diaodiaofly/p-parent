package com.ms.core.service.admin.service;

import com.ms.base.jdbc.service.IBaseService;
import com.ms.core.comm.admin.dto.SaveBatchAdminMenuRequest;
import com.ms.core.comm.admin.model.AdminMenuModel;

/**
 * <b>description</b>：后台菜单表业务接口 <br>
 * <b>time</b>：2018-08-21 09:15:21 <br>
 * <b>author</b>：ready likun_557@163.com
 */
public interface IAdminMenuService extends IBaseService<java.lang.Long, AdminMenuModel> {

    /**
     * 批量保存菜单信息
     *
     * @param dto
     * @throws Exception
     */
    void saveBatch(SaveBatchAdminMenuRequest dto) throws Exception;
}