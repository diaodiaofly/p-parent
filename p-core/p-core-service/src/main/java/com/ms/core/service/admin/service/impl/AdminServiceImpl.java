package com.ms.core.service.admin.service.impl;

import com.ms.base.comm.util.FrameUtil;
import com.ms.base.comm.util.SecurityUtil;
import com.ms.base.jdbc.dao.IBaseDao;
import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.jdbc.service.BaseServiceImpl;
import com.ms.core.comm.admin.dto.SaveAdminRequest;
import com.ms.core.comm.admin.model.AdminModel;
import com.ms.core.comm.admin.model.AdminUserRoleModel;
import com.ms.core.service.admin.dao.IAdminDao;
import com.ms.core.service.admin.service.IAdminService;
import com.ms.core.service.admin.service.IAdminUserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * <b>description</b>：后台管理员业务实现类 <br>
 * <b>time</b>：2018-08-15 18:21:11 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Service
@Slf4j
public class AdminServiceImpl extends BaseServiceImpl<Long, AdminModel> implements IAdminService {
    @Autowired
    private IAdminDao adminDao;
    @Autowired
    private IAdminUserRoleService adminUserRoleService;

    @Override
    public IBaseDao<Long, AdminModel> getBaseDao() {
        return this.adminDao;
    }

    @Transactional
    public AdminModel save(SaveAdminRequest dto) throws Exception {
        Assert.notNull(dto, "dto is not null!");
        Assert.notNull(dto.getModel(), "dto is not null!");
        Long id = null;
        if (dto.getModel().getId() == null) {
            //新增
            Assert.isTrue(StringUtils.isNotBlank(dto.getModel().getName()), "登录名不能为空!");
            Assert.isTrue(StringUtils.isNotBlank(dto.getModel().getPassword()), "密码不能为空!");
            Assert.isTrue(StringUtils.isNotBlank(dto.getModel().getMobile()), "手机号不能为空!");
            AdminModel model = this.getModelByName(dto.getModel().getName(), null);
            Assert.isTrue(model == null, "用户名已存在!");
            dto.getModel().setSecretkey(SecurityUtil.getRandomNumber(6));
            dto.getModel().setPassword(SecurityUtil.md5_2(dto.getModel().getPassword(), dto.getModel().getSecretkey()));
            id = this.insert(dto.getModel()).getId();
        } else {
            //修改
            Assert.isTrue(StringUtils.isNotBlank(dto.getModel().getName()), "登录名不能为空!");
            Assert.isTrue(StringUtils.isNotBlank(dto.getModel().getMobile()), "手机号不能为空!");
            AdminModel model = this.getModelByName(dto.getModel().getName(), null);
            Assert.isTrue(model == null || model.getId().equals(dto.getModel().getId()), "用户名已存在!");
            if (StringUtils.isNotBlank(dto.getModel().getPassword())) {
                dto.getModel().setSecretkey(SecurityUtil.getRandomNumber(6));
                dto.getModel().setPassword(SecurityUtil.md5_2(dto.getModel().getPassword(), dto.getModel().getSecretkey()));
            }
            this.update(dto.getModel());
            id = dto.getModel().getId();

            //删除关联角色信息
            this.adminUserRoleService.deleteByAdminId(id);
        }
        List<AdminUserRoleModel> adminUserRoleModelList = FrameUtil.newArrayList();
        if (dto.getRoleIdList() != null) {
            for (Long roleId : dto.getRoleIdList()) {
                if (roleId != null) {
                    adminUserRoleModelList.add(AdminUserRoleModel.builder().admin_id(id).role_id(roleId).build());
                }
            }
        }
        //插入用户关联的角色信息
        this.adminUserRoleService.insertBatch(adminUserRoleModelList);
        return this.getModelById(id, DsType.MASTER);
    }

    /**
     * 根据用户名获取用户信息
     *
     * @param name   用户名
     * @param dsType
     * @return
     * @throws Exception
     */
    public AdminModel getModelByName(String name, DsType dsType) throws Exception {
        return this.getModelOne(FrameUtil.newHashMap(AdminModel.NAME_COL, name), dsType);
    }

    @Override
    public int deleteById(Long id) throws Exception {
        this.adminUserRoleService.deleteByAdminId(id);
        return super.deleteById(id);
    }
}