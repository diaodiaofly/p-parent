package com.ms.core.service.admin.dao.impl;

import com.ms.base.jdbc.dao.BaseDaoImpl;
import com.ms.base.jdbc.dao.IBaseMapper;
import com.ms.core.comm.admin.model.AdminMenuModel;
import com.ms.core.service.admin.dao.IAdminMenuDao;
import com.ms.core.service.admin.mapper.AdminMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <b>description</b>：后台菜单表数据访问实现类 <br>
 * <b>time</b>：2018-08-21 09:15:21 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Component
public class AdminMenuDaoImpl extends BaseDaoImpl<java.lang.Long, AdminMenuModel> implements IAdminMenuDao {
    @Autowired
    private AdminMenuMapper adminMenuMapper;

    @Override
    protected String getPrimaryKeyName() {
        return "id";
    }

    @Override
    public IBaseMapper<AdminMenuModel> getBaseMapper() {
        return this.adminMenuMapper;
    }
}