package com.ms.core.service.admin.mapper;

import com.ms.base.jdbc.dao.IBaseMapper;
import com.ms.core.comm.admin.model.AdminRoleMenuModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * <b>description</b>：角色菜单关联表mapper <br>
 * <b>time</b>：2018-08-21 09:15:55 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Mapper
public interface AdminRoleMenuMapper extends IBaseMapper<AdminRoleMenuModel> {
}