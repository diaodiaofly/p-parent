package com.ms.core.service.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.cloud.ResultUtil;
import com.ms.base.comm.util.Constant;
import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import com.ms.base.page.core.PageUtil;
import com.ms.core.comm.admin.controller.IAdminController;
import com.ms.core.comm.admin.dto.SaveAdminRequest;
import com.ms.core.comm.admin.model.AdminModel;
import com.ms.core.service.admin.service.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <b>description</b>：系统用户管理 <br>
 * <b>time</b>：2018-08-16 11:39 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@RestController
public class AdminController implements IAdminController {

    @Autowired
    private IAdminService adminService;

    @Override
    public ResultDto<AdminModel> save(@RequestBody SaveAdminRequest dto) throws Exception {
        return ResultUtil.successData(this.adminService.save(dto));
    }

    @Override
    public ResultDto<Integer> deleteById(@RequestParam("id") long id) throws Exception {
        return ResultUtil.successData(this.adminService.deleteById(id));
    }

    @Override
    public ResultDto<Long> getModelListCount(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminService.getModelListCount(map, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminModel>> getModelList(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminService.getModelList(map, DsType.dsType(master)));
    }

    @Override
    public ResultDto<AdminModel> getModelById(@RequestParam("id") long id, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminService.getModelById(id, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminModel>> getModelsByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminService.getModelsByIds(idList, DsType.dsType(master)));
    }

    @Override
    public ResultDto<Map<Long, AdminModel>> getModelMapByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminService.getModelMapByIds(idList, DsType.dsType(master)));
    }

    @Override
    public ResultDto<AdminModel> getModelOne(@RequestBody Map<String, Object> paramMap, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminService.getModelOne(paramMap, DsType.dsType(master)));
    }

    @Override
    public ResultDto<PageModel<AdminModel>> getPageModel(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.PAGE_KEY) int page, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminService.getPageModel(map, page, rows, DsType.dsType(master)));
    }

    @Override
    public ResultDto<PageModel<AdminModel>> getPageModel1(@RequestBody PageParamModel pageParamModel, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminService.getPageModel1(pageParamModel, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminModel>> getModelList1(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.SKIP_KEY) int skip, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminService.getModelList1(map, skip, rows, DsType.dsType(master)));
    }

    @Override
    public ResultDto<AdminModel> getModelByName(@RequestParam(AdminModel.NAME_COL) String name, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminService.getModelByName(name, DsType.dsType(master)));
    }
}
