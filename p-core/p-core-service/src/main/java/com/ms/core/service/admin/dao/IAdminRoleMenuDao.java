package com.ms.core.service.admin.dao;

import com.ms.base.jdbc.dao.IBaseDao;
import com.ms.core.comm.admin.model.AdminRoleMenuModel;

/**
 * <b>description</b>：角色菜单关联表数据访问接口 <br>
 * <b>time</b>：2018-08-21 09:15:55 <br>
 * <b>author</b>：ready likun_557@163.com
 */
public interface IAdminRoleMenuDao extends IBaseDao<java.lang.Long, AdminRoleMenuModel> {
}