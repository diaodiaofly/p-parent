package com.ms.core.service.admin.service.impl;

import com.ms.base.comm.util.FrameUtil;
import com.ms.base.jdbc.dao.IBaseDao;
import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.jdbc.service.BaseServiceImpl;
import com.ms.core.comm.admin.model.AdminUserRoleModel;
import com.ms.core.service.admin.dao.IAdminUserRoleDao;
import com.ms.core.service.admin.service.IAdminUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <b>description</b>：用户角色关联表业务实现类 <br>
 * <b>time</b>：2018-08-21 09:16:12 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Service
public class AdminUserRoleServiceImpl extends BaseServiceImpl<java.lang.Long, AdminUserRoleModel> implements IAdminUserRoleService {
    @Autowired
    private IAdminUserRoleDao adminUserRoleDao;

    @Override
    public IBaseDao<java.lang.Long, AdminUserRoleModel> getBaseDao() {
        return this.adminUserRoleDao;
    }

    @Override
    public int deleteByAdminId(long admin_id) throws Exception {
        return this.adminUserRoleDao.delete(FrameUtil.newHashMap(AdminUserRoleModel.ADMIN_ID_COL, admin_id));
    }

    @Override
    public int deleteByRoleId(long role_id) throws Exception {
        return this.adminUserRoleDao.delete(FrameUtil.newHashMap(AdminUserRoleModel.ROLE_ID_COL, role_id));
    }

    @Override
    public List<AdminUserRoleModel> getListByAdminId(long admin_id, DsType dsType) throws Exception {
        return this.getModelList(FrameUtil.newHashMap(AdminUserRoleModel.ADMIN_ID_COL, admin_id), dsType);
    }
}