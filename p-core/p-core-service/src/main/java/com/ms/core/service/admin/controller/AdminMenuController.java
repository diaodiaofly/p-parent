package com.ms.core.service.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.cloud.ResultUtil;
import com.ms.base.comm.util.Constant;
import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import com.ms.base.page.core.PageUtil;
import com.ms.core.comm.admin.controller.IAdminMenuController;
import com.ms.core.comm.admin.dto.SaveBatchAdminMenuRequest;
import com.ms.core.comm.admin.model.AdminMenuModel;
import com.ms.core.service.admin.service.IAdminMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <b>description</b>：后台菜单表 http接口 <br>
 * <b>time</b>：2018-08-21 09:15:21 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@RestController
public class AdminMenuController implements IAdminMenuController {

    @Autowired
    private IAdminMenuService adminMenuService;

    @Override
    public ResultDto<AdminMenuModel> insert(@RequestBody AdminMenuModel model) throws Exception {
        return ResultUtil.successData(this.adminMenuService.insert(model));
    }

    @Override
    public ResultDto<Integer> update(@RequestBody AdminMenuModel model) throws Exception {
        return ResultUtil.successData(this.adminMenuService.update(model));
    }

    @Override
    public ResultDto<Integer> deleteById(@RequestParam("id") long id) throws Exception {
        return ResultUtil.successData(this.adminMenuService.deleteById(id));
    }

    @Override
    public ResultDto<Long> getModelListCount(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminMenuService.getModelListCount(map, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminMenuModel>> getModelList(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminMenuService.getModelList(map, DsType.dsType(master)));
    }

    @Override
    public ResultDto<AdminMenuModel> getModelById(@RequestParam("id") long id, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminMenuService.getModelById(id, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminMenuModel>> getModelsByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminMenuService.getModelsByIds(idList, DsType.dsType(master)));
    }

    @Override
    public ResultDto<Map<Long, AdminMenuModel>> getModelMapByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminMenuService.getModelMapByIds(idList, DsType.dsType(master)));
    }

    @Override
    public ResultDto<AdminMenuModel> getModelOne(@RequestBody Map<String, Object> paramMap, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminMenuService.getModelOne(paramMap, DsType.dsType(master)));
    }

    @Override
    public ResultDto<PageModel<AdminMenuModel>> getPageModel(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.PAGE_KEY) int page, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminMenuService.getPageModel(map, page, rows, DsType.dsType(master)));
    }

    @Override
    public ResultDto<PageModel<AdminMenuModel>> getPageModel1(@RequestBody PageParamModel pageParamModel, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminMenuService.getPageModel1(pageParamModel, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminMenuModel>> getModelList1(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.SKIP_KEY) int skip, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminMenuService.getModelList1(map, skip, rows, DsType.dsType(master)));
    }

    @Override
    public ResultDto<Void> saveBatch(@RequestBody SaveBatchAdminMenuRequest dto) throws Exception {
        this.adminMenuService.saveBatch(dto);
        return ResultUtil.success();
    }
}