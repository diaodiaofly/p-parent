package com.ms.core.service.admin.service.impl;

import com.ms.base.comm.util.FrameUtil;
import com.ms.base.jdbc.dao.IBaseDao;
import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.jdbc.service.BaseServiceImpl;
import com.ms.core.comm.admin.model.AdminRoleMenuModel;
import com.ms.core.service.admin.dao.IAdminRoleMenuDao;
import com.ms.core.service.admin.service.IAdminRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <b>description</b>：角色菜单关联表业务实现类 <br>
 * <b>time</b>：2018-08-21 09:15:55 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Service
public class AdminRoleMenuServiceImpl extends BaseServiceImpl<java.lang.Long, AdminRoleMenuModel> implements IAdminRoleMenuService {

    public static final String ROLEIDLIST = "roleIdList";

    @Autowired
    private IAdminRoleMenuDao adminRoleMenuDao;

    @Override
    public IBaseDao<java.lang.Long, AdminRoleMenuModel> getBaseDao() {
        return this.adminRoleMenuDao;
    }

    @Override
    public int deleteByRoleId(long role_id) throws Exception {
        return this.adminRoleMenuDao.delete(FrameUtil.newHashMap(AdminRoleMenuModel.ROLE_ID_COL, role_id));
    }

    @Override
    public int deleteByMenuId(long menu_id) throws Exception {
        return this.adminRoleMenuDao.delete(FrameUtil.newHashMap(AdminRoleMenuModel.MENU_ID_COL, menu_id));
    }

    @Override
    public List<AdminRoleMenuModel> getListByRoleIdList(List<Long> roleIdList, DsType dsType) throws Exception {
        return this.getModelList(FrameUtil.newHashMap(ROLEIDLIST, roleIdList), dsType);
    }
}