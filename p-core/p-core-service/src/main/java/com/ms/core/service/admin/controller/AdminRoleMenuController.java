package com.ms.core.service.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.cloud.ResultUtil;
import com.ms.base.comm.util.Constant;
import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import com.ms.base.page.core.PageUtil;
import com.ms.core.comm.admin.controller.IAdminRoleMenuController;
import com.ms.core.comm.admin.model.AdminRoleMenuModel;
import com.ms.core.service.admin.service.IAdminRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * <b>description</b>：角色菜单关联表 http接口 <br>
 * <b>time</b>：2018-08-21 09:15:55 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@RestController
public class AdminRoleMenuController implements IAdminRoleMenuController {

    @Autowired
    private IAdminRoleMenuService adminRoleMenuService;

    @Override
    public ResultDto<AdminRoleMenuModel> insert(@RequestBody AdminRoleMenuModel model) throws Exception {
        return ResultUtil.successData(this.adminRoleMenuService.insert(model));
    }

    @Override
    public ResultDto<Integer> update(@RequestBody AdminRoleMenuModel model) throws Exception {
        return ResultUtil.successData(this.adminRoleMenuService.update(model));
    }

    @Override
    public ResultDto<Integer> deleteById(@RequestParam("id") long id) throws Exception {
        return ResultUtil.successData(this.adminRoleMenuService.deleteById(id));
    }

    @Override
    public ResultDto<Long> getModelListCount(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleMenuService.getModelListCount(map, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminRoleMenuModel>> getModelList(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleMenuService.getModelList(map, DsType.dsType(master)));
    }

    @Override
    public ResultDto<AdminRoleMenuModel> getModelById(@RequestParam("id") long id, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleMenuService.getModelById(id, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminRoleMenuModel>> getModelsByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleMenuService.getModelsByIds(idList, DsType.dsType(master)));
    }

    @Override
    public ResultDto<Map<Long, AdminRoleMenuModel>> getModelMapByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleMenuService.getModelMapByIds(idList, DsType.dsType(master)));
    }

    @Override
    public ResultDto<AdminRoleMenuModel> getModelOne(@RequestBody Map<String, Object> paramMap, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleMenuService.getModelOne(paramMap, DsType.dsType(master)));
    }

    @Override
    public ResultDto<PageModel<AdminRoleMenuModel>> getPageModel(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.PAGE_KEY) int page, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleMenuService.getPageModel(map, page, rows, DsType.dsType(master)));
    }

    @Override
    public ResultDto<PageModel<AdminRoleMenuModel>> getPageModel1(@RequestBody PageParamModel pageParamModel, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleMenuService.getPageModel1(pageParamModel, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminRoleMenuModel>> getModelList1(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.SKIP_KEY) int skip, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.adminRoleMenuService.getModelList1(map, skip, rows, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<AdminRoleMenuModel>> getListByRoleIdList(@RequestBody List<Long> roleIdList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        if (roleIdList == null || roleIdList.isEmpty()) {
            return ResultUtil.successData(Collections.emptyList());
        }
        return ResultUtil.successData(this.adminRoleMenuService.getListByRoleIdList(roleIdList, DsType.dsType(master)));
    }
}