package com.ms.core.service.admin.dao.impl;

import com.ms.base.jdbc.dao.BaseDaoImpl;
import com.ms.base.jdbc.dao.IBaseMapper;
import com.ms.core.comm.admin.model.AdminRoleMenuModel;
import com.ms.core.service.admin.dao.IAdminRoleMenuDao;
import com.ms.core.service.admin.mapper.AdminRoleMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <b>description</b>：角色菜单关联表数据访问实现类 <br>
 * <b>time</b>：2018-08-21 09:15:55 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Component
public class AdminRoleMenuDaoImpl extends BaseDaoImpl<java.lang.Long, AdminRoleMenuModel> implements IAdminRoleMenuDao {
    @Autowired
    private AdminRoleMenuMapper adminRoleMenuMapper;

    @Override
    protected String getPrimaryKeyName() {
        return "id";
    }

    @Override
    public IBaseMapper<AdminRoleMenuModel> getBaseMapper() {
        return this.adminRoleMenuMapper;
    }
}