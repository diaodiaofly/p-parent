package com.ms.core.test;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageUtil;
import com.ms.core.api.admin.DemoClient;
import com.ms.core.comm.admin.model.DemoModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-30 13:50 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@RestController
public class DemoControllerTest {

    @Autowired
    private DemoClient demoClient;

    /**
     * 插入
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/demo/insert")
    public ResultDto<DemoModel> insert(@RequestBody DemoModel model) throws Exception {
        ResultDto<DemoModel> resultDto = this.demoClient.insert(model);
        return resultDto;
    }

    /**
     * 详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/demo/detail/{id}")
    public ResultDto<DemoModel> detail(@PathVariable("id") long id) throws Exception {
        Long parm = id;
        return this.demoClient.detail(parm);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("/demo/delete/{id}")
    public ResultDto<Integer> delete(@PathVariable("id") long id) throws Exception {
        return this.demoClient.delete(id);
    }

    /**
     * 获取所有数据
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("/demo/list/{page}/{rows}")
    public ResultDto<PageModel<DemoModel>> list(@PathVariable(PageUtil.PAGE_KEY) int page, @PathVariable(PageUtil.ROWS_KEY) int rows) throws Exception {
        ResultDto<PageModel<DemoModel>> result = this.demoClient.list(page, rows);
        return result;
    }

}
