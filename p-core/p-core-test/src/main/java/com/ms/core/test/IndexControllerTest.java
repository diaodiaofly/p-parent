package com.ms.core.test;

import com.ms.base.comm.cloud.RequestDto;
import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.cloud.ResultUtil;
import com.ms.base.comm.util.FrameUtil;
import com.ms.core.api.admin.IndexClient;
import com.ms.core.comm.admin.model.DemoModel;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-26 12:34 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@RestController
public class IndexControllerTest implements ApplicationContextAware {
    @Autowired
    private IndexClient indexClient;

    private ApplicationContext applicationContext;

    @RequestMapping("/")
    public ResultDto<String> index() {
        return this.indexClient.index();
    }

    @RequestMapping("/sleep/{timeout}")
    public ResultDto<String> sleep(@PathVariable("timeout") long timeout, @RequestParam(value = "error", required = false) Integer i) throws Exception {
        return ResultUtil.ok(this.indexClient.sleep(timeout, i));
    }

    @RequestMapping("/test1/{i}")
    public ResultDto<Integer> test1(@PathVariable("i") int i) throws Exception {
        return ResultUtil.ok(this.indexClient.test1(i));
    }

    @RequestMapping("/bean/{name}")
    public ResultDto<Object> bean(@PathVariable("name") String name) {
        Object bean = this.applicationContext.getBean(name);
        return ResultUtil.ok(ResultUtil.successData(Objects.nonNull(bean) ? bean.toString() : null));
    }

    @RequestMapping("/test2/{msg}")
    public ResultDto<String> test2(@PathVariable("msg") String msg) {
        FrameUtil.throwBaseException(msg);
        return ResultUtil.success();
    }

    @RequestMapping("/request1")
    public ResultDto<String> request1() {
        RequestDto requestDto = RequestDto.<Map<String, Object>>builder().data(FrameUtil.newHashMap("key1", "value1", "key2", "value2")).build();
        return this.indexClient.request1(requestDto);
    }

    @RequestMapping("/request2")
    public ResultDto<String> request2() {
        Object object = FrameUtil.newHashMap("key1", "value1", "key2", "value2");
        RequestDto<Object> requestDto = RequestDto.builder().data(object).build();
        return this.indexClient.request2(requestDto);
    }

    @RequestMapping("/request2_1")
    public ResultDto<String> request4() {
        List<DemoModel> list = FrameUtil.newArrayList();
        for (long i = 0; i < 5; i++) {
            list.add(new DemoModel(i, "name-" + i));
        }
        RequestDto<Object> requestDto = RequestDto.<Object>builder().data(list).build();
        return this.indexClient.request2(requestDto);
    }

    @RequestMapping("/request3_1")
    public ResultDto<String> request5() {
        Object object = FrameUtil.newHashMap("key1", "value1", "key2", "value2");
        RequestDto requestDto = RequestDto.builder().data(object).build();
        return this.indexClient.request3(requestDto);
    }

    @RequestMapping("/request3")
    public ResultDto<String> request3() {
        List<DemoModel> list = FrameUtil.newArrayList();
        for (long i = 0; i < 5; i++) {
            list.add(new DemoModel(i, "name-" + i));
        }
        RequestDto<List<DemoModel>> requestDto = RequestDto.<List<DemoModel>>builder().data(list).build();
        return this.indexClient.request3(requestDto);
    }

    @RequestMapping("/request4")
    public ResultDto<String> request4(@RequestParam("page") int page, @RequestParam("master") boolean master) {
        List<DemoModel> list = FrameUtil.newArrayList();
        for (long i = 0; i < 5; i++) {
            list.add(new DemoModel(i, "name-" + i));
        }
        RequestDto<List<DemoModel>> requestDto = RequestDto.<List<DemoModel>>builder().data(list).build();
        return this.indexClient.request4(requestDto, page, master);
    }

    @RequestMapping("/request5/{page}/{master}")
    public ResultDto<String> request5(@PathVariable("page") int page, @PathVariable("master") boolean master) {
        List<DemoModel> list = FrameUtil.newArrayList();
        for (long i = 0; i < 5; i++) {
            list.add(new DemoModel(i, "name-" + i));
        }
        RequestDto<List<DemoModel>> requestDto = RequestDto.<List<DemoModel>>builder().data(list).build();
        return this.indexClient.request5(requestDto, page, master);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
