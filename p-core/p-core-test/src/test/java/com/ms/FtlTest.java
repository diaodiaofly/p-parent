package com.ms;

import com.ms.base.comm.util.FrameUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-08-14 18:27 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Slf4j
public class FtlTest {
    @Test
    public void test1() throws Exception {
        String s = FreemarkerUtil.getFtlToString("test1", FrameUtil.newHashMap("msg", FrameUtil.newHashMap("aa","")));
        log.info(s);
    }
}
