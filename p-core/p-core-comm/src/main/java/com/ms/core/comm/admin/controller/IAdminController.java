package com.ms.core.comm.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.util.Constant;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import com.ms.base.page.core.PageUtil;
import com.ms.base.web.mvc.WebUtil;
import com.ms.core.comm.admin.dto.SaveAdminRequest;
import com.ms.core.comm.admin.model.AdminModel;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * <b>description</b>：后台用户管理 <br>
 * <b>time</b>：2018-08-16 13:54 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public interface IAdminController {

    String MODULE_NAME = "admin";
    String CONTROLLER_NAME = "admin";
    String DELETEBYID_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "deleteById";
    String GETMODELLISTCOUNT_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelListCount";
    String GETMODELLIST_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelList";
    String GETMODELBYID_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelById";
    String GETMODELSBYIDS_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelsByIds";
    String GETMODELMAPBYIDS_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelMapByIds";
    String GETMODELONE_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelOne";
    String GETPAGEMODEL_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getPageModel";
    String GETPAGEMODEL1_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getPageModel1";
    String GETMODELLIST1_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelList1";
    String GETMODELBYNAME_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelByName";
    String SAVE_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "save";

    /**
     * 保存(新增&修改)，返回保存之后的数据
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @RequestMapping(SAVE_REQUEST_MAPPING)
    ResultDto<AdminModel> save(@RequestBody SaveAdminRequest dto) throws Exception;

    /**
     * 根据id删除数据
     *
     * @param id id
     * @return
     * @throws Exception
     */
    @RequestMapping(DELETEBYID_REQUEST_MAPPING)
    ResultDto<Integer> deleteById(@RequestParam("id") long id) throws Exception;

    /**
     * 获取记录行数
     *
     * @param map    查询条件
     * @param master 主从查询标志
     * @return
     */
    @RequestMapping(GETMODELLISTCOUNT_REQUEST_MAPPING)
    ResultDto<Long> getModelListCount(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 获取记录列表
     *
     * @param map    查询条件
     * @param master 主从查询标志
     * @return
     */
    @RequestMapping(GETMODELLIST_REQUEST_MAPPING)
    ResultDto<List<AdminModel>> getModelList(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 根据对象id查询数据
     *
     * @param id     对象id
     * @param master 主从查询标志
     * @return 返回id对应的对象
     */
    @RequestMapping(GETMODELBYID_REQUEST_MAPPING)
    ResultDto<AdminModel> getModelById(@RequestParam("id") long id, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 查询id列表对应的对象列表
     *
     * @param idList id列表
     * @param master 主从查询标志
     * @return
     */
    @RequestMapping(GETMODELSBYIDS_REQUEST_MAPPING)
    ResultDto<List<AdminModel>> getModelsByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 根据idList获取对象列表的map，key为对象的id，value为对象
     *
     * @param idList id列表
     * @param master 主从查询标志
     * @return id->item 列表
     */
    @RequestMapping(GETMODELMAPBYIDS_MAPPING)
    ResultDto<Map<Long, AdminModel>> getModelMapByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 获取一个对象
     *
     * @param paramMap 参数
     * @param master   主从查询标志
     * @return
     */
    @RequestMapping(GETMODELONE_REQUEST_MAPPING)
    ResultDto<AdminModel> getModelOne(@RequestBody Map<String, Object> paramMap, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 分页查询
     *
     * @param map    查询条件
     * @param page   当前页数
     * @param rows   每页行数
     * @param master 主从查询标志
     * @return
     * @throws Exception
     */
    @RequestMapping(GETPAGEMODEL_REQUEST_MAPPING)
    ResultDto<PageModel<AdminModel>> getPageModel(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.PAGE_KEY) int page, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 分页查询
     *
     * @param pageParamModel 分页参数
     * @param master         主从查询标志
     * @return
     * @throws Exception
     */
    @RequestMapping(GETPAGEMODEL1_REQUEST_MAPPING)
    ResultDto<PageModel<AdminModel>> getPageModel1(@RequestBody PageParamModel pageParamModel, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;


    /**
     * 查询列表
     *
     * @param map    查询条件
     * @param skip   跳过的行数
     * @param rows   行数
     * @param master 主从查询标志
     * @return
     * @throws Exception
     */
    @RequestMapping(GETMODELLIST1_REQUEST_MAPPING)
    ResultDto<List<AdminModel>> getModelList1(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.SKIP_KEY) int skip, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;


    /**
     * 根据用户名查找对象
     *
     * @param name   用户名
     * @param master 主从查询标志
     * @return
     */
    @RequestMapping(GETMODELBYNAME_REQUEST_MAPPING)
    ResultDto<AdminModel> getModelByName(@RequestParam(AdminModel.NAME_COL) String name, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

}
