package com.ms.core.comm.admin.util;

import com.ms.core.comm.admin.model.AdminMenuModel;

/**
 * <b>description</b>：菜单工具类 <br>
 * <b>time</b>：2018-08-22 15:54 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public class AdminMenuUtil {
    /**
     * 跟菜单pid
     */
    public static Long ROOT_MENU_PID = 0L;

    /**
     * 是否是根菜单
     * @param adminMenuModel
     * @return
     */
    public static boolean isRootMenu(AdminMenuModel adminMenuModel) {
        return adminMenuModel != null && ROOT_MENU_PID.equals(adminMenuModel.getPid());
    }
}
