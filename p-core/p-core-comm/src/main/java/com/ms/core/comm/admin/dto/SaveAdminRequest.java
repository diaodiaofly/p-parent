package com.ms.core.comm.admin.dto;

import com.ms.core.comm.admin.model.AdminModel;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * <b>description</b>：保存管理员信息 <br>
 * <b>time</b>：2018-08-21 09:51 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class SaveAdminRequest implements Serializable {
    private AdminModel model;
    private List<Long> roleIdList;
}
