package com.ms.core.comm.admin.model;

import com.ms.base.comm.model.BaseModel;
import lombok.*;

import java.io.Serializable;

/**
 * <b>description</b>：用户角色关联表 <br>
 * <b>time</b>：2018-08-21 09:16:12 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AdminUserRoleModel extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String ID_COL = "id";
    public static final String ROLE_ID_COL = "role_id";
    public static final String ADMIN_ID_COL = "admin_id";
    /**
     * 编号
     */
    private java.lang.Long id;
    /**
     * 角色id
     */
    private java.lang.Long role_id;
    /**
     * 后台用户id
     */
    private java.lang.Long admin_id;
}