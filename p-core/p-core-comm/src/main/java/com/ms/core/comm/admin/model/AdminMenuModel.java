package com.ms.core.comm.admin.model;

import com.ms.base.comm.model.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <b>description</b>：后台菜单表 <br>
 * <b>time</b>：2018-08-21 09:15:21 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminMenuModel extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 编号
     */
    private java.lang.Long id;
    /**
     * 菜单名称
     */
    private java.lang.String name;
    /**
     * 父菜单id
     */
    private java.lang.Long pid;
    /**
     * 同级排序
     */
    private java.lang.Integer the_sort;
    /**
     * url
     */
    private java.lang.String url;
}