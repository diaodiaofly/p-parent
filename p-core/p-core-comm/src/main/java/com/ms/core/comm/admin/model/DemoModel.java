package com.ms.core.comm.admin.model;

import com.ms.base.comm.model.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <b>description</b>：测试 <br>
 * <b>time</b>：2018-08-07 19:09:08 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DemoModel extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 编号
     */
    private java.lang.Long id;
    /**
     * 名称
     */
    private java.lang.String name;
}