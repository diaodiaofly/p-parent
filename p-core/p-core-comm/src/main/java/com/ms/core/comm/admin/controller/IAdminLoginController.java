package com.ms.core.comm.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.web.mvc.WebUtil;
import com.ms.core.comm.admin.dto.AdminLoginRequest;
import com.ms.core.comm.admin.dto.AdminLoginResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <b>description</b>：后台用户登录 <br>
 * <b>time</b>：2018-08-16 13:54 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public interface IAdminLoginController {

    String MODULE_NAME = "admin";
    String CONTROLLER_NAME = "adminLogin";
    String LOGIN_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "login";

    /**
     * 管理员登录
     *
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(LOGIN_REQUEST_MAPPING)
    ResultDto<AdminLoginResponse> login(@RequestBody AdminLoginRequest request) throws Exception;

}
