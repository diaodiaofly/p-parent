package com.ms.core.comm.admin.dto;

import com.ms.core.comm.admin.model.AdminRoleModel;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * <b>description</b>：保存后台角色信息 <br>
 * <b>time</b>：2018-08-21 09:51 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class SaveAdminRoleRequest implements Serializable {
    private AdminRoleModel model;
    private List<Long> menuIdList;
}
