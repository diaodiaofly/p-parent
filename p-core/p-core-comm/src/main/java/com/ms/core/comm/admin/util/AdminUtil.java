package com.ms.core.comm.admin.util;

import com.ms.base.comm.util.SecurityUtil;
import com.ms.core.comm.admin.model.AdminModel;
import org.springframework.util.Assert;

/**
 * <b>description</b>：后台管理工具类 <br>
 * <b>time</b>：2018-08-22 13:57 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public class AdminUtil {

    /**
     * 初始化密码
     *
     * @param adminModel
     * @return
     */
    public static void initPassword(AdminModel adminModel) {
        Assert.notNull(adminModel, "adminModel is not null!");
        if (adminModel.getSecretkey() == null) {
            adminModel.setSecretkey(SecurityUtil.getRandomNumber(6));
        }
        adminModel.setPassword(cipherPassword(adminModel.getPassword(), adminModel.getSecretkey()));
    }

    /**
     * 对明文密码进行加密
     *
     * @param password  明文密码
     * @param secretkey 秘钥
     * @return
     */
    public static String cipherPassword(String password, String secretkey) {
        return SecurityUtil.md5_2(password, secretkey);
    }

    /**
     * 验证密码是否正确
     *
     * @param adminModel 管理员信息
     * @param password   明文密码
     * @return
     */
    public static boolean validatePassword(AdminModel adminModel, String password) {
        return adminModel != null && cipherPassword(password, adminModel.getSecretkey()).equals(adminModel.getPassword());
    }
}
