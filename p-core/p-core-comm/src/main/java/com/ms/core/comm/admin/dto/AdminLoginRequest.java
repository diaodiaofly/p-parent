package com.ms.core.comm.admin.dto;

import lombok.*;

import java.io.Serializable;

/**
 * <b>description</b>：管理员登录信息 <br>
 * <b>time</b>：2018-08-23 13:43 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AdminLoginRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    //用户名
    private String name;
    //密码
    private String password;
    //登录ip
    private String ip;
}
