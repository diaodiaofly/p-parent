package com.ms.core.comm.admin.dto;

import com.ms.core.comm.admin.model.AdminMenuModel;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * <b>description</b>：批量保存后台菜单信息 <br>
 * <b>time</b>：2018-08-21 09:51 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class SaveBatchAdminMenuRequest implements Serializable {
    private List<AdminMenuModel> adminMenuModels;
}
