package com.ms.core.comm.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.util.Constant;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import com.ms.base.page.core.PageUtil;
import com.ms.base.web.mvc.WebUtil;
import com.ms.core.comm.admin.model.AdminRoleMenuModel;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * <b>description</b>：角色菜单关联表 http接口 <br>
 * <b>time</b>：2018-08-21 09:15:55 <br>
 * <b>author</b>：ready likun_557@163.com
 */
public interface IAdminRoleMenuController {

    String MODULE_NAME = "admin";
    String CONTROLLER_NAME = "adminRoleMenu";
    String INSERT_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "insert";
    String UPDATE_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "update";
    String DELETEBYID_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "deleteById";
    String GETMODELLISTCOUNT_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelListCount";
    String GETMODELLIST_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelList";
    String GETMODELBYID_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelById";
    String GETMODELSBYIDS_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelsByIds";
    String GETMODELMAPBYIDS_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelMapByIds";
    String GETMODELONE_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelOne";
    String GETPAGEMODEL_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getPageModel";
    String GETPAGEMODEL1_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getPageModel1";
    String GETMODELLIST1_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelList1";
    String GETLISTBYROLEIDLIST_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getListByRoleIdList";

    /**
     * 插入
     *
     * @param model 数据
     * @return
     * @throws Exception
     */
    @RequestMapping(INSERT_REQUEST_MAPPING)
    ResultDto<AdminRoleMenuModel> insert(@RequestBody AdminRoleMenuModel model) throws Exception;

    /**
     * 更新
     *
     * @param model 数据
     * @return 影响行数
     * @throws Exception
     */
    @RequestMapping(UPDATE_REQUEST_MAPPING)
    ResultDto<Integer> update(@RequestBody AdminRoleMenuModel model) throws Exception;

    /**
     * 根据id删除数据
     *
     * @param id id
     * @return
     * @throws Exception
     */
    @RequestMapping(DELETEBYID_REQUEST_MAPPING)
    ResultDto<Integer> deleteById(@RequestParam("id") long id) throws Exception;

    /**
     * 获取记录行数
     *
     * @param map    查询条件
     * @param master 主从查询标志
     * @return
     */
    @RequestMapping(GETMODELLISTCOUNT_REQUEST_MAPPING)
    ResultDto<Long> getModelListCount(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 获取记录列表
     *
     * @param map    查询条件
     * @param master 主从查询标志
     * @return
     */
    @RequestMapping(GETMODELLIST_REQUEST_MAPPING)
    ResultDto<List<AdminRoleMenuModel>> getModelList(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 根据对象id查询数据
     *
     * @param id     对象id
     * @param master 主从查询标志
     * @return 返回id对应的对象
     */
    @RequestMapping(GETMODELBYID_REQUEST_MAPPING)
    ResultDto<AdminRoleMenuModel> getModelById(@RequestParam("id") long id, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 查询id列表对应的对象列表
     *
     * @param idList id列表
     * @param master 主从查询标志
     * @return
     */
    @RequestMapping(GETMODELSBYIDS_REQUEST_MAPPING)
    ResultDto<List<AdminRoleMenuModel>> getModelsByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 根据idList获取对象列表的map，key为对象的id，value为对象
     *
     * @param idList id列表
     * @param master 主从查询标志
     * @return id->item 列表
     */
    @RequestMapping(GETMODELMAPBYIDS_MAPPING)
    ResultDto<Map<Long, AdminRoleMenuModel>> getModelMapByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 获取一个对象
     *
     * @param paramMap 参数
     * @param master   主从查询标志
     * @return
     */
    @RequestMapping(GETMODELONE_REQUEST_MAPPING)
    ResultDto<AdminRoleMenuModel> getModelOne(@RequestBody Map<String, Object> paramMap, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 分页查询
     *
     * @param map    查询条件
     * @param page   当前页数
     * @param rows   每页行数
     * @param master 主从查询标志
     * @return
     * @throws Exception
     */
    @RequestMapping(GETPAGEMODEL_REQUEST_MAPPING)
    ResultDto<PageModel<AdminRoleMenuModel>> getPageModel(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.PAGE_KEY) int page, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 分页查询
     *
     * @param pageParamModel 分页参数
     * @param master         主从查询标志
     * @return
     * @throws Exception
     */
    @RequestMapping(GETPAGEMODEL1_REQUEST_MAPPING)
    ResultDto<PageModel<AdminRoleMenuModel>> getPageModel1(@RequestBody PageParamModel pageParamModel, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;


    /**
     * 查询列表
     *
     * @param map    查询条件
     * @param skip   跳过的行数
     * @param rows   行数
     * @param master 主从查询标志
     * @return
     * @throws Exception
     */
    @RequestMapping(GETMODELLIST1_REQUEST_MAPPING)
    ResultDto<List<AdminRoleMenuModel>> getModelList1(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.SKIP_KEY) int skip, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;


    /**
     * 查询roleIdList关联的AdminRoleMenuModel集合信息
     *
     * @param roleIdList
     * @param master     主从查询标识
     * @return
     * @throws Exception
     */
    @RequestMapping(GETLISTBYROLEIDLIST_REQUEST_MAPPING)
    ResultDto<List<AdminRoleMenuModel>> getListByRoleIdList(@RequestBody List<Long> roleIdList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;
}