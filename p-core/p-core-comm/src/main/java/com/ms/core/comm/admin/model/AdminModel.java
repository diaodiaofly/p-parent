package com.ms.core.comm.admin.model;

import com.ms.base.comm.model.BaseModel;
import lombok.*;

import java.io.Serializable;

/**
 * <b>description</b>：后台管理员 <br>
 * <b>time</b>：2018-08-15 18:21:11 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AdminModel extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String ID_COL = "id";
    public static final String NAME_COL = "name";
    public static final String MOBILE_COL = "mobile";
    public static final String PASSWORD_COL = "password";
    /**
     * 编号
     */
    private java.lang.Long id;
    /**
     * 用户名
     */
    private java.lang.String name;
    /**
     * 手机号
     */
    private java.lang.String mobile;
    /**
     * 密码
     */
    private java.lang.String password;
    /**
     * 密码加密秘钥
     */
    private java.lang.String secretkey;
    /**
     * 是否是管理员
     */
    private java.lang.Boolean is_admin;
    /**
     * 最后登录ip
     */
    private java.lang.String last_login_ip;
    /**
     * 最后登录时间
     */
    private java.lang.Long last_login_time;
}