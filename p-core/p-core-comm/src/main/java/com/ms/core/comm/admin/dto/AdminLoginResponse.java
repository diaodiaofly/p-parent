package com.ms.core.comm.admin.dto;

import com.ms.core.comm.admin.model.AdminMenuModel;
import com.ms.core.comm.admin.model.AdminModel;
import com.ms.core.comm.admin.model.AdminRoleMenuModel;
import com.ms.core.comm.admin.model.AdminUserRoleModel;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * <b>description</b>：管理员登录结果 <br>
 * <b>time</b>：2018-08-23 13:44 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AdminLoginResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    //用户信息
    private AdminModel adminModel;
    //用户角色列表
    private List<AdminUserRoleModel> adminUserRoleModelList;
    //用户角色关联的菜单信息
    private List<AdminRoleMenuModel> adminRoleMenuModelList;
    //系统所有菜单信息
    private List<AdminMenuModel> adminMenuModelList;
}
