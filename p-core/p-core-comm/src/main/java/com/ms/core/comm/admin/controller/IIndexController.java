package com.ms.core.comm.admin.controller;

import com.ms.base.comm.cloud.RequestDto;
import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.util.Constant;
import com.ms.base.page.core.PageUtil;
import com.ms.core.comm.admin.model.DemoModel;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-07-26 12:34 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public interface IIndexController {

    @RequestMapping("/")
    ResultDto<String> index();

    @RequestMapping("/sleep/{timeout}")
    ResultDto<String> sleep(@PathVariable("timeout") long timeout, @RequestParam(value = "error", required = false) Integer i) throws Exception;

    @RequestMapping("/test1/{i}")
    ResultDto<Integer> test1(@PathVariable("i") int i) throws Exception;

    @RequestMapping("/request1")
    ResultDto<String> request1(@RequestBody RequestDto<Map<String, Object>> requestDto);

    @RequestMapping("/request2")
    ResultDto<String> request2(@RequestBody RequestDto<Object> requestDto);

    @RequestMapping("/request3")
    ResultDto<String> request3(@RequestBody RequestDto<List<DemoModel>> requestDto);

    @RequestMapping("/request4")
    ResultDto<String> request4(@RequestBody RequestDto<List<DemoModel>> requestDto, @RequestParam(PageUtil.PAGE_KEY) int page, @RequestParam(Constant.MSTER_KEY) boolean master);

    @RequestMapping("/request5/{page}/{master}")
    ResultDto<String> request5(@RequestBody RequestDto<List<DemoModel>> requestDto, @PathVariable(PageUtil.PAGE_KEY) int page, @PathVariable(Constant.MSTER_KEY) boolean master);
}
