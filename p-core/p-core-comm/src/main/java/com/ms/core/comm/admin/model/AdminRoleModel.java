package com.ms.core.comm.admin.model;

import com.ms.base.comm.model.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <b>description</b>：后台角色表 <br>
 * <b>time</b>：2018-08-21 09:15:41 <br>
 * <b>author</b>：ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminRoleModel extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 编号
     */
    private java.lang.Long id;
    /**
     * 名称
     */
    private java.lang.String name;
    /**
     * 角色描述
     */
    private java.lang.String description;
}