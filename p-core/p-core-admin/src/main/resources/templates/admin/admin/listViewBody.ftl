<div class="wrap">
    <div class="mb10">
        <a href="<@ms_lib.action url='/admin/admin/addView' />" class="btn"><span class="add"></span>添加管理员</a>
    </div>
    <div class="table_list">
        <table width="100%">
            <thead>
            <tr>
                <td width="100">管理员ID</td>
                <td width="150">登录名</td>
                <td width="150">手机</td>
                <td width="100">是否是管理员</td>
                <td width="150">登录时间</td>
                <td width="200">登录ip</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody id="J_word_list">
            <#list pageHtmlModel.pageModel.dataList as item>
            <tr>
                <td>${item.id}</td>
                <td>${item.name}</td>
                <td>${item.mobile}</td>
                <td>${item.extData.is_admin}</td>
                <td>${(item.extData.last_login_time)!"-"}</td>
                <td>
                    <#if item.last_login_ip?? && item.last_login_ip!="">
                    ${item.last_login_ip}
                    <#else>
                        -
                    </#if>
                </td>
                <td>
                    <a href="<@ms_lib.action url='/admin/admin/editView/${item.id}' />" class="btn mr10 btn_green">修改</a>
                    <a href="<@ms_lib.action url='/admin/admin/delete/${item.id}' />" class="btn btn_orange J_ajaxRequest" data-msg="确定删除？" id="J_ajaxRequest_delete_${item.id}">删除</a>
                </td>
            </tr>
            </#list>
            </tbody>
        </table>
    </div>
    <#include "/lib/page.ftl" />
</div>