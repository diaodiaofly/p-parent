<#import "../../lib/ms_lib.ftl" as ms_lib />
<div class="wrap">
    <div class="nav">
        <a href="<@ms_lib.action url='/admin/admin/listView' />" class="btn">
            <b style="color:#ff9400;font-family:Arial;font-size:14px;">&lt;</b> 返回
        </a>
    </div>
    <form method="post" class="J_ajaxForm" autocomplete="off" action="<@ms_lib.action url='/admin/admin/save' />">
        <input type="hidden" name="model.id" value="${model.id}">
        <div class="h_a">${((model.id)??)?string("编辑管理员","添加管理员")}</div>
        <div class="table_full">
            <table width="100%">
                <tr>
                    <th width="100">登录名：</th>
                    <td>
                        <input name="model.name" type="text" class="input length_2" value="${(model.name)!'ready'}">
                    </td>
                </tr>
                <tr>
                    <th>密码：</th>
                    <td>
                        <#if (model.id)??>
                        <input name="model.password" type="text" class="input length_2">
                        <#else>
                        <input name="model.password" type="text" class="input length_2" value="${(model.password)!'123456'}">
                        </#if>
                    </td>
                </tr>
                <tr>
                    <th>手机号：</th>
                    <td>
                        <input name="model.mobile" type="text" class="input length_2" value="${model.mobile}">
                    </td>
                </tr>
                <tr>
                    <th>是否是管理员：</th>
                    <td>
                        <ul class="single_list cc">
                            <li><label><input ${((model.is_admin??) && model.is_admin)?string("checked","")} type="radio" name="is_admin"><span>是</span></label></li>
                            <li><label><input ${((model.is_admin??) && model.is_admin)?string("","checked")} type="radio" name="is_admin"><span>否</span></label></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>角色组：</th>
                    <td>
                        <ul class="double_list cc" style="width:500px;">
                            <#assign roleIdListIndex=0>
                            <#list adminRoleModelList as item>
                            <li>
                                <label>
                                <input ${((item.extData.checked)?? && item.extData.checked)?string("checked","")} type="checkbox" name="roleIdList[${roleIdListIndex}]" value="${item.id}" class="J_check">
                                    <span>${item.name}</span>
                                </label>
                            </li>
                                <#assign roleIdListIndex++>
                            </#list>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
        <div class="footer_wrap">
            <button class="btn btn_submit" type="submit">提交</button>
        </div>
</div>