<#import "../../lib/ms_lib.ftl" as ms_lib/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <meta charset="UTF-8"/>
    <title><@ms_lib.site key="title"/></title>
    <meta name="renderer" content="webkit"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <@ms_lib.link url="/p-core-admin/css/admin/main/index.css"/>
    <@ms_lib.script url="/p-core-admin/js/yjd.js"/>
    <@ms_lib.script url="/p-core-admin/js/config.js"/>
    <@ms_lib.script url="/p-core-admin/js/plugins/Jquery/1.8.0/Jquery.js"/>
    <script>
        /*var SUBMENU_CONFIG = {
            "nav_1": {
                "id": "nav_1",
                "name": "系统",
                "items": {
                    "work_219": {
                        "id": "work_219",
                        "name": "管理员",
                        "parent": "nav_1",
                        "url": "<@ms_lib.action url='/admin/admin/listView' />"
                    },
                    "work_220": {
                        "id": "work_220",
                        "name": "系统角色",
                        "parent": "nav_1",
                        "url": "<@ms_lib.action url='/admin/adminRole/listView' />"
                    },
                    "work_221": {
                        "id": "work_221",
                        "name": "系统菜单",
                        "parent": "nav_1",
                        "url": "<@ms_lib.action url='/admin/adminMenu/listView' />"
                    }
                }
            }
        };*/
        var SUBMENU_CONFIG = ${Request["com.ms.core.admin.admin.util.LoginUtil.LOGIN_KEY"]["menuMapJson"]};
        YJD.use('admin/main', function (a) {
            $(function () {
                a.init();
            });
        });
    </script>
</head>
<body>
<div class="wrap">
    <noscript><h1 class="noscript">您已禁用脚本，这样会导致页面不可用，请启用脚本后刷新页面</h1></noscript>
    <table width="100%" height="100%" style="table-layout:fixed;">
        <tr class="head">
            <th><a href="<@ms_lib.action />" class="logo">管理中心</a></th>
            <td>
                <div class="nav">
                    <!-- 菜单异步获取，采用json格式，由js处理菜单展示结构 -->
                    <ul id="J_B_main_block">
                    </ul>
                </div>
                <div class="login_info">
                    <span class="mr15">账户：  ${Request["com.ms.core.admin.admin.util.LoginUtil.LOGIN_KEY"]["adminModel"]["name"]}</span>
                    <span class="mr15"><a href='<@ms_lib.action url="/admin/adminLogin/exit" />' class="btn" style="color:#fff">注销</a></span>
                </div>
            </td>
        </tr>
        <tr class="tab">
            <td colspan="2">
                <div id="B_tabA" class="tabA">
                    <a href="" tabindex="-1" class="tabA_pre" id="J_prev" title="上一页">上一页</a>
                    <a href="" tabindex="-1" class="tabA_next" id="J_next" title="下一页">下一页</a>
                    <div style="margin:0 25px;min-height:1px;">
                        <div style="position:relative;height:30px;width:100%;overflow:hidden;">
                            <ul id="B_history" style="white-space:nowrap;position:absolute;left:0;top:0;">
                                <li class="current" data-id="default" tabindex="0"><span><a>git仓库</a></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr class="content">
            <th style="overflow:hidden;" id="content_height">
                <div id="B_menunav">
                    <div class="menubar">
                        <dl id="B_menubar">
                            <dt class="disabled"></dt>
                        </dl>
                    </div>
                </div>
            </th>
            <td id="B_frame" height="100%">
                <div class="options">
                    <a href="" class="refresh" id="J_refresh" title="刷新">刷新</a>
                    <a href="" id="J_fullScreen" class="full_screen" title="全屏">全屏</a>
                </div>
                <iframe id="iframe_default" src="https://gitee.com/likun_557/p-parent" style="height: 100%; width: 100%;" data-id="default" frameborder="0" scrolling="auto"></iframe>
            </td>
        </tr>
    </table>
</div>
</body>
</html>