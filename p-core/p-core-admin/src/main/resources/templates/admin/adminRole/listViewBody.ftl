<div class="wrap">
    <div class="mb10">
        <a href="<@ms_lib.action url='/admin/adminRole/addView' />" class="btn"><span class="add"></span>添加角色</a>
    </div>
    <div class="table_list">
        <table width="100%">
            <thead>
            <tr>
                <td width="100">ID</td>
                <td width="150">角色名</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody id="J_word_list">
            <#list pageHtmlModel.pageModel.dataList as item>
            <tr>
                <td>${item.id}</td>
                <td>${item.name}</td>
                <td>
                    <a href="<@ms_lib.action url='/admin/adminRole/editView/${item.id}' />" class="btn mr10 btn_green">配置</a>
                    <a href="<@ms_lib.action url='/admin/adminRole/delete/${item.id}' />" class="btn btn_orange J_ajaxRequest" data-msg="确定删除？" id="J_ajaxRequest_delete_${item.id}">删除</a>
                </td>
            </tr>
            </#list>
            </tbody>
        </table>
    </div>
    <#include "/lib/page.ftl" />
</div>