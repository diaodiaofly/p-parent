<#import "../../lib/ms_lib.ftl" as ms_lib />
<div class="wrap">
    <div class="nav">
        <a href="<@ms_lib.action url='/admin/adminRole/listView' />" class="btn">
            <b style="color:#ff9400;font-family:Arial;font-size:14px;">&lt;</b> 返回
        </a>
    </div>
    <form method="post" class="J_ajaxForm" autocomplete="off" action="<@ms_lib.action url='/admin/adminRole/save' />">
        <input type="hidden" name="model.id" value="${model.id}">
        <div class="h_a">${((model.id)??)?string("设置角色","添加角色")}</div>
        <div class="table_full">
            <table width="100%">
                <tr>
                    <th width="100">角色名：</th>
                    <td>
                        <input name="model.name" type="text" class="input length_2" value="${model.name}">
                    </td>
                </tr>
                <tr>
                    <th>描述：</th>
                    <td>
                        <textarea class="length_6" name="model.description">${model.description}</textarea>
                    </td>
                </tr>
            </table>
        </div>
        <div class="table_list">
            <table width="100%">
                <thead>
                <tr>
                    <td width="60"><label><input type="checkbox" class="J_check_all">全选</label></td>
                    <td width="50">ID</td>
                    <td width="150">菜单名称</td>
                    <td width="400">url</td>
                    <td>同级排序</td>
                </tr>
                </thead>
                <tbody>
                <#assign menuIdListIndex=0>
                <#list topAdminMenuModelList as tm>
                <tr style="font-weight:bold">
                    <td></td>
                    <td></td>
                    <td>${tm.name}</td>
                    <td></td>
                    <td></td>
                </tr>
                <#assign smList=(adminMenuModelListMap.get(tm.id))>
                <#if smList?? && smList?is_sequence>
                <#list smList as sm>
                <tr>
                    <td><input ${((sm.extData.checked)?? && sm.extData.checked)?string("checked","")} type="checkbox" name="menuIdList[${menuIdListIndex}]" value="${sm.id}" class="J_check"></td>
                    <td>${sm.id}</td>
                    <td>└─ ${sm.name}</td>
                    <td>${sm.url}</td>
                    <td>${sm.the_sort}</td>
                </tr>
                    <#assign menuIdListIndex++>
                </#list>
                </#if>
                </#list>
                </tbody>
            </table>
        </div>
        <div class="footer_wrap">
            <button class="btn btn_submit" type="submit">提交</button>
        </div>
</div>