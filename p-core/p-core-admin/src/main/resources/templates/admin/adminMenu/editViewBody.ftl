<#import "../../lib/ms_lib.ftl" as ms_lib />
<div class="wrap">
    <div class="nav">
        <a href="<@ms_lib.action url='/admin/adminMenu/listView' />" class="btn">
            <b style="color:#ff9400;font-family:Arial;font-size:14px;">&lt;</b> 返回
        </a>
    </div>
    <form method="post" class="J_ajaxForm" autocomplete="off" action="<@ms_lib.action url='/admin/adminMenu/save' />">
        <input type="hidden" name="id" value="${model.id}">
        <div class="h_a">添加菜单</div>
        <div class="table_full">
            <table width="100%">
                <#if pmodel??>
                <tr>
                    <th width="100">父菜单：</th>
                    <td>
                        <input name="pid" type="hidden" class="input length_6" value="${pmodel.id}">
                        ${pmodel.name}
                    </td>
                </tr>
                </#if>
                <tr>
                    <th width="100">菜单名称：</th>
                    <td>
                        <input name="name" type="text" class="input length_6" value="${model.name}">
                    </td>
                </tr>
                <tr>
                    <th width="100">url：</th>
                    <td>
                        <input name="url" type="text" class="input length_6" value="${model.url}">
                    </td>
                </tr>
                <tr>
                    <th width="100">同级排序：</th>
                    <td>
                        <input name="the_sort" type="text" class="input length_6" value="${model.the_sort}">
                    </td>
                </tr>
            </table>
        </div>
        <div class="footer_wrap">
            <button class="btn btn_submit" type="submit">提交</button>
        </div>
</div>