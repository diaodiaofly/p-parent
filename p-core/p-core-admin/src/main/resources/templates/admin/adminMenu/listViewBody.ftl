<div class="wrap">
    <div class="mb10">
        <a href="<@ms_lib.action url='/admin/adminMenu/addView' />" class="btn"><span class="add"></span>添加菜单</a>
    </div>
    <form id="J_word_form" class="J_ajaxForm" method="post" action="<@ms_lib.action url='/admin/adminMenu/saveBatch' />" autocomplete="off">
        <#assign modelIndex=0 >
        <div class="table_list">
            <table width="100%">
                <thead>
                <tr>
                    <td width="60"><label><input type="checkbox" class="J_check_all">全选</label></td>
                    <td width="200">名称</td>
                    <td width="400">url</td>
                    <td width="200">排序</td>
                    <td>操作</td>
                </tr>
                </thead>
                <tbody>
                <#list topAdminMenuModelList as tm>
                <tr>
                    <td></td>
                    <td><a class="btn mr10" href="<@ms_lib.action url='/admin/adminMenu/addView?pid=${tm.id}' />"><span class="add"></span>添加【${tm.name}】</a></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><input type="checkbox" name="adminMenuModels[${modelIndex}].id" value="${tm.id}" class="J_check"></td>
                    <td><input type="text" class="input length_2" name="adminMenuModels[${modelIndex}].name" value="${tm.name}"/></td>
                    <td></td>
                    <td><input type="text" class="input length_1" name="adminMenuModels[${modelIndex}].the_sort" value="${tm.the_sort}"/></td>
                    <td>
                        <a class="btn btn_orange J_ajaxRequest" data-msg="确定删除该控制器？" href="<@ms_lib.action url='/admin/adminMenu/delete/${sm.id}' />" id="J_ajaxRequest_${sm.id}">删除</a>
                    </td>
                </tr>
                    <#assign modelIndex=modelIndex+1>
                <#assign smList=(adminMenuModelListMap.get(tm.id))>
                <#if smList?? && smList?is_sequence>
                <#list smList as sm>
                <tr>
                    <td><input type="checkbox" name="adminMenuModels[${modelIndex}].id" value="${sm.id}" class="J_check"></td>
                    <td>└─ <input type="text" class="input length_2" name="adminMenuModels[${modelIndex}].name" value="${sm.name}"/></td>
                    <td><input type="text" class="input length_6" name="adminMenuModels[${modelIndex}].url" value="${sm.url}" /></td>
                    <td><input type="text" class="input length_1" name="adminMenuModels[${modelIndex}].the_sort" value="${sm.the_sort}"/></td>
                    <td>
                        <a class="btn btn_orange J_ajaxRequest" data-msg="确定删除该控制器？" href="<@ms_lib.action url='/admin/adminMenu/delete/${sm.id}' />" id="J_ajaxRequest_${sm.id}">删除</a>
                    </td>
                </tr>
                <#assign modelIndex=modelIndex+1>
                </#list>
                </#if>
                </#list>
                </tbody>
            </table>
        </div>

    ${cms}
        <div class="footer_wrap">
            <label class="mr20"><input type="checkbox" class="J_check_all">全选</label>
            <button class="btn btn_submit" type="submit">提交</button>
        </div>
    </form>
</div>