<#import "/lib/ms_lib.ftl" as ms_lib />
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        <#if layoutTitle??>
            ${layoutTitle}
        <#else>
            <@ms_lib.site key="title"/>
        </#if>
    </title>
    <meta http-equiv="expires" content="0">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <@ms_lib.link url="/p-core-admin/css/Admin.css"/>
    <@ms_lib.script url="/p-core-admin/js/yjd.js"/>
    <@ms_lib.script url="/p-core-admin/js/config.js"/>
    <@ms_lib.script url="/p-core-admin/js/plugins/Jquery/1.8.0/Jquery.js"/>
    <@ms_lib.script url="/p-core-admin/js/admin/common.js"/>
    <script>
        YJD.use('admin/common', function (common) {
            $(function () {
                common.init();
            });
        });
    </script>
</head>
<body>
<#include "${layoutBody}" />
</body>
</html>