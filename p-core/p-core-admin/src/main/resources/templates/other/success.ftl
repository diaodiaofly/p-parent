<#import "/lib/ms_lib.ftl" as ms_lib />
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>提示</title>
    <@ms_lib.link url="/p-core-admin/css/other/success.css" />
</head>
<body>
<div class="wrap">
    <div id="tips">
        <div class="tips-title">信息提示</div>
        <div class="tips-con">
            <table width="100%">
                <tr>
                    <td width='60' valign='top'><span class="tips-icon success"></span></td>
                    <td valign='top'>
                        <#if (Request["REQUEST_RESULT"]["msg"])??>
                            ${Request["REQUEST_RESULT"]["msg"]}
                        <#elseif msg??>
                            ${msg}
                        </#if>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <div class="tips-return">
                            <p class="jump">
                            <#if (Request["REQUEST_RESULT"]["extData"]["refer"])??>
                                页面自动 <a id="href" href='${Request["REQUEST_RESULT"]["extData"]["refer"]}'>跳转</a> 等待时间：<b id="wait">${(Request["REQUEST_RESULT"]["extData"]["REQUEST_WAIT_SECONDS_KEY"])!2}</b></p>
                            <#elseif (extData["refer"])??>
                                页面自动 <a id="href" href='${extData["refer"]}'>跳转</a> 等待时间：<b id="wait">${(extData["REQUEST_WAIT_SECONDS_KEY"])!2}</b></p>
                            <#else>
                                <a href="javascript:history.go(-1);" class="btn">返回</a>
                            </#if>
                            </p>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    (function () {
        var wait = document.getElementById('wait'), href = document.getElementById('href').href;
        var interval = setInterval(function () {
            var time = --wait.innerHTML;
            if (time <= 0) {
                location.href = href;
                clearInterval(interval);
            }
        }, 1000);
    })();
</script>
</body>
</html>