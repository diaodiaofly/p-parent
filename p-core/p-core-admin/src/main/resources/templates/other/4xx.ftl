<#import "/lib/ms_lib.ftl" as ms_lib />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>出错了 - <@ms_lib.site key="title" /></title>
</head>
<body>
<h2>您访问的资源不存在!</h2>
</body>
</html>