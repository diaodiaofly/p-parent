<#import "lib/ms_lib.ftl" as ms_lib />
<!doctype html>
<html>
<head>
    <meta charset="UTF-8"/>
    <title><@ms_lib.site key="title"/></title>
    <meta name="renderer" content="webkit"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <@ms_lib.link url="/p-core-admin/css/index.css" />
    <@ms_lib.script url="/p-core-admin/js/yjd.js"/>
    <@ms_lib.script url="/p-core-admin/js/config.js"/>
    <@ms_lib.script url="/p-core-admin/js/plugins/Jquery/1.8.0/Jquery.js"/>
    <script>
        YJD.use('admin/adminLogin', function (a) {
            $(function () {
                a.init();
            });
        });
    </script>
</head>
<body>
<div class="wrap">
    <h1 style="text-align: center; color:#FFF">后台管理中心</h1>
    <form method="post" name="login" action='<@ms_lib.action url="/admin/adminLogin" />'>
        <div class="login">
            <ul>
                <li><input class="input" id="name" name="name" type="text" placeholder="帐号" value="admin"/></li>
                <li><input class="input" id="password" name="password" type="password" placeholder="登录密码" value="123456" /></li>
            </ul>
            <input type="submit" name="dosubmit" class="btn" value="登录">
        </div>
    </form>
</div>
</body>
</html>