<#-- 链接资源 -->
<#macro static url>//${Request['SITE_CONFIG']['staticHost']}${url}</#macro>
<#-- js -->
<#macro script url>
<script src='<@static url=url />?v=20180821'></script>
</#macro>
<#-- js -->
<#macro link url>
<link rel="stylesheet" href='<@static url=url />?v=20180821'>
</#macro>
<#-- 链接 -->
<#macro action url=''>//${Request['SITE_CONFIG']['host']}${url}</#macro>
<#macro site key>${Request['SITE_CONFIG'][key]}</#macro>