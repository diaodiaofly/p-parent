<#if (pageHtmlModel.pageModel.dataList)?? && (pageHtmlModel.pageModel.count >= 2)>
<div class="footer_wrap">
    <div class="pages">
        <#list pageHtmlModel.pageToolsModels as item>
            <#if pageHtmlModel.pageModel.currentPage==item.pageIndex>
                <strong>${pageHtmlModel.pageModel.currentPage}</strong>
            <#elseif item.url??>
                <a href="${item.url}">${item.label}</a>
            <#else>
                <span>${item.label}</span>
            </#if>
        </#list>
    ${pageHtmlModel.pageModel.count} 条数据
    </div>
</div>
</#if>