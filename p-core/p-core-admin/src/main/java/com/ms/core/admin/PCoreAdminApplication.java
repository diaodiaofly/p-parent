package com.ms.core.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PCoreAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(PCoreAdminApplication.class, args);
    }
}
