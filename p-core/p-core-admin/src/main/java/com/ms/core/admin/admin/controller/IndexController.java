package com.ms.core.admin.admin.controller;

import com.ms.base.web.mvc.WebUtil;
import com.ms.core.admin.admin.util.LoginUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-08-13 13:53 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Controller
public class IndexController {
    @RequestMapping("/")
    public String index(HttpServletRequest request) {
        if (LoginUtil.isLogin(request)) {
            return "redirect:" + WebUtil.getControllerPath(MainController.MODULE_NAME, MainController.CONTROLLER_NAME, MainController.INDEX_METHOD);
        }
        return "index";
    }
}
