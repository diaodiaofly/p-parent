package com.ms.core.admin.admin.dto;

import com.ms.core.comm.admin.model.AdminModel;
import lombok.*;

import java.io.Serializable;
import java.util.Map;

/**
 * <b>description</b>：用户登录信息 <br>
 * <b>time</b>：2018-08-22 15:37 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AdminLoginDataDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private AdminModel adminModel;
    private Map<String, MenuDto> menuMap;
    private String menuMapJson;
}
