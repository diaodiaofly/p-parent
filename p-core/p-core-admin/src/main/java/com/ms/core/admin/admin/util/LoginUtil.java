package com.ms.core.admin.admin.util;

import com.ms.base.comm.util.FrameUtil;
import com.ms.core.admin.admin.dto.AdminLoginDataDto;
import com.ms.core.admin.admin.dto.MenuDto;
import com.ms.core.comm.admin.model.AdminMenuModel;
import com.ms.core.comm.admin.model.AdminModel;
import com.ms.core.comm.admin.util.AdminMenuUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <b>description</b>：登录工具类 <br>
 * <b>time</b>：2018-08-22 13:57 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public class LoginUtil {
    public static final String LOGIN_KEY = LoginUtil.class.getName() + ".LOGIN_KEY";
    public static final String IS_LOGIN_KEY = LoginUtil.class.getName() + ".IS_LOGIN_KEY";

    /**
     * 构建AdminLoginDataDto信息
     *
     * @param adminModel         管理员信息
     * @param adminMenuModelList 所有菜单信息
     * @param userMenuIdList     用户对应的菜单信息
     * @return
     */
    public static AdminLoginDataDto build(AdminModel adminModel, List<AdminMenuModel> adminMenuModelList, List<Long> userMenuIdList) {

        if (adminModel == null || adminMenuModelList == null || adminMenuModelList.isEmpty()) {
            return AdminLoginDataDto.builder().adminModel(adminModel).build();
        }
        if (adminModel.getIs_admin()) {
            userMenuIdList = adminMenuModelList.stream().filter(item -> !AdminMenuUtil.isRootMenu(item)).map(item -> item.getId()).collect(Collectors.toList());
        }
        if (userMenuIdList == null || userMenuIdList.isEmpty()) {
            return AdminLoginDataDto.builder().adminModel(adminModel).build();
        }
        userMenuIdList = userMenuIdList.stream().distinct().collect(Collectors.toList());
        adminMenuModelList.sort(Comparator.comparing(AdminMenuModel::getPid).thenComparing(AdminMenuModel::getThe_sort));

        Map<String, MenuDto> menuMap = FrameUtil.newLinkedHashMap();
        for (AdminMenuModel item : adminMenuModelList) {
            if (AdminMenuUtil.isRootMenu(item)) {
                String id = String.format("nav_%s", item.getId().toString());
                Map<String, MenuDto> items = FrameUtil.newLinkedHashMap();
                for (AdminMenuModel item1 : adminMenuModelList) {
                    if (item.getId().equals(item1.getPid()) && userMenuIdList.contains(item1.getId())) {
                        String id1 = String.format("nav_%s", item1.getId().toString());
                        items.put(id1, MenuDto.builder().id(id1).name(item1.getName()).parent(id).url(item1.getUrl()).build());
                    }
                }
                if (!items.isEmpty()) {
                    menuMap.put(id, MenuDto.builder().id(id).name(item.getName()).items(items).build());
                }
            }
        }
        return AdminLoginDataDto.builder().adminModel(adminModel).menuMap(menuMap).menuMapJson(FrameUtil.json(menuMap)).build();
    }

    /**
     * 将用户登录信息放入session
     *
     * @param request
     * @param adminLoginDataDto
     */
    public static void putSession(HttpServletRequest request, AdminLoginDataDto adminLoginDataDto) {
        request.setAttribute(LOGIN_KEY, adminLoginDataDto);
        request.getSession().setAttribute(LOGIN_KEY, adminLoginDataDto);
        request.setAttribute(IS_LOGIN_KEY, Boolean.TRUE);
        request.getSession().setAttribute(IS_LOGIN_KEY, Boolean.TRUE);
    }

    /**
     * 判断用户是否登录了
     *
     * @param request
     * @return
     */
    public static boolean isLogin(HttpServletRequest request) {
        return request.getAttribute(IS_LOGIN_KEY) != null || request.getSession().getAttribute(IS_LOGIN_KEY) != null;
    }

    /**
     * 获取登录信息
     *
     * @param request
     * @return
     */
    public static AdminLoginDataDto getAdminLoginDataDto(HttpServletRequest request) {
        Object object = request.getAttribute(LOGIN_KEY);
        if (Objects.isNull(object)) {
            object = request.getSession().getAttribute(LOGIN_KEY);
        }
        if (!Objects.isNull(object)) {
            request.setAttribute(LOGIN_KEY, object);
            request.setAttribute(IS_LOGIN_KEY, Boolean.TRUE);
        }
        return (AdminLoginDataDto) object;
    }

    /**
     * 退出
     *
     * @param request
     */
    public static void loginOut(HttpServletRequest request) {
        request.getSession().removeAttribute(LOGIN_KEY);
        request.removeAttribute(LOGIN_KEY);
        request.getSession().removeAttribute(IS_LOGIN_KEY);
        request.removeAttribute(IS_LOGIN_KEY);
    }
}
