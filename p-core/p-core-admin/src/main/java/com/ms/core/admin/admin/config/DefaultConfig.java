package com.ms.core.admin.admin.config;

import com.ms.core.admin.admin.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * <b>description</b>：配置文件 <br>
 * <b>time</b>：2018-08-22 16:40 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Configuration
public class DefaultConfig extends WebMvcConfigurationSupport {

    @Bean
    public LoginInterceptor loginInterceptor() {
        return new LoginInterceptor();
    }

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        LoginInterceptor interceptor = this.loginInterceptor();
        registry.addInterceptor(interceptor).order(interceptor.getOrder()).addPathPatterns("/**");
    }
}
