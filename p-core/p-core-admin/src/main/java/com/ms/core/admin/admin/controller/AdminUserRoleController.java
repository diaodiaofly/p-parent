package com.ms.core.admin.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.util.DateUtil;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import com.ms.base.web.mvc.BaseViewController;
import com.ms.base.web.mvc.IModuleViewController;
import com.ms.base.web.mvc.WebUtil;
import com.ms.base.web.page.PageHtmlModel;
import com.ms.base.web.page.PageHtmlUtil;
import com.ms.core.api.admin.AdminUserRoleClient;
import com.ms.core.comm.admin.model.AdminUserRoleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * <b>description</b>：后台管理系统用户管理 <br>
 * <b>time</b>：2018-08-16 14:43 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Controller
@RequestMapping(AdminUserRoleController.REQUEST_MAPPING)
public class AdminUserRoleController extends BaseViewController implements IModuleViewController {

    public static final String MODULE_NAME = "admin";
    public static final String CONTROLLER_NAME = "adminUserRole";
    public static final String REQUEST_MAPPING = WebUtil.URL_SPLIT + MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME;

    public static final String LISTVIEW_METHOD_NAME = "listView";
    public static final String LISTVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + LISTVIEW_METHOD_NAME;
    public static final String LISTVIEW_REQUEST_MAPPING_PATH = REQUEST_MAPPING + WebUtil.URL_SPLIT + LISTVIEW_METHOD_NAME;

    public static final String ADDVIEW_METHOD_NAME = "addView";
    public static final String ADDVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + ADDVIEW_METHOD_NAME;

    public static final String EDITVIEW_METHOD_NAME = "editView";
    public static final String EDITVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + EDITVIEW_METHOD_NAME + WebUtil.URL_SPLIT + "{id}";

    public static final String SAVE_METHOD_NAME = "save";
    public static final String SAVE_REQUEST_MAPPING = WebUtil.URL_SPLIT + SAVE_METHOD_NAME;

    public static final String DELETE_METHOD_NAME = "delete";
    public static final String DELETE_REQUEST_MAPPING = WebUtil.URL_SPLIT + DELETE_METHOD_NAME + WebUtil.URL_SPLIT + "{id}";


    @Autowired
    private AdminUserRoleClient adminUserRoleClient;

    /**
     * 列表页面
     *
     * @param modelMap 数据存储对象
     * @return
     * @throws Exception
     */
    @RequestMapping(LISTVIEW_REQUEST_MAPPING)
    public String listView(HttpServletRequest request, ModelMap modelMap) throws Exception {
        PageParamModel pageParamModel = WebUtil.getPageParamModel(request);
        PageModel<AdminUserRoleModel> pageModel = this.adminUserRoleClient.getPageModel1(pageParamModel, true).okData();
        PageHtmlModel<AdminUserRoleModel> pageHtmlModel = PageHtmlUtil.build(pageModel, pageParamModel, WebUtil.getPathUrl(LISTVIEW_REQUEST_MAPPING_PATH));
        modelMap.addAttribute("pageHtmlModel", pageHtmlModel);
        return this.getViewName(LISTVIEW_METHOD_NAME);
    }

    /**
     * 跳转到新增页面
     *
     * @return 数据存储对象
     */
    @RequestMapping(ADDVIEW_REQUEST_MAPPING)
    public String addView() {
        return this.getViewName(EDITVIEW_METHOD_NAME);
    }

    /**
     * 跳转到编辑页面
     *
     * @param id       对象id
     * @param modelMap
     * @return
     * @throws Exception
     */
    @RequestMapping(EDITVIEW_REQUEST_MAPPING)
    public String editView(@PathVariable("id") long id, ModelMap modelMap) throws Exception {
        modelMap.put("model", this.adminUserRoleClient.getModelById(id, true).okData());
        return this.getViewName(EDITVIEW_METHOD_NAME);
    }

    /**
     * 新增或者保存
     *
     * @param model 对象信息
     * @return
     * @throws Exception
     */
    @RequestMapping(SAVE_REQUEST_MAPPING)
    @ResponseBody
    public ResultDto save(AdminUserRoleModel model) throws Exception {
        if (Objects.isNull(model.getId())) {
            this.adminUserRoleClient.insert(model).ok();
            return this.successResultDto("新增成功!");
        } else {
            this.adminUserRoleClient.update(model).ok();
            return this.successPathResultDto("修改成功!", LISTVIEW_REQUEST_MAPPING_PATH);
        }
    }

    /**
     * 删除数据
     *
     * @param id 对象id
     * @return
     * @throws Exception
     */
    @RequestMapping(DELETE_REQUEST_MAPPING)
    @ResponseBody
    public ResultDto delete(@PathVariable("id") long id) throws Exception {
        this.adminUserRoleClient.deleteById(id).ok();
        return this.successResultDto("删除成功!");
    }

    @Override
    public String moduleName() {
        return MODULE_NAME;
    }

    @Override
    public String controllerName() {
        return CONTROLLER_NAME;
    }
}