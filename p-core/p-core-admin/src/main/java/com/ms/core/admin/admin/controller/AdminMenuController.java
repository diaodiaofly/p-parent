package com.ms.core.admin.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.web.mvc.BaseViewController;
import com.ms.base.web.mvc.IModuleViewController;
import com.ms.base.web.mvc.WebUtil;
import com.ms.core.api.admin.AdminMenuClient;
import com.ms.core.comm.admin.dto.SaveBatchAdminMenuRequest;
import com.ms.core.comm.admin.model.AdminMenuModel;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <b>description</b>：后台管理系统用户管理 <br>
 * <b>time</b>：2018-08-16 14:43 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Controller
@RequestMapping(AdminMenuController.REQUEST_MAPPING)
public class AdminMenuController extends BaseViewController implements IModuleViewController {

    public static final String MODULE_NAME = "admin";
    public static final String CONTROLLER_NAME = "adminMenu";
    public static final String REQUEST_MAPPING = WebUtil.URL_SPLIT + MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME;

    public static final String LISTVIEW_METHOD_NAME = "listView";
    public static final String LISTVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + LISTVIEW_METHOD_NAME;
    public static final String LISTVIEW_REQUEST_MAPPING_PATH = REQUEST_MAPPING + WebUtil.URL_SPLIT + LISTVIEW_METHOD_NAME;

    public static final String ADDVIEW_METHOD_NAME = "addView";
    public static final String ADDVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + ADDVIEW_METHOD_NAME;

    public static final String EDITVIEW_METHOD_NAME = "editView";
    public static final String EDITVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + EDITVIEW_METHOD_NAME + WebUtil.URL_SPLIT + "{id}";

    public static final String DELETE_METHOD_NAME = "delete";
    public static final String DELETE_REQUEST_MAPPING = WebUtil.URL_SPLIT + DELETE_METHOD_NAME + WebUtil.URL_SPLIT + "{id}";

    public static final String SAVEBATCH_METHOD_NAME = "saveBatch";
    public static final String SAVEBATCH_REQUEST_MAPPING = WebUtil.URL_SPLIT + SAVEBATCH_METHOD_NAME;

    public static final String SAVE_METHOD_NAME = "save";
    public static final String SAVE_REQUEST_MAPPING = WebUtil.URL_SPLIT + SAVE_METHOD_NAME;

    @Autowired
    private AdminMenuClient adminMenuClient;

    /**
     * 列表页面
     *
     * @param modelMap 数据存储对象
     * @return
     * @throws Exception
     */
    @RequestMapping(LISTVIEW_REQUEST_MAPPING)
    public String listView(HttpServletRequest request, ModelMap modelMap) throws Exception {
        List<AdminMenuModel> adminMenuModelList = this.adminMenuClient.getModelList(Collections.emptyMap(), true).okData();
        //查找所有pid=0的(第一集菜单)
        List<AdminMenuModel> topAdminMenuModelList = adminMenuModelList.stream().filter(a -> a.getPid() == 0L).collect(Collectors.toList());
        topAdminMenuModelList.sort(Comparator.comparing(AdminMenuModel::getThe_sort));
        //按照pid进行分组
        Map<Long, List<AdminMenuModel>> adminMenuModelListMap = adminMenuModelList.stream().collect(Collectors.groupingBy(AdminMenuModel::getPid));
        for (List<AdminMenuModel> item : adminMenuModelListMap.values()) {
            item.sort(Comparator.comparing(AdminMenuModel::getThe_sort));
        }
        modelMap.addAttribute("topAdminMenuModelList", topAdminMenuModelList);
        modelMap.addAttribute("adminMenuModelListMap", adminMenuModelListMap);
        return this.getViewName(LISTVIEW_METHOD_NAME);
    }

    /**
     * 跳转到新增页面
     *
     * @return 数据存储对象
     */
    @RequestMapping(ADDVIEW_REQUEST_MAPPING)
    public String addView(HttpServletRequest request, ModelMap modelMap) throws Exception {
        String pid = request.getParameter("pid");
        if (StringUtils.isNotBlank(pid)) {
            modelMap.put("pid", pid);
            AdminMenuModel pmodel = this.adminMenuClient.getModelById(Long.parseLong(pid), true).okData();
            modelMap.put("pmodel", pmodel);
        }
        return this.getViewName(EDITVIEW_METHOD_NAME);
    }

    /**
     * 保存
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(SAVE_REQUEST_MAPPING)
    @ResponseBody
    public ResultDto<Void> save(AdminMenuModel model) throws Exception {
        this.adminMenuClient.insert(model).ok();
        return this.successResultDto("新增成功!");
    }

    /**
     * 批量更新菜单信息
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @RequestMapping(SAVEBATCH_REQUEST_MAPPING)
    @ResponseBody
    public ResultDto<Void> saveBatch(SaveBatchAdminMenuRequest dto) throws Exception {
        this.adminMenuClient.saveBatch(dto).ok();
        return this.successResultDto("更新数据成功!");
    }

    /**
     * 删除数据
     *
     * @param id 对象id
     * @return
     * @throws Exception
     */
    @RequestMapping(DELETE_REQUEST_MAPPING)
    @ResponseBody
    public ResultDto delete(@PathVariable("id") long id) throws Exception {
        this.adminMenuClient.deleteById(id).ok();
        return this.successResultDto("删除成功!");
    }

    @Override
    public String moduleName() {
        return MODULE_NAME;
    }

    @Override
    public String controllerName() {
        return CONTROLLER_NAME;
    }
}