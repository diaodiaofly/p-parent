package com.ms.core.admin.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.util.FrameUtil;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import com.ms.base.web.mvc.BaseViewController;
import com.ms.base.web.mvc.IModuleViewController;
import com.ms.base.web.mvc.WebUtil;
import com.ms.base.web.page.PageHtmlModel;
import com.ms.base.web.page.PageHtmlUtil;
import com.ms.core.api.admin.AdminMenuClient;
import com.ms.core.api.admin.AdminRoleClient;
import com.ms.core.api.admin.AdminRoleMenuClient;
import com.ms.core.comm.admin.dto.SaveAdminRoleRequest;
import com.ms.core.comm.admin.model.AdminMenuModel;
import com.ms.core.comm.admin.model.AdminRoleMenuModel;
import com.ms.core.comm.admin.model.AdminRoleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <b>description</b>：后台管理系统用户管理 <br>
 * <b>time</b>：2018-08-16 14:43 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Controller
@RequestMapping(AdminRoleController.REQUEST_MAPPING)
public class AdminRoleController extends BaseViewController implements IModuleViewController {

    public static final String MODULE_NAME = "admin";
    public static final String CONTROLLER_NAME = "adminRole";
    public static final String REQUEST_MAPPING = WebUtil.URL_SPLIT + MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME;

    public static final String LISTVIEW_METHOD_NAME = "listView";
    public static final String LISTVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + LISTVIEW_METHOD_NAME;
    public static final String LISTVIEW_REQUEST_MAPPING_PATH = REQUEST_MAPPING + WebUtil.URL_SPLIT + LISTVIEW_METHOD_NAME;

    public static final String ADDVIEW_METHOD_NAME = "addView";
    public static final String ADDVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + ADDVIEW_METHOD_NAME;

    public static final String EDITVIEW_METHOD_NAME = "editView";
    public static final String EDITVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + EDITVIEW_METHOD_NAME + WebUtil.URL_SPLIT + "{id}";

    public static final String SAVE_METHOD_NAME = "save";
    public static final String SAVE_REQUEST_MAPPING = WebUtil.URL_SPLIT + SAVE_METHOD_NAME;

    public static final String DELETE_METHOD_NAME = "delete";
    public static final String DELETE_REQUEST_MAPPING = WebUtil.URL_SPLIT + DELETE_METHOD_NAME + WebUtil.URL_SPLIT + "{id}";


    @Autowired
    private AdminRoleClient adminRoleClient;
    @Autowired
    private AdminMenuClient adminMenuClient;
    @Autowired
    private AdminRoleMenuClient adminRoleMenuClient;

    /**
     * 列表页面
     *
     * @param modelMap 数据存储对象
     * @return
     * @throws Exception
     */
    @RequestMapping(LISTVIEW_REQUEST_MAPPING)
    public String listView(HttpServletRequest request, ModelMap modelMap) throws Exception {
        PageParamModel pageParamModel = WebUtil.getPageParamModel(request);
        PageModel<AdminRoleModel> pageModel = this.adminRoleClient.getPageModel1(pageParamModel, true).okData();
        PageHtmlModel<AdminRoleModel> pageHtmlModel = PageHtmlUtil.build(pageModel, pageParamModel, WebUtil.getPathUrl(LISTVIEW_REQUEST_MAPPING_PATH));
        modelMap.addAttribute("pageHtmlModel", pageHtmlModel);
        return this.getViewName(LISTVIEW_METHOD_NAME);
    }

    /**
     * 跳转到新增页面
     *
     * @return 数据存储对象
     */
    @RequestMapping(ADDVIEW_REQUEST_MAPPING)
    public String addView(ModelMap modelMap) throws Exception {
        List<AdminMenuModel> adminMenuModelList = this.adminMenuClient.getModelList(Collections.emptyMap(), true).okData();
        modelMap.put("adminMenuModelList", adminMenuModelList);
        //查找所有pid=0的(第一集菜单)
        List<AdminMenuModel> topAdminMenuModelList = adminMenuModelList.stream().filter(a -> a.getPid() == 0L).collect(Collectors.toList());
        //按照pid进行分组
        Map<Long, List<AdminMenuModel>> adminMenuModelListMap = adminMenuModelList.stream().collect(Collectors.groupingBy(AdminMenuModel::getPid));
        modelMap.addAttribute("topAdminMenuModelList", topAdminMenuModelList);
        modelMap.addAttribute("adminMenuModelListMap", adminMenuModelListMap);
        return this.getViewName(EDITVIEW_METHOD_NAME);
    }

    /**
     * 跳转到编辑页面
     *
     * @param id       对象id
     * @param modelMap
     * @return
     * @throws Exception
     */
    @RequestMapping(EDITVIEW_REQUEST_MAPPING)
    public String editView(@PathVariable("id") long id, ModelMap modelMap) throws Exception {
        List<AdminRoleMenuModel> adminRoleMenuModelList = this.adminRoleMenuClient.getModelList(FrameUtil.newHashMap(AdminRoleMenuModel.ROLE_ID_COL, id), true).okData();
        modelMap.put("adminRoleMenuModelList", adminRoleMenuModelList);
        List<AdminMenuModel> adminMenuModelList = this.adminMenuClient.getModelList(Collections.emptyMap(), true).okData();
        modelMap.put("adminMenuModelList", adminMenuModelList);
        adminMenuModelList.forEach(a -> {
            adminRoleMenuModelList.forEach(b -> {
                if (a.getId().equals(b.getMenu_id())) {
                    a.putExtData("checked", true);
                }
            });
        });
        //查找所有pid=0的(第一集菜单)
        List<AdminMenuModel> topAdminMenuModelList = adminMenuModelList.stream().filter(a -> a.getPid() == 0L).collect(Collectors.toList());
        //按照pid进行分组
        Map<Long, List<AdminMenuModel>> adminMenuModelListMap = adminMenuModelList.stream().collect(Collectors.groupingBy(AdminMenuModel::getPid));
        modelMap.addAttribute("topAdminMenuModelList", topAdminMenuModelList);
        modelMap.addAttribute("adminMenuModelListMap", adminMenuModelListMap);

        modelMap.put("model", this.adminRoleClient.getModelById(id, true).okData());
        return this.getViewName(EDITVIEW_METHOD_NAME);
    }

    /**
     * 新增或者保存
     *
     * @param dto 对象信息
     * @return
     * @throws Exception
     */
    @RequestMapping(SAVE_REQUEST_MAPPING)
    @ResponseBody
    public ResultDto save(SaveAdminRoleRequest dto) throws Exception {
        if (Objects.isNull(dto.getModel().getId())) {
            this.adminRoleClient.save(dto).ok();
            return this.successResultDto("新增成功!");
        } else {
            this.adminRoleClient.save(dto).ok();
            return this.successPathResultDto("修改成功!", LISTVIEW_REQUEST_MAPPING_PATH);
        }
    }

    /**
     * 删除数据
     *
     * @param id 对象id
     * @return
     * @throws Exception
     */
    @RequestMapping(DELETE_REQUEST_MAPPING)
    @ResponseBody
    public ResultDto delete(@PathVariable("id") long id) throws Exception {
        this.adminRoleClient.deleteById(id).ok();
        return this.successResultDto("删除成功!");
    }

    @Override
    public String moduleName() {
        return MODULE_NAME;
    }

    @Override
    public String controllerName() {
        return CONTROLLER_NAME;
    }
}