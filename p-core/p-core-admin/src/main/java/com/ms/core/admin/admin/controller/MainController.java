package com.ms.core.admin.admin.controller;

import com.ms.base.web.mvc.BaseViewController;
import com.ms.base.web.mvc.IModuleViewController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <b>description</b>：主页面controller <br>
 * <b>time</b>：2018-08-14 09:20 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Controller
@RequestMapping("/" + MainController.MODULE_NAME + "/" + MainController.CONTROLLER_NAME)
public class MainController extends BaseViewController implements IModuleViewController {

    public static final String MODULE_NAME = "admin";
    public static final String CONTROLLER_NAME = "main";
    public static final String INDEX_METHOD = "index";

    /**
     * 主页面
     *
     * @return
     */
    @RequestMapping("/" + INDEX_METHOD)
    public String index() {
        return this.getViewName(INDEX_METHOD);
    }

    @Override
    public String moduleName() {
        return MODULE_NAME;
    }

    @Override
    public String controllerName() {
        return MainController.CONTROLLER_NAME;
    }
}
