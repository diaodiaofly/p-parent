package com.ms.core.admin.admin.interceptor;

import com.ms.base.comm.util.FrameUtil;
import com.ms.base.web.interceptor.BaseInterceptor;
import com.ms.base.web.mvc.BaseViewController;
import com.ms.base.web.mvc.WebUtil;
import com.ms.core.admin.admin.dto.AdminLoginDataDto;
import com.ms.core.admin.admin.util.LoginUtil;
import org.springframework.core.Ordered;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <b>description</b>：登录判断拦截器 <br>
 * <b>time</b>：2018-08-22 16:34 <br>
 * <b>author</b>： ready likun_557@163.com
 */
public class LoginInterceptor extends BaseInterceptor implements Ordered {
    @Override
    public boolean doPreHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        AdminLoginDataDto adminLoginDataDto = LoginUtil.getAdminLoginDataDto(request);
        if (adminLoginDataDto == null) {
            FrameUtil.throwBaseException("请先登录!", FrameUtil.newHashMap(BaseViewController.REFER_KEY, WebUtil.getPathUrl("")));
        }
        return true;
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    @Override
    protected List<String> getNoUrlFiles() {
        return FrameUtil.newArrayList("nologinurl.txt");
    }
}
