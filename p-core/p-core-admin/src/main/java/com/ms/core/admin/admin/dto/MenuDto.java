package com.ms.core.admin.admin.dto;

import lombok.*;

import java.io.Serializable;
import java.util.Map;

/**
 * <b>description</b>：菜单信息 <br>
 * <b>time</b>：2018-08-22 15:58 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class MenuDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private String parent;
    private String url;
    private Map<String, MenuDto> items;
}
