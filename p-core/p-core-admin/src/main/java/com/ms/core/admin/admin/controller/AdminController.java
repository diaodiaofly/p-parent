package com.ms.core.admin.admin.controller;

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.util.DateUtil;
import com.ms.base.comm.util.FrameUtil;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import com.ms.base.web.mvc.BaseViewController;
import com.ms.base.web.mvc.IModuleViewController;
import com.ms.base.web.mvc.WebUtil;
import com.ms.base.web.page.PageHtmlModel;
import com.ms.base.web.page.PageHtmlUtil;
import com.ms.core.api.admin.AdminClient;
import com.ms.core.api.admin.AdminRoleClient;
import com.ms.core.api.admin.AdminUserRoleClient;
import com.ms.core.comm.admin.dto.SaveAdminRequest;
import com.ms.core.comm.admin.model.AdminModel;
import com.ms.core.comm.admin.model.AdminRoleModel;
import com.ms.core.comm.admin.model.AdminUserRoleModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * <b>description</b>：后台管理系统用户管理 <br>
 * <b>time</b>：2018-08-16 14:43 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Controller
@RequestMapping(AdminController.REQUEST_MAPPING)
@Slf4j
public class AdminController extends BaseViewController implements IModuleViewController {

    public static final String MODULE_NAME = "admin";
    public static final String CONTROLLER_NAME = "admin";
    public static final String REQUEST_MAPPING = WebUtil.URL_SPLIT + MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME;

    public static final String LISTVIEW_METHOD_NAME = "listView";
    public static final String LISTVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + LISTVIEW_METHOD_NAME;
    public static final String LISTVIEW_REQUEST_MAPPING_PATH = REQUEST_MAPPING + WebUtil.URL_SPLIT + LISTVIEW_METHOD_NAME;

    public static final String ADDVIEW_METHOD_NAME = "addView";
    public static final String ADDVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + ADDVIEW_METHOD_NAME;

    public static final String EDITVIEW_METHOD_NAME = "editView";
    public static final String EDITVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + EDITVIEW_METHOD_NAME + WebUtil.URL_SPLIT + "{id}";

    public static final String SAVE_METHOD_NAME = "save";
    public static final String SAVE_REQUEST_MAPPING = WebUtil.URL_SPLIT + SAVE_METHOD_NAME;

    public static final String DELETE_METHOD_NAME = "delete";
    public static final String DELETE_REQUEST_MAPPING = WebUtil.URL_SPLIT + DELETE_METHOD_NAME + WebUtil.URL_SPLIT + "{id}";


    @Autowired
    private AdminClient adminClient;
    @Autowired
    private AdminRoleClient adminRoleClient;
    @Autowired
    private AdminUserRoleClient adminUserRoleClient;

    /**
     * 列表页面
     *
     * @param modelMap 数据存储对象
     * @return
     * @throws Exception
     */
    @RequestMapping(LISTVIEW_REQUEST_MAPPING)
    public String listView(HttpServletRequest request, ModelMap modelMap) throws Exception {
        log.info("{}",request.getSession().getAttribute("pageHtmlModel"));
        PageParamModel pageParamModel = WebUtil.getPageParamModel(request);
        PageModel<AdminModel> pageModel = this.adminClient.getPageModel1(pageParamModel, true).okData();
        pageModel.getDataList().forEach((AdminModel model) -> {
            model.putExtData("is_admin", model.getIs_admin() ? "是" : "否");
            model.putExtData("last_login_time", DateUtil.timestampToDateString(model.getLast_login_time()));
        });
        PageHtmlModel<AdminModel> pageHtmlModel = PageHtmlUtil.build(pageModel, pageParamModel, WebUtil.getPathUrl(LISTVIEW_REQUEST_MAPPING_PATH));
        modelMap.addAttribute("pageHtmlModel", pageHtmlModel);
        request.getSession().setAttribute("pageHtmlModel",pageHtmlModel);
        log.info("{}",request.getSession().getAttribute("pageHtmlModel"));
        return this.getViewName(LISTVIEW_METHOD_NAME);
    }

    /**
     * 跳转到新增页面
     *
     * @return 数据存储对象
     */
    @RequestMapping(ADDVIEW_REQUEST_MAPPING)
    public String addView(ModelMap modelMap) throws Exception {
        List<AdminRoleModel> adminRoleModelList = this.adminRoleClient.getModelList(Collections.emptyMap(), true).okData();
        modelMap.put("adminRoleModelList", adminRoleModelList);
        return this.getViewName(EDITVIEW_METHOD_NAME);
    }

    /**
     * 跳转到编辑页面
     *
     * @param id       对象id
     * @param modelMap
     * @return
     * @throws Exception
     */
    @RequestMapping(EDITVIEW_REQUEST_MAPPING)
    public String editView(@PathVariable("id") long id, ModelMap modelMap) throws Exception {
        List<AdminUserRoleModel> adminUserRoleModelList = this.adminUserRoleClient.getModelList(FrameUtil.newHashMap(AdminUserRoleModel.ADMIN_ID_COL, id), true).okData();
        modelMap.put("adminUserRoleModelList", adminUserRoleModelList);
        List<AdminRoleModel> adminRoleModelList = this.adminRoleClient.getModelList(Collections.emptyMap(), true).okData();
        modelMap.put("adminRoleModelList", adminRoleModelList);
        adminRoleModelList.forEach(a -> {
            adminUserRoleModelList.forEach(b -> {
                if (a.getId().equals(b.getRole_id())) {
                    a.putExtData("checked", true);
                }
            });
        });
        modelMap.put("model", this.adminClient.getModelById(id, true).okData());
        return this.getViewName(EDITVIEW_METHOD_NAME);
    }

    /**
     * 新增或者保存
     *
     * @param dto 对象信息
     * @return
     * @throws Exception
     */
    @RequestMapping(SAVE_REQUEST_MAPPING)
    @ResponseBody
    public ResultDto save(SaveAdminRequest dto) throws Exception {
        if (Objects.isNull(dto.getModel().getId())) {
            this.adminClient.save(dto).ok();
            return this.successResultDto("新增成功!");
        } else {
            this.adminClient.save(dto).ok();
            return this.successPathResultDto("修改成功!", LISTVIEW_REQUEST_MAPPING_PATH);
        }
    }

    /**
     * 删除数据
     *
     * @param id 对象id
     * @return
     * @throws Exception
     */
    @RequestMapping(DELETE_REQUEST_MAPPING)
    @ResponseBody
    public ResultDto delete(@PathVariable("id") long id) throws Exception {
        this.adminClient.deleteById(id).ok();
        return this.successResultDto("删除成功!");
    }

    @Override
    public String moduleName() {
        return MODULE_NAME;
    }

    @Override
    public String controllerName() {
        return CONTROLLER_NAME;
    }
}