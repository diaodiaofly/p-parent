package ${controllerData.classData.pkgName};

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.util.Constant;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import com.ms.base.page.core.PageUtil;
import com.ms.base.web.mvc.WebUtil;
import ${modelData.classData.name};
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * <b>description</b>：${remark} http接口 <br>
 * <b>time</b>：${dateTime} <br>
 * <b>author</b>：${author}
 */
public interface ${controllerData.classData.simpleName} {

    String MODULE_NAME = "${moduleName}";
    String CONTROLLER_NAME = "${controllerData.controllerName}";
    String INSERT_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "insert";
    String UPDATE_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "update";
    String DELETEBYID_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "deleteById";
    String GETMODELLISTCOUNT_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelListCount";
    String GETMODELLIST_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelList";
    String GETMODELBYID_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelById";
    String GETMODELSBYIDS_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelsByIds";
    String GETMODELMAPBYIDS_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelMapByIds";
    String GETMODELONE_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelOne";
    String GETPAGEMODEL_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getPageModel";
    String GETPAGEMODEL1_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getPageModel1";
    String GETMODELLIST1_REQUEST_MAPPING = MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME + WebUtil.URL_SPLIT + "getModelList1";

    /**
     * 插入
     *
     * @param model 数据
     * @return
     * @throws Exception
     */
    @RequestMapping(INSERT_REQUEST_MAPPING)
    ResultDto<${modelData.classData.simpleName}> insert(@RequestBody ${modelData.classData.simpleName} model) throws Exception;

    /**
     * 更新
     *
     * @param model 数据
     * @return 影响行数
     * @throws Exception
     */
    @RequestMapping(UPDATE_REQUEST_MAPPING)
    ResultDto<Integer> update(@RequestBody ${modelData.classData.simpleName} model) throws Exception;

    /**
     * 根据id删除数据
     *
     * @param id id
     * @return
     * @throws Exception
     */
    @RequestMapping(DELETEBYID_REQUEST_MAPPING)
    ResultDto<Integer> deleteById(@RequestParam("id") long id) throws Exception;

    /**
     * 获取记录行数
     *
     * @param map    查询条件
     * @param master 主从查询标志
     * @return
     */
    @RequestMapping(GETMODELLISTCOUNT_REQUEST_MAPPING)
    ResultDto<Long> getModelListCount(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 获取记录列表
     *
     * @param map    查询条件
     * @param master 主从查询标志
     * @return
     */
    @RequestMapping(GETMODELLIST_REQUEST_MAPPING)
    ResultDto<List<${modelData.classData.simpleName}>> getModelList(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 根据对象id查询数据
     *
     * @param id     对象id
     * @param master 主从查询标志
     * @return 返回id对应的对象
     */
    @RequestMapping(GETMODELBYID_REQUEST_MAPPING)
    ResultDto<${modelData.classData.simpleName}> getModelById(@RequestParam("id") long id, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 查询id列表对应的对象列表
     *
     * @param idList id列表
     * @param master 主从查询标志
     * @return
     */
    @RequestMapping(GETMODELSBYIDS_REQUEST_MAPPING)
    ResultDto<List<${modelData.classData.simpleName}>> getModelsByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 根据idList获取对象列表的map，key为对象的id，value为对象
     *
     * @param idList id列表
     * @param master 主从查询标志
     * @return id->item 列表
     */
    @RequestMapping(GETMODELMAPBYIDS_MAPPING)
    ResultDto<Map<Long, ${modelData.classData.simpleName}>> getModelMapByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 获取一个对象
     *
     * @param paramMap 参数
     * @param master   主从查询标志
     * @return
     */
    @RequestMapping(GETMODELONE_REQUEST_MAPPING)
    ResultDto<${modelData.classData.simpleName}> getModelOne(@RequestBody Map<String, Object> paramMap, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 分页查询
     *
     * @param map    查询条件
     * @param page   当前页数
     * @param rows   每页行数
     * @param master 主从查询标志
     * @return
     * @throws Exception
     */
    @RequestMapping(GETPAGEMODEL_REQUEST_MAPPING)
    ResultDto<PageModel<${modelData.classData.simpleName}>> getPageModel(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.PAGE_KEY) int page, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;

    /**
     * 分页查询
     *
     * @param pageParamModel 分页参数
     * @param master         主从查询标志
     * @return
     * @throws Exception
     */
    @RequestMapping(GETPAGEMODEL1_REQUEST_MAPPING)
    ResultDto<PageModel<${modelData.classData.simpleName}>> getPageModel1(@RequestBody PageParamModel pageParamModel, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;


    /**
     * 查询列表
     *
     * @param map    查询条件
     * @param skip   跳过的行数
     * @param rows   行数
     * @param master 主从查询标志
     * @return
     * @throws Exception
     */
    @RequestMapping(GETMODELLIST1_REQUEST_MAPPING)
    ResultDto<List<${modelData.classData.simpleName}>> getModelList1(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.SKIP_KEY) int skip, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception;
}