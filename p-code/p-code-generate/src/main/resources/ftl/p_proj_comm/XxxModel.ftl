package ${modelData.classData.pkgName};

import com.ms.base.comm.model.BaseModel;
import lombok.*;

import java.io.Serializable;

/**
 * <b>description</b>：${remark} <br>
 * <b>time</b>：${dateTime} <br>
 * <b>author</b>：${author}
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ${modelData.classData.simpleName} extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
    <#list tableModel.fieldModelList as fieldModel>
    public static final String ${fieldModel.name?upper_case}_COL = "${fieldModel.name}";
    </#list>
    <#list tableModel.fieldModelList as fieldModel>
    /**
     * ${fieldModel.remark}
     */
    private ${fieldModel.javaTypeName} ${fieldModel.name};
    </#list>
}