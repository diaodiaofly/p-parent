package ${clientData.classData.pkgName};

import com.ms.core.api.ApiConstant;
import ${controllerData.classData.name};
import org.springframework.cloud.openfeign.FeignClient;

/**
 * <b>description</b>：${remark} 相关操作Feign客户端 <br>
 * <b>time</b>：${dateTime} <br>
 * <b>author</b>：${author}
 */
@FeignClient(value = ApiConstant.SERVICE_ID)
public interface ${clientData.classData.simpleName} extends ${controllerData.classData.simpleName} {
}