package ${apiConstantData.classData.pkgName};

/**
 * <b>description</b>：api常量 <br>
 * <b>time</b>：${dateTime} <br>
 * <b>author</b>：${author}
 */
public class ApiConstant {
    public static final String SERVICE_ID = "p-${prjName}-service";
}