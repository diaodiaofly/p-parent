package ${serviceImplData.classData.pkgName};

import com.ms.base.jdbc.dao.IBaseDao;
import com.ms.base.jdbc.service.BaseServiceImpl;
import ${modelData.classData.name};
import ${daoData.classData.name};
import ${serviceData.classData.name};
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <b>description</b>：${remark}业务实现类 <br>
 * <b>time</b>：${dateTime} <br>
 * <b>author</b>：${author}
 */
@Service
@Slf4j
public class ${serviceImplData.classData.simpleName} extends BaseServiceImpl<${tableModel.primaryFieldModel.javaTypeName}, ${modelData.classData.simpleName}> implements ${serviceData.classData.simpleName} {
    @Autowired
    private ${daoData.classData.simpleName} ${daoData.classData.objName};

    @Override
    public IBaseDao<${tableModel.primaryFieldModel.javaTypeName}, ${modelData.classData.simpleName}> getBaseDao() {
        return this.${daoData.classData.objName};
    }
}