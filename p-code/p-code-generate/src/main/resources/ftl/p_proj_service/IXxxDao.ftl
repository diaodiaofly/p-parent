package ${daoData.classData.pkgName};

import com.ms.base.jdbc.dao.IBaseDao;
import ${modelData.classData.name};

/**
 * <b>description</b>：${remark}数据访问接口 <br>
 * <b>time</b>：${dateTime} <br>
 * <b>author</b>：${author}
 */
public interface ${daoData.classData.simpleName} extends IBaseDao<${tableModel.primaryFieldModel.javaTypeName}, ${modelData.classData.simpleName}> {
}