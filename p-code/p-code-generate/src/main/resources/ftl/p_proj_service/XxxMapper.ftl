package ${mapperData.classData.pkgName};

import com.ms.base.jdbc.dao.IBaseMapper;
import ${modelData.classData.name};
import org.apache.ibatis.annotations.Mapper;

/**
 * <b>description</b>：${remark}mapper <br>
 * <b>time</b>：${dateTime} <br>
 * <b>author</b>：${author}
 */
@Mapper
public interface ${mapperData.classData.simpleName} extends IBaseMapper<${modelData.classData.simpleName}> {
}