package ${controllerData1.classData.pkgName};

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.cloud.ResultUtil;
import com.ms.base.comm.util.Constant;
import com.ms.base.jdbc.datasource.DsType;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import com.ms.base.page.core.PageUtil;
import ${controllerData.classData.name};
import ${modelData.classData.name};
import ${serviceData.classData.name};
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <b>description</b>：${remark} http接口 <br>
 * <b>time</b>：${dateTime} <br>
 * <b>author</b>：${author}
 */
@RestController
public class ${controllerData1.classData.simpleName} implements ${controllerData.classData.simpleName} {

    @Autowired
    private ${serviceData.classData.simpleName} ${serviceData.classData.objName};

    @Override
    public ResultDto<${modelData.classData.simpleName}> insert(@RequestBody ${modelData.classData.simpleName} model) throws Exception {
        return ResultUtil.successData(this.${serviceData.classData.objName}.insert(model));
    }

    @Override
    public ResultDto<Integer> update(@RequestBody ${modelData.classData.simpleName} model) throws Exception {
        return ResultUtil.successData(this.${serviceData.classData.objName}.update(model));
    }

    @Override
    public ResultDto<Integer> deleteById(@RequestParam("id") long id) throws Exception {
        return ResultUtil.successData(this.${serviceData.classData.objName}.deleteById(id));
    }

    @Override
    public ResultDto<Long> getModelListCount(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.${serviceData.classData.objName}.getModelListCount(map, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<${modelData.classData.simpleName}>> getModelList(@RequestBody Map<String, Object> map, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.${serviceData.classData.objName}.getModelList(map, DsType.dsType(master)));
    }

    @Override
    public ResultDto<${modelData.classData.simpleName}> getModelById(@RequestParam("id") long id, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.${serviceData.classData.objName}.getModelById(id, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<${modelData.classData.simpleName}>> getModelsByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.${serviceData.classData.objName}.getModelsByIds(idList, DsType.dsType(master)));
    }

    @Override
    public ResultDto<Map<Long, ${modelData.classData.simpleName}>> getModelMapByIds(@RequestBody List<Long> idList, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.${serviceData.classData.objName}.getModelMapByIds(idList, DsType.dsType(master)));
    }

    @Override
    public ResultDto<${modelData.classData.simpleName}> getModelOne(@RequestBody Map<String, Object> paramMap, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.${serviceData.classData.objName}.getModelOne(paramMap, DsType.dsType(master)));
    }

    @Override
    public ResultDto<PageModel<${modelData.classData.simpleName}>> getPageModel(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.PAGE_KEY) int page, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.${serviceData.classData.objName}.getPageModel(map, page, rows, DsType.dsType(master)));
    }

    @Override
    public ResultDto<PageModel<${modelData.classData.simpleName}>> getPageModel1(@RequestBody PageParamModel pageParamModel, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.${serviceData.classData.objName}.getPageModel1(pageParamModel, DsType.dsType(master)));
    }

    @Override
    public ResultDto<List<${modelData.classData.simpleName}>> getModelList1(@RequestBody Map<String, Object> map, @RequestParam(PageUtil.SKIP_KEY) int skip, @RequestParam(PageUtil.ROWS_KEY) int rows, @RequestParam(Constant.MSTER_KEY) boolean master) throws Exception {
        return ResultUtil.successData(this.${serviceData.classData.objName}.getModelList1(map, skip, rows, DsType.dsType(master)));
    }
}