package ${controllerData2.classData.pkgName};

import com.ms.base.comm.cloud.ResultDto;
import com.ms.base.comm.util.DateUtil;
import com.ms.base.page.core.PageModel;
import com.ms.base.page.core.PageParamModel;
import com.ms.base.web.mvc.BaseViewController;
import com.ms.base.web.mvc.IModuleViewController;
import com.ms.base.web.mvc.WebUtil;
import com.ms.base.web.page.PageHtmlModel;
import com.ms.base.web.page.PageHtmlUtil;
import ${clientData.classData.name};
import ${modelData.classData.name};
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * <b>description</b>：后台管理系统用户管理 <br>
 * <b>time</b>：2018-08-16 14:43 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Controller
@RequestMapping(${controllerData2.classData.simpleName}.REQUEST_MAPPING)
@Slf4j
public class ${controllerData2.classData.simpleName} extends BaseViewController implements IModuleViewController {

    public static final String MODULE_NAME = "${moduleName}";
    public static final String CONTROLLER_NAME = "${controllerData2.controllerName}";
    public static final String REQUEST_MAPPING = WebUtil.URL_SPLIT + MODULE_NAME + WebUtil.URL_SPLIT + CONTROLLER_NAME;

    public static final String LISTVIEW_METHOD_NAME = "listView";
    public static final String LISTVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + LISTVIEW_METHOD_NAME;
    public static final String LISTVIEW_REQUEST_MAPPING_PATH = REQUEST_MAPPING + WebUtil.URL_SPLIT + LISTVIEW_METHOD_NAME;

    public static final String ADDVIEW_METHOD_NAME = "addView";
    public static final String ADDVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + ADDVIEW_METHOD_NAME;

    public static final String EDITVIEW_METHOD_NAME = "editView";
    public static final String EDITVIEW_REQUEST_MAPPING = WebUtil.URL_SPLIT + EDITVIEW_METHOD_NAME + WebUtil.URL_SPLIT + "{id}";

    public static final String SAVE_METHOD_NAME = "save";
    public static final String SAVE_REQUEST_MAPPING = WebUtil.URL_SPLIT + SAVE_METHOD_NAME;

    public static final String DELETE_METHOD_NAME = "delete";
    public static final String DELETE_REQUEST_MAPPING = WebUtil.URL_SPLIT + DELETE_METHOD_NAME + WebUtil.URL_SPLIT + "{id}";


    @Autowired
    private ${clientData.classData.simpleName} ${clientData.classData.objName};

    /**
     * 列表页面
     *
     * @param modelMap 数据存储对象
     * @return
     * @throws Exception
     */
    @RequestMapping(LISTVIEW_REQUEST_MAPPING)
    public String listView(HttpServletRequest request, ModelMap modelMap) throws Exception {
        PageParamModel pageParamModel = WebUtil.getPageParamModel(request);
        PageModel<${modelData.classData.simpleName}> pageModel = this.${clientData.classData.objName}.getPageModel1(pageParamModel, true).okData();
        PageHtmlModel<${modelData.classData.simpleName}> pageHtmlModel = PageHtmlUtil.build(pageModel, pageParamModel, WebUtil.getPathUrl(LISTVIEW_REQUEST_MAPPING_PATH));
        modelMap.addAttribute("pageHtmlModel", pageHtmlModel);
        return this.getViewName(LISTVIEW_METHOD_NAME);
    }

    /**
     * 跳转到新增页面
     *
     * @return 数据存储对象
     */
    @RequestMapping(ADDVIEW_REQUEST_MAPPING)
    public String addView() {
        return this.getViewName(EDITVIEW_METHOD_NAME);
    }

    /**
     * 跳转到编辑页面
     *
     * @param id       对象id
     * @param modelMap
     * @return
     * @throws Exception
     */
    @RequestMapping(EDITVIEW_REQUEST_MAPPING)
    public String editView(@PathVariable("id") long id, ModelMap modelMap) throws Exception {
        modelMap.put("model", this.${clientData.classData.objName}.getModelById(id, true).okData());
        return this.getViewName(EDITVIEW_METHOD_NAME);
    }

    /**
     * 新增或者保存
     *
     * @param model 对象信息
     * @return
     * @throws Exception
     */
    @RequestMapping(SAVE_REQUEST_MAPPING)
    @ResponseBody
    public ResultDto save(${modelData.classData.simpleName} model) throws Exception {
        if (Objects.isNull(model.getId())) {
            this.${clientData.classData.objName}.insert(model).ok();
            return this.successResultDto("新增成功!");
        } else {
            this.${clientData.classData.objName}.update(model).ok();
            return this.successPathResultDto("修改成功!", LISTVIEW_REQUEST_MAPPING_PATH);
        }
    }

    /**
     * 删除数据
     *
     * @param id 对象id
     * @return
     * @throws Exception
     */
    @RequestMapping(DELETE_REQUEST_MAPPING)
    @ResponseBody
    public ResultDto delete(@PathVariable("id") long id) throws Exception {
        this.${clientData.classData.objName}.deleteById(id).ok();
        return this.successResultDto("删除成功!");
    }

    @Override
    public String moduleName() {
        return MODULE_NAME;
    }

    @Override
    public String controllerName() {
        return CONTROLLER_NAME;
    }
}