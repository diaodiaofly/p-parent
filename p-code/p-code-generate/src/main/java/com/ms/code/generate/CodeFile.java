package com.ms.code.generate;

import lombok.*;

/**
 * <b>description</b>：代码文件信息 <br>
 * <b>time</b>：2018-08-07 14:40 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CodeFile {
    //代码内容
    private String content;
    //文件所在目录
    private String fileDir;
    //文件名称
    private String fileName;
}
