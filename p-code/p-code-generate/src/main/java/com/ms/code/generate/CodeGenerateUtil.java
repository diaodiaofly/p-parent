package com.ms.code.generate;

import com.ms.base.comm.util.DateUtil;
import com.ms.base.comm.util.FrameUtil;
import com.ms.code.generate.p_proj_api.ApiConstantData;
import com.ms.code.generate.p_proj_api.ClientData;
import com.ms.code.generate.p_proj_comm.IControllerData;
import com.ms.code.generate.p_proj_comm.ModelData;
import com.ms.code.generate.p_proj_service.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.nio.charset.Charset;
import java.sql.*;
import java.util.Calendar;

/**
 * <b>description</b>： <br>
 * <b>time</b>：2018-08-06 18:03 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Slf4j
public class CodeGenerateUtil {
    public static void main(String[] args) throws Exception {
        String tableName = "t_admin_user_role";
        String remark = "用户角色关联表";//备注
        String jdbcUrl = "jdbc:mysql://localhost:3306/ms_master?characterEncoding=UTF-8";
        String jdbcDriver = "com.mysql.jdbc.Driver";
        String jdbcUserName = "root";
        String jdbcPassword = "root";
        String codeDirPath = "C:\\Users\\pc\\IdeaProjects1\\p-parent"; //p-parent目录
        boolean writeFile = true;//是否写文件
        boolean overwriteFile = true;//是否覆盖文件
        String prjName = "core";//项目名称
        String moduleName = "admin";//模块名称
        String author = "ready likun_557@163.com";
        String dateTime = DateUtil.format(Calendar.getInstance().getTime(), DateUtil.PATTERN_yyyy_MM_dd_HH_mm_ss);

        log.info("开始生成代码.....");
        CodeData codeData = buildCodeData(CodeData.builder().tableConfig(
                TableConfig.builder().
                        tableName(tableName).
                        jdbcUrl(jdbcUrl).
                        jdbcDriver(jdbcDriver).
                        jdbcUserName(jdbcUserName).
                        jdbcPassword(jdbcPassword).
                        remark(remark).
                        build()).
                codeDirPath(codeDirPath).
                writeFile(writeFile).
                overwriteFile(overwriteFile).
                prefix("t_").
                prjName(prjName).
                moduleName(moduleName).
                remark(remark).
                author(author).
                dateTime(dateTime).
                controllerData2Flag(true).
                build());
//        log.info(FrameUtil.json(codeData, true));
        log.info("代码生成完毕.....");
    }

    /**
     * 构建CodeData
     *
     * @param codeData
     * @return
     */
    public static CodeData buildCodeData(CodeData codeData) throws Exception {
        codeData.setTableModel(buildTable(codeData));

        /**
         * 构建模型数据
         */
        //p_proj_comm
        codeData.setModelData(buildModelData(codeData));
        codeData.setControllerData(buildIControllerData(codeData));
        //p_proj_service
        codeData.setServiceData(buildServiceData(codeData));
        codeData.setServiceImplData(buildServiceImplData(codeData));
        codeData.setDaoData(buildDaoData(codeData));
        codeData.setDaoImplData(buildDaoImplData(codeData));
        codeData.setMapperData(buildMapperData(codeData));
        codeData.setSqlMapData(SqlMapData.builder().build());
        codeData.setControllerData1(buildControllerData1(codeData));
        //p_proj_api
        codeData.setApiConstantData(buildApiConstantData(codeData));
        codeData.setClientData(buildClientData(codeData));
        //p_proj_admin
        codeData.setControllerData2(buildControllerData2(codeData));


        /**
         * 生成代码内容
         */
        //p_proj_comm
        codeData.getModelData().setCodeFile(buildModelCodeFile(codeData));
        codeData.getControllerData().setCodeFile(buildIControllerCodeFile(codeData));
        //p_proj_service
        codeData.getServiceData().setCodeFile(buildServiceCodeFile(codeData));
        codeData.getServiceImplData().setCodeFile(buildServiceImplCodeFile(codeData));
        codeData.getDaoData().setCodeFile(buildDaoCodeFile(codeData));
        codeData.getDaoImplData().setCodeFile(buildDaoImplCodeFile(codeData));
        codeData.getMapperData().setCodeFile(buildMapperCodeFile(codeData));
        codeData.getSqlMapData().setCodeFile(buildSqlMapCodeFile(codeData));
        codeData.getControllerData1().setCodeFile(buildController1CodeFile(codeData));
        //p_proj_api
        codeData.getApiConstantData().setCodeFile(buildApiConstantCodeFile(codeData));
        codeData.getClientData().setCodeFile(buildClientCodeFile(codeData));
        //p_proj_admin
        codeData.getControllerData2().setCodeFile(buildController2CodeFile(codeData));

        if (codeData.isWriteFile()) {
            //保存文件
            saveFile(codeData);
        }

        return codeData;
    }

    /**
     * @param codeData
     * @throws Exception
     */
    private static void saveFile(CodeData codeData) throws Exception {
        boolean overwriteFile = codeData.isOverwriteFile();
        writeFile(codeData.getModelData().getCodeFile(), overwriteFile);
        writeFile(codeData.getControllerData().getCodeFile(), overwriteFile);

        writeFile(codeData.getServiceData().getCodeFile(), overwriteFile);
        writeFile(codeData.getServiceImplData().getCodeFile(), overwriteFile);
        writeFile(codeData.getDaoData().getCodeFile(), overwriteFile);
        writeFile(codeData.getDaoImplData().getCodeFile(), overwriteFile);
        writeFile(codeData.getMapperData().getCodeFile(), overwriteFile);
        writeFile(codeData.getSqlMapData().getCodeFile(), overwriteFile);
        writeFile(codeData.getControllerData1().getCodeFile(), overwriteFile);

        writeFile(codeData.getApiConstantData().getCodeFile(), overwriteFile);
        writeFile(codeData.getClientData().getCodeFile(), overwriteFile);

        if (codeData.isControllerData2Flag()) {
            writeFile(codeData.getControllerData2().getCodeFile(), overwriteFile);
        }

    }

    private static void writeFile(CodeFile codeFile, boolean overwrite) throws Exception {
        File file = new File(codeFile.getFileDir(), codeFile.getFileName());
        if (!file.exists() || overwrite) {
            if (log.isDebugEnabled()) {
                log.debug("生成文件:{}", file.getAbsolutePath());
            }
            FileUtils.writeStringToFile(file, codeFile.getContent(), Charset.defaultCharset());
        }
    }

    /**
     * 构建model代码文件对象
     *
     * @param codeData
     * @return
     * @throws Exception
     */
    public static CodeFile buildModelCodeFile(CodeData codeData) throws Exception {
        String ftlName = "p_proj_comm/XxxModel";
        String codeDirPath = codeData.getCodeDirPath();
        ModelData modelData = codeData.getModelData();
        ClassData classData = modelData.getClassData();
        String content = code(ftlName, codeData);
        String prjName = codeData.getPrjName();
        String prePath = String.format("p-%s/p-%s-comm/src/main/java", prjName, prjName);
        String child = classData.getName().replaceAll("\\.", "/");
        String suffix = "java";
        File file = new File(String.format("%s/%s/%s.%s", codeDirPath, prePath, child, suffix));
        return CodeFile.builder().content(content).fileDir(file.getParent()).fileName(file.getName()).build();
    }

    /**
     * 构建IController代码文件对象
     *
     * @param codeData
     * @return
     * @throws Exception
     */
    public static CodeFile buildIControllerCodeFile(CodeData codeData) throws Exception {
        String ftlName = "p_proj_comm/IXxxController";
        String codeDirPath = codeData.getCodeDirPath();
        IControllerData controllerData = codeData.getControllerData();
        ClassData classData = controllerData.getClassData();
        String content = code(ftlName, codeData);
        String prjName = codeData.getPrjName();
        String prePath = String.format("p-%s/p-%s-comm/src/main/java", prjName, prjName);
        String child = classData.getName().replaceAll("\\.", "/");
        String suffix = "java";
        File file = new File(String.format("%s/%s/%s.%s", codeDirPath, prePath, child, suffix));
        return CodeFile.builder().content(content).fileDir(file.getParent()).fileName(file.getName()).build();
    }

    /**
     * 构建service接口代码文件对象
     *
     * @param codeData
     * @return
     * @throws Exception
     */
    public static CodeFile buildServiceCodeFile(CodeData codeData) throws Exception {
        String ftlName = "p_proj_service/IXxxService";
        String codeDirPath = codeData.getCodeDirPath();
        IServiceData serviceData = codeData.getServiceData();
        ClassData classData = serviceData.getClassData();
        String content = code(ftlName, codeData);
        String prjName = codeData.getPrjName();
        String prePath = String.format("p-%s\\p-%s-service\\src\\main\\java", prjName, prjName);
        String child = classData.getName().replaceAll("\\.", "/");
        String suffix = "java";
        File file = new File(String.format("%s/%s/%s.%s", codeDirPath, prePath, child, suffix));
        return CodeFile.builder().content(content).fileDir(file.getParent()).fileName(file.getName()).build();
    }

    /**
     * 构建service实现类代码文件对象
     *
     * @param codeData
     * @return
     * @throws Exception
     */
    public static CodeFile buildServiceImplCodeFile(CodeData codeData) throws Exception {
        String ftlName = "p_proj_service/XxxServiceImpl";
        String codeDirPath = codeData.getCodeDirPath();
        ServiceImplData serviceImplData = codeData.getServiceImplData();
        ClassData classData = serviceImplData.getClassData();
        String content = code(ftlName, codeData);
        String prjName = codeData.getPrjName();
        String prePath = String.format("p-%s\\p-%s-service\\src\\main\\java", prjName, prjName);
        String child = classData.getName().replaceAll("\\.", "/");
        String suffix = "java";
        File file = new File(String.format("%s/%s/%s.%s", codeDirPath, prePath, child, suffix));
        return CodeFile.builder().content(content).fileDir(file.getParent()).fileName(file.getName()).build();
    }

    /**
     * 构建dao接口类代码文件对象
     *
     * @param codeData
     * @return
     * @throws Exception
     */
    public static CodeFile buildDaoCodeFile(CodeData codeData) throws Exception {
        String ftlName = "p_proj_service/IXxxDao";
        String codeDirPath = codeData.getCodeDirPath();
        IDaoData IXxxDaoData = codeData.getDaoData();
        ClassData classData = IXxxDaoData.getClassData();
        String content = code(ftlName, codeData);
        String prjName = codeData.getPrjName();
        String prePath = String.format("p-%s\\p-%s-service\\src\\main\\java", prjName, prjName);
        String child = classData.getName().replaceAll("\\.", "/");
        String suffix = "java";
        File file = new File(String.format("%s/%s/%s.%s", codeDirPath, prePath, child, suffix));
        return CodeFile.builder().content(content).fileDir(file.getParent()).fileName(file.getName()).build();
    }

    /**
     * 构建dao实现类类代码文件对象
     *
     * @param codeData
     * @return
     * @throws Exception
     */
    public static CodeFile buildDaoImplCodeFile(CodeData codeData) throws Exception {
        String ftlName = "p_proj_service/XxxDaoImpl";
        String codeDirPath = codeData.getCodeDirPath();
        DaoImplData daoImplData = codeData.getDaoImplData();
        ClassData classData = daoImplData.getClassData();
        String content = code(ftlName, codeData);
        String prjName = codeData.getPrjName();
        String prePath = String.format("p-%s\\p-%s-service\\src\\main\\java", prjName, prjName);
        String child = classData.getName().replaceAll("\\.", "/");
        String suffix = "java";
        File file = new File(String.format("%s/%s/%s.%s", codeDirPath, prePath, child, suffix));
        return CodeFile.builder().content(content).fileDir(file.getParent()).fileName(file.getName()).build();
    }

    /**
     * 构建mapper类类代码文件对象
     *
     * @param codeData
     * @return
     * @throws Exception
     */
    public static CodeFile buildMapperCodeFile(CodeData codeData) throws Exception {
        String ftlName = "p_proj_service/XxxMapper";
        String codeDirPath = codeData.getCodeDirPath();
        MapperData mapperData = codeData.getMapperData();
        ClassData classData = mapperData.getClassData();
        String content = code(ftlName, codeData);
        String prjName = codeData.getPrjName();
        String prePath = String.format("p-%s\\p-%s-service\\src\\main\\java", prjName, prjName);
        String child = classData.getName().replaceAll("\\.", "/");
        String suffix = "java";
        File file = new File(String.format("%s/%s/%s.%s", codeDirPath, prePath, child, suffix));
        return CodeFile.builder().content(content).fileDir(file.getParent()).fileName(file.getName()).build();
    }

    /**
     * 构建sqlmap代码文件对象
     *
     * @param codeData
     * @return
     * @throws Exception
     */
    public static CodeFile buildSqlMapCodeFile(CodeData codeData) throws Exception {
        String ftlName = "p_proj_service/SqlMap";
        String codeDirPath = codeData.getCodeDirPath();
        String content = code(ftlName, codeData);
        String prjName = codeData.getPrjName();
        String prePath = String.format("p-%s\\p-%s-service\\src\\main\\resources\\mybatis\\mapper", prjName, prjName);
        String child = codeData.getModelData().getClassData().objName.substring(0, codeData.getModelData().getClassData().objName.length() - "Model".length());
        String suffix = "xml";
        File file = new File(String.format("%s/%s/%s.%s", codeDirPath, prePath, child, suffix));
        return CodeFile.builder().content(content).fileDir(file.getParent()).fileName(file.getName()).build();
    }

    /**
     * 构建controller1代码文件对象
     *
     * @param codeData
     * @return
     * @throws Exception
     */
    public static CodeFile buildController1CodeFile(CodeData codeData) throws Exception {
        String ftlName = "p_proj_service/XxxController";
        String codeDirPath = codeData.getCodeDirPath();
        ControllerData controllerData1 = codeData.getControllerData1();
        ClassData classData = controllerData1.getClassData();
        String content = code(ftlName, codeData);
        String prjName = codeData.getPrjName();
        String prePath = String.format("p-%s\\p-%s-service\\src\\main\\java", prjName, prjName);
        String child = classData.getName().replaceAll("\\.", "/");
        String suffix = "java";
        File file = new File(String.format("%s/%s/%s.%s", codeDirPath, prePath, child, suffix));
        return CodeFile.builder().content(content).fileDir(file.getParent()).fileName(file.getName()).build();
    }

    /**
     * 构建ApiConstant代码文件对象
     *
     * @param codeData
     * @return
     * @throws Exception
     */
    public static CodeFile buildApiConstantCodeFile(CodeData codeData) throws Exception {
        String ftlName = "p_proj_api/ApiConstant";
        String codeDirPath = codeData.getCodeDirPath();
        ApiConstantData apiConstantData = codeData.getApiConstantData();
        ClassData classData = apiConstantData.getClassData();
        String content = code(ftlName, codeData);
        String prjName = codeData.getPrjName();
        String prePath = String.format("p-%s\\p-%s-api\\src\\main\\java", prjName, prjName);
        String child = classData.getName().replaceAll("\\.", "/");
        String suffix = "java";
        File file = new File(String.format("%s/%s/%s.%s", codeDirPath, prePath, child, suffix));
        return CodeFile.builder().content(content).fileDir(file.getParent()).fileName(file.getName()).build();
    }

    /**
     * 构建client代码文件对象
     *
     * @param codeData
     * @return
     * @throws Exception
     */
    public static CodeFile buildClientCodeFile(CodeData codeData) throws Exception {
        String ftlName = "p_proj_api/XxxClient";
        String codeDirPath = codeData.getCodeDirPath();
        ClientData clientData = codeData.getClientData();
        ClassData classData = clientData.getClassData();
        String content = code(ftlName, codeData);
        String prjName = codeData.getPrjName();
        String prePath = String.format("p-%s\\p-%s-api\\src\\main\\java", prjName, prjName);
        String child = classData.getName().replaceAll("\\.", "/");
        String suffix = "java";
        File file = new File(String.format("%s/%s/%s.%s", codeDirPath, prePath, child, suffix));
        return CodeFile.builder().content(content).fileDir(file.getParent()).fileName(file.getName()).build();
    }

    /**
     * 构建controller2代码文件对象
     *
     * @param codeData
     * @return
     * @throws Exception
     */
    public static CodeFile buildController2CodeFile(CodeData codeData) throws Exception {
        String ftlName = "p_proj_admin/XxxController";
        String codeDirPath = codeData.getCodeDirPath();
        com.ms.code.generate.p_proj_admin.ControllerData controllerData2 = codeData.getControllerData2();
        ClassData classData = controllerData2.getClassData();
        String content = code(ftlName, codeData);
        String prjName = codeData.getPrjName();
        String prePath = String.format("p-core\\p-core-admin\\src\\main\\java", prjName, prjName);
        String child = classData.getName().replaceAll("\\.", "/");
        String suffix = "java";
        File file = new File(String.format("%s/%s/%s.%s", codeDirPath, prePath, child, suffix));
        return CodeFile.builder().content(content).fileDir(file.getParent()).fileName(file.getName()).build();
    }


    /**
     * 获取代码
     *
     * @param ftlName  ftl名称
     * @param codeData
     * @return
     * @throws Exception
     */
    public static String code(String ftlName, CodeData codeData) throws Exception {
        return FreemarkerUtil.getFtlToString(ftlName, codeData);
    }

    /**
     * 构建ModelData
     *
     * @param codeData
     * @return
     */
    private static IControllerData buildIControllerData(CodeData codeData) {
        TableModel tableModel = codeData.getTableModel();
        String tableName = tableModel.getName();
        String tname = tableName;
        String split = "_";
        String simpleName = String.format("I%sController", nameCamel(tname, split, true));
        String pkgName = String.format("com.ms.%s.comm.%s.controller", codeData.getPrjName(), codeData.getModuleName());
        String name = String.format("%s.%s", pkgName, simpleName);
        String beanName = nameCamel(tname, split, false);
        String objName = String.format("%sModel", beanName);
        String controllerName = beanName;
        return IControllerData.builder().classData(ClassData.builder().simpleName(simpleName).pkgName(pkgName).name(name).objName(objName).build()).controllerName(controllerName).build();
    }

    /**
     * 构建ModelData
     *
     * @param codeData
     * @return
     */
    private static ModelData buildModelData(CodeData codeData) {
        TableModel tableModel = codeData.getTableModel();
        String tableName = tableModel.getName();
        String tname = tableName;
        String split = "_";
        String simpleName = String.format("%sModel", nameCamel(tname, split, true));
        String pkgName = String.format("com.ms.%s.comm.%s.model", codeData.getPrjName(), codeData.getModuleName());
        String name = String.format("%s.%s", pkgName, simpleName);
        String objName = String.format("%sModel", nameCamel(tname, split, false));
        return ModelData.builder().classData(ClassData.builder().simpleName(simpleName).pkgName(pkgName).name(name).objName(objName).build()).build();
    }

    /**
     * 构建ServiceData
     *
     * @param codeData
     * @return
     */
    private static IServiceData buildServiceData(CodeData codeData) {
        TableModel tableModel = codeData.getTableModel();
        String tableName = tableModel.getName();
        String tname = tableName;
        String split = "_";
        String simpleName = String.format("I%sService", nameCamel(tname, split, true));
        String pkgName = String.format("com.ms.%s.service.%s.service", codeData.getPrjName(), codeData.getModuleName());
        String name = String.format("%s.%s", pkgName, simpleName);
        String objName = String.format("%sService", nameCamel(tname, split, false));
        return IServiceData.builder().classData(ClassData.builder().simpleName(simpleName).pkgName(pkgName).name(name).objName(objName).build()).build();
    }

    /**
     * 构建ServiceImplData
     *
     * @param codeData
     * @return
     */
    private static ServiceImplData buildServiceImplData(CodeData codeData) {
        TableModel tableModel = codeData.getTableModel();
        String tableName = tableModel.getName();
        String tname = tableName;
        String split = "_";
        String simpleName = String.format("%sServiceImpl", nameCamel(tname, split, true));
        String pkgName = String.format("com.ms.%s.service.%s.service.impl", codeData.getPrjName(), codeData.getModuleName());
        String name = String.format("%s.%s", pkgName, simpleName);
        return ServiceImplData.builder().classData(ClassData.builder().simpleName(simpleName).pkgName(pkgName).name(name).build()).build();
    }

    /**
     * 构建DaoData
     *
     * @param codeData
     * @return
     */
    private static IDaoData buildDaoData(CodeData codeData) {
        TableModel tableModel = codeData.getTableModel();
        String tableName = tableModel.getName();
        String tname = tableName;
        String split = "_";
        String simpleName = String.format("I%sDao", nameCamel(tname, split, true));
        String pkgName = String.format("com.ms.%s.service.%s.dao", codeData.getPrjName(), codeData.getModuleName());
        String name = String.format("%s.%s", pkgName, simpleName);
        String objName = String.format("%sDao", nameCamel(tname, split, false));
        return IDaoData.builder().classData(ClassData.builder().simpleName(simpleName).pkgName(pkgName).name(name).objName(objName).build()).build();
    }

    /**
     * 构建DaoImplData
     *
     * @param codeData
     * @return
     */
    private static DaoImplData buildDaoImplData(CodeData codeData) {
        TableModel tableModel = codeData.getTableModel();
        String tableName = tableModel.getName();
        String tname = tableName;
        String split = "_";
        String simpleName = String.format("%sDaoImpl", nameCamel(tname, split, true));
        String pkgName = String.format("com.ms.%s.service.%s.dao.impl", codeData.getPrjName(), codeData.getModuleName());
        String name = String.format("%s.%s", pkgName, simpleName);
        return DaoImplData.builder().classData(ClassData.builder().simpleName(simpleName).pkgName(pkgName).name(name).build()).build();
    }

    /**
     * 构建MapperData
     *
     * @param codeData
     * @return
     */
    private static MapperData buildMapperData(CodeData codeData) {
        TableModel tableModel = codeData.getTableModel();
        String tableName = tableModel.getName();
        String tname = tableName;
        String split = "_";
        String simpleName = String.format("%sMapper", nameCamel(tname, split, true));
        String pkgName = String.format("com.ms.%s.service.%s.mapper", codeData.getPrjName(), codeData.getModuleName());
        String name = String.format("%s.%s", pkgName, simpleName);
        String objName = String.format("%sMapper", nameCamel(tname, split, false));
        return MapperData.builder().classData(ClassData.builder().simpleName(simpleName).pkgName(pkgName).name(name).objName(objName).build()).build();
    }

    /**
     * 构建
     *
     * @param codeData
     * @return
     * @see com.ms.code.generate.p_proj_service.ControllerData
     */
    private static ControllerData buildControllerData1(CodeData codeData) {
        TableModel tableModel = codeData.getTableModel();
        String tableName = tableModel.getName();
        String tname = tableName;
        String split = "_";
        String simpleName = String.format("%sController", nameCamel(tname, split, true));
        String pkgName = String.format("com.ms.%s.service.%s.controller", codeData.getPrjName(), codeData.getModuleName());
        String name = String.format("%s.%s", pkgName, simpleName);
        String beanName = nameCamel(tname, split, false);
        String objName = String.format("%sMapper", beanName);
        String controllerName = beanName;
        return ControllerData.builder().classData(ClassData.builder().simpleName(simpleName).pkgName(pkgName).name(name).objName(objName).build()).controllerName(controllerName).build();
    }

    /**
     * 构建ApiConstantData
     *
     * @param codeData
     * @return
     */
    private static ApiConstantData buildApiConstantData(CodeData codeData) {
        TableModel tableModel = codeData.getTableModel();
        String tableName = tableModel.getName();
        String tname = tableName;
        String split = "_";
        String simpleName = String.format("ApiConstant", nameCamel(tname, split, true));
        String pkgName = String.format("com.ms.%s.api", codeData.getPrjName());
        String name = String.format("%s.%s", pkgName, simpleName);
        String beanName = nameCamel(tname, split, false);
        String objName = String.format("%s", beanName);
        return ApiConstantData.builder().classData(ClassData.builder().simpleName(simpleName).pkgName(pkgName).name(name).objName(objName).build()).build();
    }

    /**
     * 构建ApiConstantData
     *
     * @param codeData
     * @return
     */
    private static ClientData buildClientData(CodeData codeData) {
        TableModel tableModel = codeData.getTableModel();
        String tableName = tableModel.getName();
        String tname = tableName;
        String split = "_";
        String simpleName = String.format("%sClient", nameCamel(tname, split, true));
        String pkgName = String.format("com.ms.%s.api.%s", codeData.getPrjName(), codeData.getModuleName());
        String name = String.format("%s.%s", pkgName, simpleName);
        String beanName = nameCamel(tname, split, false);
        String objName = String.format("%sClient", beanName);
        return ClientData.builder().classData(ClassData.builder().simpleName(simpleName).pkgName(pkgName).name(name).objName(objName).build()).build();
    }

    /**
     * 构建
     *
     * @param codeData
     * @return
     * @see com.ms.code.generate.p_proj_admin.ControllerData
     */
    private static com.ms.code.generate.p_proj_admin.ControllerData buildControllerData2(CodeData codeData) {
        TableModel tableModel = codeData.getTableModel();
        String tableName = tableModel.getName();
        String tname = tableName;
        String split = "_";
        String simpleName = String.format("%sController", nameCamel(tname, split, true));
        String pkgName = String.format("com.ms.core.admin.%s.controller", codeData.getModuleName());
        String name = String.format("%s.%s", pkgName, simpleName);
        String beanName = nameCamel(tname, split, false);
        String objName = String.format("%sController", beanName);
        String controllerName = beanName;
        return com.ms.code.generate.p_proj_admin.ControllerData.builder().classData(ClassData.builder().simpleName(simpleName).pkgName(pkgName).name(name).objName(objName).build()).controllerName(controllerName).build();
    }


    /**
     * 将name转换为骆驼命名法对象
     *
     * @param name
     * @param split
     * @param firstIsUpper
     * @return
     */
    public static String nameCamel(String name, String split, boolean firstIsUpper) {
        String[] strs = name.toLowerCase().split(split);
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < strs.length; i++) {
            String s = strs[i];
            str.append((i == 0 ? s : (s.substring(0, 1).toUpperCase() + s.substring(1))));
        }
        return firstIsUpper ? str.substring(0, 1).toUpperCase() + str.substring(1) : str.toString();
    }

    public static String objName(String typeName) {
        return typeName.substring(0, 1).toLowerCase() + typeName.substring(1);
    }


    public static TableModel buildTable(CodeData codeData) throws Exception {
        TableConfig tableConfig = codeData.getTableConfig();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String tableName = tableConfig.getTableName();
        String name = tableName;
        if (StringUtils.isNotBlank(codeData.getPrefix()) && tableName.startsWith(codeData.getPrefix())) {
            name = tableName.substring(codeData.getPrefix().length());
        }
        TableModel tableModel = TableModel.builder().
                tableName(tableName).
                name(name).
                remark(tableConfig.getRemark()).
                fieldModelList(FrameUtil.newArrayList()).build();
        try {
            conn = getConnection(tableConfig);
            DatabaseMetaData dbmd = conn.getMetaData();
            rs = dbmd.getColumns(null, "%", tableName, "%");
            while (rs.next()) {
                String column_name = rs.getString("COLUMN_NAME");
                String remark = rs.getString("REMARKS");
                boolean primary = rs.getString("IS_AUTOINCREMENT").toUpperCase().equals("YES");
                int type = rs.getInt("DATA_TYPE");
                FieldModel fieldModel = FieldModel.builder().
                        name(column_name.toLowerCase()).
                        remark(remark).
                        primary(primary).
                        type(type).
                        javaType(JdbcTypeJavaTypeMap.jdbcTypeJavaTypeMap.get(type)).
                        build();
                fieldModel.setJavaTypeName(fieldModel.getJavaType().getTypeName());
                tableModel.getFieldModelList().add(fieldModel);
                if (fieldModel.isPrimary()) {
                    tableModel.setPrimaryFieldModel(fieldModel);
                }
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null && !conn.isClosed()) {
                    conn.close();
                }
            } catch (SQLException e) {
                throw e;
            }
        }
        return tableModel;
    }

    private static Connection getConnection(TableConfig tableConfig) throws ClassNotFoundException, SQLException {
        Class.forName(tableConfig.getJdbcDriver());
        return DriverManager.getConnection(tableConfig.getJdbcUrl(), tableConfig.getJdbcUserName(), tableConfig.getJdbcPassword());
    }
}
