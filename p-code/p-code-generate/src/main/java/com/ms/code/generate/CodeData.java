package com.ms.code.generate;

import com.ms.code.generate.p_proj_api.ApiConstantData;
import com.ms.code.generate.p_proj_api.ClientData;
import com.ms.code.generate.p_proj_comm.IControllerData;
import com.ms.code.generate.p_proj_comm.ModelData;
import com.ms.code.generate.p_proj_service.*;
import lombok.*;

/**
 * <b>description</b>：代码其他信息 <br>
 * <b>time</b>：2018-08-07 09:43 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CodeData {
    //表配置
    private TableConfig tableConfig;
    //代码存放路径
    private String codeDirPath;
    //是否生成文件
    private boolean writeFile;
    //是否覆盖文件
    private boolean overwriteFile;
    //生成类需要去掉的前缀
    private String prefix;
    //项目名称
    private String prjName;
    //模块名称
    private String moduleName;
    //作者
    private String author;
    //描述信息
    private String remark;
    //系统当前时间
    private String dateTime;
    //表信息
    private TableModel tableModel;

    //p_proj_comm中的代码
    private ModelData modelData;
    private boolean modelDataFlag = true;//model文件是否需要生成
    private IControllerData controllerData;
    private boolean controllerDataFlag = true;//IController文件是否需要生成

    //p_proj_service中的代码
    private IServiceData serviceData;
    private ServiceImplData serviceImplData;
    private IDaoData daoData;
    private DaoImplData daoImplData;
    private MapperData mapperData;
    private SqlMapData sqlMapData;
    private ControllerData controllerData1;

    private boolean serviceDataFlag = true;//service接口文件是否需要生成
    private boolean serviceImplDataFlag = true;//service实现类文件是否需要生成
    private boolean daoDataFlag = true;//dao接口文件是否需要生成
    private boolean daoImplDataFlag = true;//dao实现类文件是否需要生成
    private boolean mapperDataFlag = true;//mapper文件是否需要生成
    private boolean sqlMapDataFlag = true;//sqlmap文件是否需要生成
    private boolean controllerData1Flag = true;//service对外公开的controller文件是否需要生成

    //p_proj_admin中的代码
    private com.ms.code.generate.p_proj_admin.ControllerData controllerData2;
    private boolean controllerData2Flag = false;//管理后台是否需要在生成controller文件是否需要生成

    //p_proj_api中的代码
    private ApiConstantData apiConstantData;
    private ClientData clientData;
}
