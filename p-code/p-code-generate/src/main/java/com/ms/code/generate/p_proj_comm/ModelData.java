package com.ms.code.generate.p_proj_comm;

import com.ms.code.generate.ClassData;
import com.ms.code.generate.CodeFile;
import lombok.*;

/**
 * <b>description</b>：model数据 <br>
 * <b>time</b>：2018-08-07 09:57 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ModelData{
    private ClassData classData;
    private CodeFile codeFile;
}
