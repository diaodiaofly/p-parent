package com.ms.code.generate.p_proj_comm;

import com.ms.code.generate.ClassData;
import com.ms.code.generate.CodeFile;
import lombok.*;

/**
 * <b>description</b>：IController <br>
 * <b>time</b>：2018-08-20 16:43 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class IControllerData {
    private ClassData classData;
    private CodeFile codeFile;
    private String controllerName;
}
