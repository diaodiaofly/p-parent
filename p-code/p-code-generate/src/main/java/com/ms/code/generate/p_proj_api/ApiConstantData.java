package com.ms.code.generate.p_proj_api;

import com.ms.code.generate.ClassData;
import com.ms.code.generate.CodeFile;
import lombok.*;

/**
 * <b>description</b>：ApiConstant <br>
 * <b>time</b>：2018-08-20 16:26 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ApiConstantData{
    private ClassData classData;
    private CodeFile codeFile;
}
