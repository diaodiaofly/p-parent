package com.ms.code.generate.p_proj_service;

import com.ms.code.generate.ClassData;
import com.ms.code.generate.CodeFile;
import lombok.*;

/**
 * <b>description</b>：service实现类数据 <br>
 * <b>time</b>：2018-08-07 09:57 <br>
 * <b>author</b>： ready likun_557@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ServiceImplData {
    private ClassData classData;
    private CodeFile codeFile;
}
